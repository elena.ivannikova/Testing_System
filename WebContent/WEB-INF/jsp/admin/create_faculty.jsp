<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Testing system" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form id="search_form" class="search-form" action="controller"
				method="post">
				<input type="hidden" name="command" value="createFaculty" />
				<h2>
					<fmt:message key="label.create_faculty" />
				</h2>
				<hr>
				<fieldset class="form-info">
					<%@include file="/WEB-INF/jspf/institutions.jspf"%>
					<div>
						<label><fmt:message key="label.faculty_name" /></label> <input
							type="text" name="facultyName">
					</div>
				</fieldset>
				<fieldset>
					<input type="submit" id="btn1"
						value="<fmt:message key='btn.confirm'/>">
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>