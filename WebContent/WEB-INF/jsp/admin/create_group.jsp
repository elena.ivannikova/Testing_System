<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Testing system" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form id="search_form" class="search-form" name="form1"
				action="controller" method="post">
				<input type="hidden" name="command" value="createGroup" />
				<h2>
					<fmt:message key="label.create_group" />
				</h2>
				<hr>
				<fieldset class="form-info">
					<%@include file="/WEB-INF/jspf/institutions.jspf"%>
					<%@include file="/WEB-INF/jspf/faculties.jspf"%>
					<div>
						<label><fmt:message key="label.group_name" />:</label> <input
							type="text" name="groupName" value="${groupName}">
					</div>
				</fieldset>
				<fieldset>
					<c:if test="${not empty faculties}">
						<input id="btn1" type="submit" name="confirm"
							value="<fmt:message key='btn.confirm'/>">
					</c:if>
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>