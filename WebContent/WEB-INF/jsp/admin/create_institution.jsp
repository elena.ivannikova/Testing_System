<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Testing system" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form id="search_form" class="search-form" action="controller"
				method="post">
				<input type="hidden" name="command" value="createInstitution" />
				<h2>
					<fmt:message key="label.create_institution" />
				</h2>
				<hr>
				<fieldset class="form-info">
					<div>
						<label><fmt:message key="label.educational_institution" />:</label><input
							type="text" name="institutionName">
					</div>
				</fieldset>
				<fieldset>
					<input id="btn1" type="submit"
						value="<fmt:message key='btn.confirm'/>">
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>