<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Testing system" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<!-- ==================== Секция навигации по вопросам
			========================= с отображением названия теста
			========================================================================= -->
			<c:if test="${not empty newTest}">
				<div style="text-align: center;">
					<h2><fmt:message key="label.test_name" />:
						${newTest.test.name}</h2>
				</div>
				<br/>
				<%-- <section id="left_content">
 					<c:forEach var="k" begin="1" end="${newTest.questionNumber}"
						step="1">
						<div>
							<a
								href="controller?command=showCreateQuestionPage&questionOrdinal=${k}"><input
								id="btn_number" type="button" value="${k}" /></a>
						</div>
					</c:forEach>
					<form action="controller" method="post">
						<input type="hidden" name="command" value="" /> <input
							type="submit" value="Завершить создание теста" />
					</form>
				</section>  --%>
				<!-- ==================== Секция отображения номера вопроса с выбором 
				========================= количества вариантов ответа
				========================================================================= -->
				<!-- <section id="question"> -->
					<form style="width: 70%;" class="search-form form-info" name="form2" action="controller" method="post">
						<input type="hidden" value="confirmCreateQuestion" name="command" />
						<input type="hidden" value="${questionOrdinal}"
							name="questionOrdinal" />
						<p style="font-weight: bold; font-size: 16px; line-height: 2;">
							<fmt:message key="label.number_of_question" />: ${questionOrdinal}
						</p>
						<strong><fmt:message key="label.variants_number" /></strong>:
						   <select name="variantsNumber"
							onchange="document.forms['form2'].submit()">
							<c:if test="${empty variantsNumber}">
								<option disabled selected></option>
							</c:if>
							<c:if test="${not empty variantsNumber}">
								<option disabled></option>
							</c:if>
							<c:forEach var="k" begin="1" end="10">
								<c:if test="${k==variantsNumber}">
									<option value="${k}" selected>${k}</option>
								</c:if>
								<c:if test="${k!=variantsNumber}">
									<option value="${k}">${k}</option>
								</c:if>
							</c:forEach>
						</select>

						<!-- ==================== Секция с текстовыми полями ввода текста вопроса и 
						========================= вариантов ответа с кнопкой подтверждения создания вопроса
						================================================================================= -->
						<c:if test="${not empty variantsNumber}">
							<p style="margin-top: 2%;"><fmt:message key="label.question_text"/>:</p>
							<p>
								<textarea name="questionText" rows="20">${questionText}</textarea> <%-- ${currentQuestion.question.text} --%>
							</p>
							<div>
								<c:forEach var="k" begin="1" end="${variantsNumber}">
									<p>
										<fmt:message key="label.variant"/> ${k} <input id="checkbox" type="checkbox" name="correctAnswers"
											value="${k}" /> <fmt:message key="label.correct"/>
									</p>
									<p>
										<textarea name="answerText" rows="6">${answerText[k-1]}</textarea>
									</p>
								</c:forEach>
							</div>
							<input id="btn1" style="width: 270px; margin-top: 1%;" type="submit" name="questionCreateSubmit"
								value="<fmt:message key='btn.confirm_create_question'/>" />
						</c:if>
					</form>
				<!-- </section> -->
			</c:if>
		</div>
		<%-- <%@include file="/WEB-INF/jspf/footer.jspf"%> --%>
	</div>
</body>
</html>