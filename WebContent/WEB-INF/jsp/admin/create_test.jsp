<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Testing system" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form id="search_form" class="search-form" name="form1"
				action="controller" method="get">
				<input type="hidden" name="command" value="showCreateTestPage" />
				<h2>
					<fmt:message key="label.create_test" />
				</h2>
				<hr>
				<fieldset class="form-info">
					<label><fmt:message key="create_test_jsp.label.test_name" />:</label>
					<input type="text" name="testName" value="${testName}">

					<%@include file="/WEB-INF/jspf/categories.jspf"%>
					<%@include file="/WEB-INF/jspf/subjects.jspf"%>

					<c:if test="${not empty complexityList}">
						<label><fmt:message key="label.complexity_level" />:</label>
						<select name="complexity" class="select">
							<option ${complexity==null ? 'selected' : '' } disabled></option>
							<c:forEach var="item" items="${complexityList}">
								<option ${item.id==complexity ? 'selected' : '' }
									value="${item.id}"><fmt:message key="${item.name}"/></option>
							</c:forEach>
						</select>
					</c:if>
					<div>
						<label><fmt:message key="create_test_jsp.duration" />:</label> <input
							name="duration" type="text" placeholder=hh:mm title="hh:mm" value="${duration}" />
					</div>
					<div>
						<label><fmt:message key="create_test_jsp.question_number" />:</label>
						<input type="text" name="questionNumber" value="${questionNumber}"
							title="1-...">
					</div>
					<div>
						<label><fmt:message key="create_test_jsp.free_access" />:</label>
						<input type="checkbox" id="checkbox" name="freeAccess"
							${freeAccess==null ? '' : 'checked'}>
					</div>
				</fieldset>
				<fieldset>
					<input id="btn1" type="submit" name="confirm"
						value="<fmt:message key='btn.confirm'/>" formmethod="post">
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>