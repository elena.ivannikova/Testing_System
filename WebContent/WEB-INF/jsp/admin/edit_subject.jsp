<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form class="search-form" action="controller" method="post">
					<input type="hidden" name="command" value="editSubject"> <input
						type="hidden" name="subjectId" value="${subject.id}">
					<h2>
						<fmt:message key="label.edit_subject" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<label><fmt:message key='label.subject_name' />:</label> <input
							type="text" name="subjectName" value="${subject.name}">
						<%@include file="/WEB-INF/jspf/categories.jspf"%>
					</fieldset>
					<fieldset>
						<input id="btn1" type="submit" name="confirm"
							value="<fmt:message key='btn.confirm'/>" />
					</fieldset>
				</form>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>