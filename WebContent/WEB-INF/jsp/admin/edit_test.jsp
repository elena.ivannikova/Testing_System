<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form class="search-form">
				<input type="hidden" name="command" value="editTest"> <input
					type="hidden" name="testId" value="${testInfo.id}">
				<h2>
					<fmt:message key="label.edit_test" />
				</h2>
				<hr>
				<fieldset>
					<label><fmt:message key="label.test_name" />:</label> <input
						type="text" name="testName" value="${testInfo.testName}">
					<c:if test="${not empty complexityList}">
						<label><fmt:message key="label.complexity_level" />:</label>
						<select class="select" name="complexity" required>
							<option ${testInfo.complexityName ==null?'selected':''} disabled></option>
							<c:forEach var="item" items="${complexityList}">
								<option value="${item.id}"
									${item.name==testInfo.complexityName?'selected':''}><fmt:message
										key="${item.name}" /></option>
							</c:forEach>
						</select>
					</c:if>
					<label><fmt:message key="create_test_jsp.duration" />:</label> <input
						type="text" name="duration" value="${testInfo.duration}">
					<label><fmt:message key="create_test_jsp.free_access" />:</label>
					<c:if test="${testInfo.freeAccess==true}">
						<input id="checkbox" type="checkbox" name="freeAccess" checked>
					</c:if>
					<c:if test="${testInfo.freeAccess==false}">
						<input id="checkbox" type="checkbox" name="freeAccess">
					</c:if>
				</fieldset>
				<fieldset>
					<input id="btn1" type="submit"
						value="<fmt:message key='btn.save_changes'/>">
					<%-- <a
						href="controller?command=showTestQuestions&testId=${testInfo.id}"><input
						id="btn2" type="button" value="<fmt:message key='btn.edit_questions'/>"></a> --%>
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>