<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form id="search_form" class="search-form" action="controller"
					method="post">
					<input type="hidden" name="command" value="searchFaculties" />
					<h2>
						<fmt:message key="label.search_faculties" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<label><fmt:message key="label.faculty_name" />:</label>
						<input
							type="text" name="facultyName" value="${facultyName}">

						<%@include file="/WEB-INF/jspf/institutions.jspf"%>
					</fieldset>
					<fieldset>
						<input id="btn1" name="confirm" type="submit"
							value="<fmt:message key='btn.search'/>" />
					</fieldset>
				</form>
				<c:if test="${not empty faculties}">
					<table>
						<thead>
							<tr>
								<th><fmt:message key="label.faculty" /></th>
								<th><fmt:message key="label.action" /></th>
							</tr>
						</thead>
						<c:forEach var="item" items="${faculties}">
							<tr>
								<td>${item.name}</td>
								<td><a
									href="controller?command=showEditFacultyPage&facultyId=${item.id}"><fmt:message
											key='label.edit' /></a></td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>