<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form id="search_form" class="search-form" name="form1"
					action="controller" method="post">
					<input type="hidden" name="command" value="searchGroups" />
					<h2>
						<fmt:message key="label.search_groups" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<label><fmt:message key="label.group_name"/>:</label>
						<input type="text"
							name="groupName" value="${groupName}">
							
						<%@include file="/WEB-INF/jspf/institutions.jspf"%>
						<%@include file="/WEB-INF/jspf/faculties.jspf"%>
						
					</fieldset>
					<fieldset>
						<input id="btn1" name="confirm" type="submit" value="<fmt:message key='btn.search'/>" />
					</fieldset>
				</form>
				<c:if test="${not empty groups}">
					<table>
						<thead>
							<tr>
								<th><fmt:message key="label.group_name"/></th>
								<th><fmt:message key="label.action"/></th>
							</tr>
						</thead>
						<c:forEach var="item" items="${groups}">
							<tr>
								<td>${item.name}</td>
								<td><a
									href="controller?command=showEditGroupPage&groupId=${item.id}"><fmt:message key="label.edit"/></a></td>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>