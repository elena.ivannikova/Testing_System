<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form id="search_form" class="search-form" action="controller"
					method="get">
					<input type="hidden" name="command" value="searchInstitutions">
					<h2>
						<fmt:message key="label.search_institutions" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<label><fmt:message key="label.educational_institution"/>:</label> <input type="text"
							name="institutionName" value="${institutionName}">
					</fieldset>
					<fieldset>
						<input id="btn1" name="confirm" type="submit" value="<fmt:message key='btn.search'/>" />
					</fieldset>
				</form>
			</c:if>
			<c:if test="${not empty institutions}">
				<div style="display: block;">
					<table>
						<thead>
							<tr>
								<th><fmt:message key="label.educational_institution"/></th>
								<th><fmt:message key="label.action"/></th>
							</tr>
						</thead>
						<c:forEach var="item" items="${institutions}">
							<tr>
								<td>${item.name}</td>
								<td><a
									href="controller?command=showEditInstitutionPage&institutionId=${item.id}"><fmt:message key="label.edit"/></a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>