<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form id="search_form" class="search-form" action="controller"
					method="post">
					<input type="hidden" name="command" value="searchSubjects">
					<h2>
						<fmt:message key="search_subjects_jsp.label.search_subjects" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<div>
							<label><fmt:message key="label.subject_name" />:</label> <input
								type="text" name="subjectName" value="${subjectName}">
						</div>
						<%@include file="/WEB-INF/jspf/categories.jspf"%>
					</fieldset>
					<fieldset>
						<input id="btn1" type="submit" name="confirm"
							value="<fmt:message key='btn.search'/>" />
					</fieldset>
				</form>
			</c:if>
			<c:if test="${not empty subjects}">
				<table>
					<thead>
						<tr>
							<th><fmt:message key="label.subject_name"/></th>
							<th><fmt:message key="label.action"/></th>
						</tr>
					</thead>
					<c:forEach var="item" items="${subjects}">
						<tr>
							<td>${item.name}</td>
							<td><a
								href="controller?command=showEditSubjectPage&subjectId=${item.id}"><fmt:message key="label.edit"/></a></td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>