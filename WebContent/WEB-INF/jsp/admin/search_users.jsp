<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form id="search_form" class="search-form" action="controller"
					name="form1" method="post">
					<input type="hidden" name="command" value="searchUsers">
					<h2>
						<fmt:message key="search_users_jsp.label.search_users" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<label><fmt:message key="search_users_jsp.label.user_name" />:</label>
						<input type="text" name="userName" value="${userName}">
						<%@include file="/WEB-INF/jspf/institutions.jspf"%>
						<%@include file="/WEB-INF/jspf/faculties.jspf"%>
						<%@include file="/WEB-INF/jspf/groups.jspf"%>
						<div>
							<label><fmt:message key="search_users_jsp.label.locked" />:</label>
							<div style="display: inline-block; width: 50%;">
								<input id="checkbox" type="checkbox"
									style="background-color: grey;" name="locked"
									value="${checked}" ${checked}>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<input id="btn1" type="submit" name="confirm"
							value="<fmt:message key='btn.search'/>">
					</fieldset>
				</form>
				<form action="controller" method="get">
					<input type="hidden" name="command" value="lockUsers">
					<c:if test="${not empty users}">
						<div class="table-wrapper">
							<table>
								<thead>
									<tr>
										<td><input type="checkbox" id="check-all" onclick="f()"></td>
										<td><fmt:message key="label.last_name" /></td>
										<td><fmt:message key="label.first_name" /></td>
										<td><fmt:message key="label.email" /></td>
										<td><fmt:message key="label.login" /></td>
										<td><fmt:message
												key="search_users_jsp.label.registration_date" /></td>
										<td><fmt:message key="search_users_jsp.label.role" /></td>
									</tr>
								</thead>
								<c:forEach var="item" items="${users}">
									<tr>
										<td><input class="ch" type="checkbox" name="users"
											value="${item.id}"></td>
										<td>${item.lastName}</td>
										<td>${item.firstName}</td>
										<td>${item.email}</td>
										<td>${item.login}</td>
										<c:set var="dd" scope="request">
											<fmt:message key='date_pattern' />
										</c:set>
										<td><fmt:formatDate pattern="${dd}"
												value='${item.registrDate}' /></td>
										<td><fmt:message key="${item.roleName}" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>
						<div style="display: block;">
							<c:if test="${not empty checked}">
								<input id="btn1" type="submit" name="confirmUnlock"
									value="<fmt:message key='btn.unlock'/>">
							</c:if>
							<c:if test="${empty checked}">
								<input id="btn1" type="submit" name="confirmLock"
									value="<fmt:message key='btn.lock'/>">
							</c:if>
						</div>
					</c:if>
				</form>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>