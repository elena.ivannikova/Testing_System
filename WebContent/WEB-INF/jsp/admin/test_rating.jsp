<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty rating}">
				<div class="search-form" style="text-align: center;">
					<h2><fmt:message key="label.text_rating"/>: ${testName}</h2>
					<hr>
					<div class="table-wrapper">
						<table>
							<thead>
								<tr>
									<th><fmt:message key="label.last_name"/></th>
									<th><fmt:message key="label.first_name"/></th>
									<th><fmt:message key="label.login"/></th>
									<th><fmt:message key="label.result"/></th>
								</tr>
							</thead>
							<c:forEach var="item" items="${rating}">
								<tr>
									<td>${item.lastName}</td>
									<td>${item.firstName}</td>
									<td>${item.login}</td>
									<td><fmt:formatNumber maxFractionDigits="2" value="${item.result}"/>%</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>