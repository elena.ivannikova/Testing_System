<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<u:permit/>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<%-- <c:if test="${not empty user}"> --%>
				<c:if test="${not empty users}">
					<form method="post" action="controller">
						<input type="hidden" name="command" value="confirmUsers">
						<fieldset class="form-info">
							<u:userList users="${users}" />
						</fieldset>
						<div class="btn2_container">
							<input id="btn2" type="submit"  name="confirm"
								value="<fmt:message key="btn.confirm"/>"> <input
								id="btn2" type="submit" name="reject"
								value="<fmt:message key="btn.reject"/>">
						</div>
					</form>
				</c:if>
			<%-- </c:if> --%>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>