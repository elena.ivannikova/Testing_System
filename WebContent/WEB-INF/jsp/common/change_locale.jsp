<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@page import="ua.nure.blyzniuk.SummaryTask4.db.entity.*" %>
<%@page import="ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService" %>
<%@page import="ua.nure.blyzniuk.SummaryTask4.Path" %>

<%-- set the locale --%>
<fmt:setLocale value="${param.locale}" scope="session" />

<%-- load the bundle (by locale) --%>
<fmt:setBundle basename="resources" />

<%-- set current locale to session --%>
<c:set var="currentLocale" value="${param.locale}" scope="session" />
<%
	String currentLocale = session.getAttribute("currentLocale").toString();
	Language language = CommonService.getCurrentLanguage(currentLocale);
	session.setAttribute("language", language);
	Country country = CommonService.getCurrentCountry(currentLocale);
	session.setAttribute("country", country);
	response.sendRedirect(getServletContext().getContextPath() + Path.PAGE_MAIN);
%>