<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form class="search-form" action="controller" method="get">
				<input name="command" value="changePassword" type="hidden">
				<fieldset class="form-info">
					<div>
						<label><fmt:message key="change_password_jsp.label.enter_current_password"/>:</label> <input type="password"
							name="currentPassword">
					</div>
					<div>
						<label><fmt:message key="change_password_jsp.label.enter_new_password"/>:</label> <input type="password"
							name="newPassword">
					</div>
					<div>
						<label><fmt:message key="change_password_jsp.label.repeat_new_password"/>:</label> <input type="password"
							name="newPassword">
					</div>
				</fieldset>
				<fieldset>
					<input id="btn1" type="submit" value="<fmt:message key='form.submit_confirm'/>">
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>