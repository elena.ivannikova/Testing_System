<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body onload="startTimer()">
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form class="search-form" id="form1" action="controller" method="get">
				<input name="command" value="editUser" type="hidden">
				<fieldset class="form-info">
					<div>
						<label><fmt:message key="label.first_name"/>:</label> <input type="text" name="firstName"
							value="${user.firstName}">
					</div>
					<div>
						<label><fmt:message key="label.last_name"/>:</label> <input type="text" name="lastName"
							value="${user.lastName}">
					</div>
					<div>
						<label><fmt:message key="label.email"/>:</label> <input type="text" name="email"
							value="${user.email}">
					</div>
					<div>
						<label><fmt:message key="label.login"/>:</label> <input type="text" name="login"
							value="${user.login}">
					</div>
				</fieldset>
				<fieldset>
					<input id="btn2" type="submit" value="<fmt:message key='btn.save'/>" /> <a
						href="controller?command=showChangePasswordPage"><input id="btn2" type="button" value="<fmt:message key='btn.change_password'/>"></a>
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>