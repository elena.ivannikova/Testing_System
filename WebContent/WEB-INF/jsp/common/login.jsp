<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${empty user}">
				<form class="login-form" action="controller"
					method="post">
					<input type="hidden" name="command" value="login" />
					<fieldset class="form-info">
						<label> <fmt:message key="login_jsp.label.enter_login" />
							<input type="text" name="login" value="" />
						</label> <label> <fmt:message key="login_jsp.label.enter_password" />
							<input type="password" name="password" value="" />
						</label>
					</fieldset>
					<fieldset class="login-form-action">
						<input class="btn" type="submit" name="submit"
							value='<fmt:message key="header_jspf.menu.link.log_in"/>'>
						<label><a href="controller?command=showRegistrationPage"
							style="text-align: center;"><fmt:message
									key="header_jspf.menu.link.registration" /></a></label>
					</fieldset>
				</form>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>