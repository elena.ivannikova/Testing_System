<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<div class="section2">
					<div class="info-block" id="info_block">
						<div>
							<label><fmt:message key="label.first_name" />:</label> <label
								id="label-info">${user.firstName}</label>
						</div>
						<div>
							<label><fmt:message key="label.last_name" />:</label> <label
								id="label-info">${user.lastName}</label>
						</div>
						<div>
							<label><fmt:message key="label.email" />:</label> <label
								id="label-info">${user.email}</label>
						</div>
						<div>
							<label><fmt:message key="label.login" />:</label> <label
								id="label-info">${user.login}</label>
						</div>
						<div>
							<c:set var="dd" scope="request">
								<fmt:message key='date_pattern' />
							</c:set>
							<label><fmt:message
									key="search_users_jsp.label.registration_date" />:</label> <label
								id="label-info"><fmt:formatDate pattern="${dd}"
									value='${user.registrDate}' /></label>
						</div>
						<c:if test="${userRole.name == 'student'}">
							<div>
								<label><fmt:message key="label.test_limit" />:</label> <label
									id="label-info">${user.testLimit}</label>
							</div>
						</c:if>
						<a href="controller?command=showEditUserPage"><input id="btn2"
							type="button" value="<fmt:message key='label.edit'/>" /></a>
						<c:if
							test="${userRole.name == 'student' || userRole.name == 'educator'}">
							<a href="controller?command=showAddInstitutionPage"><input
								id="btn2" type="button"
								value="<fmt:message key='btn.add_educational_institution'/>" /></a>
						</c:if>
					</div>
				</div>
				<c:if
					test="${userRole.name == 'student' || userRole.name == 'educator'}">
					<div class="section2" style="position: absolute; margin-top: 0;">
						<c:if test="${not empty institutions}">
							<table>
								<thead>
									<tr>
										<td><fmt:message key="label.educational_institution" /></td>
										<td><fmt:message key="label.faculty" /></td>
										<td><fmt:message key="label.group" /></td>
									</tr>
								</thead>
								<c:forEach var="item" items="${institutions}">
									<tr>
										<td>${item.universityName}</td>
										<td>${item.facultyName}</td>
										<td>${item.groupName}</td>
									</tr>
								</c:forEach>
							</table>
						</c:if>
					</div>
				</c:if>
				<c:if test="${userRole.name == 'student'}">
					<c:if test="${not empty studentTestsList}">
						<table>
							<thead>
								<tr>
									<td><fmt:message key="label.test" /></td>
									<td><fmt:message key="label.subject" /></td>
									<td><fmt:message key="label.date_pass" /></td>
									<td><fmt:message key="label.complexity_level" /></td>
									<td><fmt:message key="create_test_jsp.duration" /></td>
									<td><fmt:message key="create_test_jsp.question_number" /></td>
									<td><fmt:message key="label.obligatory_test" /></td>
									<td><fmt:message key="label.result" /></td>
								</tr>
							</thead>
							<c:set var="k" value="0" />
							<c:forEach var="item" items="${studentTestsList}">
								<c:set var="k" value="${k+1}" />
								<tr>
									<td>${item.testName}</td>
									<td>${item.subjectName}</td>
									<c:set var="dd" scope="request">
										<fmt:message key='date_pattern' />
									</c:set>
									<td><fmt:formatDate pattern="${dd}"
											value='${item.datePass}' /></td>
									<td><fmt:message key="${item.complexityName}" /></td>
									<td>${item.duration}</td>
									<td>${item.questionNumber}</td>
									<td>${item.freeAccess==true?'-':'+'}</td>
									<td>${item.testResult}%</td>
								</tr>
							</c:forEach>
						</table>
					</c:if>
				</c:if>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>