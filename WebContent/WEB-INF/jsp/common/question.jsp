<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body onload="startTimer()">
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<div style="text-align: center;">
				<c:if test="${not empty testing}">
					<h2>
						<strong><span style="color: #107dbe;"><fmt:message
									key="create_test_jsp.label.test_name" />: ${testing.test.name}</span></strong>
					</h2>
				</c:if>
			</div>
			<br />
			<section id="left_content">
				<c:if test="${userRole.name == 'student'}">
					<div>
						<span id="my_timer"
							style="color: #107dbe; font-size: 150%; font-weight: bold;"></span>
						<input id="timer" type="hidden" value="${timer1}">
					</div>
				</c:if>
				<c:forEach var="item" items="${testing.questions}">
					<c:set var="k" value="${k+1}" />
					<div>
						<a
							href="controller?command=chooseQuestion&questionOrdinal=${k}&timer=${timer}">
							<c:if test="${item.confirmed.equals(true)}">
								<input id="btn_number"
									style="color: white; font-weight: bold; font-size: 16px; background: #23d859;"
									type="button" value="${k}" />
							</c:if> <c:if test="${item.confirmed.equals(false)}">
								<input id="btn_number" type="button" value="${k}" />
							</c:if>
						</a>
					</div>
				</c:forEach>
				<c:if test="${userRole.name == 'student'}">
					<form action="controller" method="post" name="form3">
						<input type="hidden" name="command" value="finishTest" /> <input
							id="btn1" style="margin-left: 0; margin-top: 20px; width: 150px;"
							type="submit" value="<fmt:message key='btn.finish_test'/>" />
					</form>
				</c:if>
			</section>
			<section id="question">
				<form id="form1" class="form-info" action="controller" method="get">
					<input type="hidden" name="timer" value="${timer}"> <input
						type="hidden" value="confirmAnswer" name="command" /> <input
						type="hidden" value="${questionAnswers.question.ordinal+1}"
						name="questionOrdinal" />
					<c:if test="${not empty questionAnswers}">
						<div style="width: 200px;"><pre style="font-weight: bold;">${questionAnswers.question.ordinal}. ${questionAnswers.question.text}</pre></div>
						<c:forEach var="item" items="${questionAnswers.answers}">
							<c:set var="k" value="${k+1}" />
							<p>
								<c:if test="${userRole.name == 'admin'}">
									<input type="checkbox" name="answers" value="${item.ordinal}"
										${item.correct.equals(true)?'checked':''} disabled />${item.text}
									<%-- <input type="text" name="variants" value="${item.text}"
										disabled> --%>
								</c:if>

								<c:if test="${userRole.name == 'educator'}">
									<c:if test="${item.correct.equals(true)}">
										<input type="checkbox" name="answers" value="${item.ordinal}"
											checked disabled />
								${item.text}
									</c:if>
									<c:if
										test="${item.correct.equals(false) || item.correct == null}">
										<input type="checkbox" name="answers" value="${item.ordinal}"
											disabled />
								${item.text}
									</c:if>
								</c:if>

								<c:if test="${userRole.name == 'student'}">
									<c:if test="${item.correct.equals(true)}">
										<input type="checkbox" name="answers" value="${item.ordinal}"
											checked />
								${item.text}
									</c:if>
									<c:if
										test="${item.correct.equals(false) || item.correct == null}">
										<input type="checkbox" name="answers" value="${item.ordinal}" />
								${item.text}
									</c:if>
								</c:if>
							</p>
						</c:forEach>
					</c:if>
					<c:if test="${userRole.name == 'student'}">
						<input type="submit" value="Ответить" id="btn1"
							style="margin-left: 0; margin-top: 20px; width: 120px;" />
					</c:if>
					<c:if test="${userRole.name == 'admin'}">
						<script type="text/javascript">
							function enableElements() {
								var elementsList1 = document
										.getElementsByName("answers");
								for (var i = 0; i < elementsList1.length; i++) {
									elementsList1[i]
											.removeAttribute("disabled"); // i узел в коллекции NodeList
								}
								var elementsList2 = document
										.getElementsByName("variants");
								for (var i = 0; i < elementsList2.length; i++) {
									elementsList2[i]
											.removeAttribute("disabled"); // i узел в коллекции NodeList
								}

								document.getElementById("btn1").style.display = "none";
								document.getElementById("btn2").style.display = "inline";
							}
						</script>
						<%-- <input onclick="enableElements()" id="btn1"
							style="margin-left: 0; margin-top: 20px;" type="button"
							value="<fmt:message key='label.edit'/>" />
						<input type="submit" id="btn2" style="display: none;"
							value="<fmt:message key='btn.save_changes'/>" /> --%>
					</c:if>
				</form>
			</section>
		</div>
		<%-- <%@include file="/WEB-INF/jspf/footer.jspf"%> --%>
	</div>
</body>
</html>