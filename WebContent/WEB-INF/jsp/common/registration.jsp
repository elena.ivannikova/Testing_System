<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form class="search-form" action="controller" method="post">
				<input type="hidden" name="command" value="register" />
				<h2>
					<fmt:message key="registration_jsp.label.registration" />
				</h2>
				<hr>
				<fieldset class="form-info">
					<div>
						<label><fmt:message key="label.first_name" />:</label> <input
							type="text" name="first_name" value="">
					</div>
					<div>
						<label><fmt:message key="label.last_name" />:</label> <input
							type="text" name="last_name" value="" required />
					</div>
					<div>
						<label><fmt:message key="label.email" />:</label> <input
							type="text" name="email" value="" required />
					</div>
					<div>
						<label><fmt:message key="label.login" />:</label> <input
							title="<fmt:message key='label.login_format'/>" type="text" name="login" value="" required />
					</div>
					<div>
						<label><fmt:message key="registration_jsp.label.password" />:</label>
						<input type="password" name="password" value="" required />
					</div>
					<div>
						<label><fmt:message
								key="registration_jsp.label.password_repeat" />:</label> <input
							type="password" name="repeat_password" value="" required />
					</div>
					<c:if test="${userRole.name == 'admin' }">
						<div>
							<label><fmt:message key="registration_jsp.label.educator" />:</label>
							<input id="checkbox" type="radio" value="2" name="role" required />
						</div>
						<div>
							<label><fmt:message key="registration_jsp.label.admin" />:</label>
							<input id="checkbox" type="radio" value="1" name="role" required />
						</div>
					</c:if>
				</fieldset>
				<fieldset>
					<input id="btn1" type="submit"
						value="<fmt:message key='btn.confirm'/>" />
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>