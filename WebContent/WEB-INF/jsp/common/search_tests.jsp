<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form class="search-form" action="controller" name="form1"
					method="post">
					<input type="hidden" name="command" value="searchTests">
					<h2>
						<fmt:message key="search_tests_jsp.label.search_tests" />
					</h2>
					<hr>
					<fieldset class="form-info">
						<div>
							<label><fmt:message key="label.test_name" />:</label> <input
								type="text" name="testName" value="${testName}">
						</div>
						<%@include file="/WEB-INF/jspf/categories.jspf"%>
						<%@include file="/WEB-INF/jspf/subjects.jspf"%>
						<div>
							<label><fmt:message key="label.sorting" />:</label> <select
								class="select" name="sorting">
								<option selected></option>
								<c:forEach var="item" items="${sorting}">
									<option value="${item}" ${item==sort ? 'selected' : '' }><fmt:message
											key="${item}" /></option>
								</c:forEach>
							</select>
						</div>
					</fieldset>
					<fieldset>
						<input id="btn1" type="submit" name="confirm"
							value="<fmt:message key='btn.search'/>" />
					</fieldset>
				</form>
			</c:if>
			<c:if test="${not empty foundTests}">
				<form action="controller?command=deleteTests" method="post"
					name="form4">
					<div class="table-wrapper">
						<table>
							<thead>
								<tr>
									<c:if test="${userRole.name == 'admin'}">
										<td><input type="checkbox" id="check-all" onclick="f()"></td>
									</c:if>
									<td><fmt:message key="create_test_jsp.label.test_name" /></td>
									<td><fmt:message key="label.subject" /></td>
									<td><fmt:message key="create_test_jsp.duration" /></td>
									<td><fmt:message key="create_test_jsp.complexity" /></td>
									<td><fmt:message key="create_test_jsp.question_number" /></td>
									<c:if test="${userRole.name == 'admin'}">
										<td><fmt:message key="create_test_jsp.free_access" /></td>
									</c:if>
								</tr>
							</thead>
							<c:forEach var="item" items="${foundTests}">
								<tr>
									<c:if test="${userRole.name == 'admin'}">
										<td><input class="ch" type="checkbox" name="tests"
											value="${item.id}"></td>
									</c:if>
									<td><a
										href="controller?command=showTestInfo&testId=${item.id}">${item.testName}</a></td>
									<td>${item.subjectName}</td>
									<td>${item.duration}</td>
									<td><fmt:message key="${item.complexityName}" /></td>
									<td>${item.questionNumber}</td>
									<c:if test="${userRole.name == 'admin'}">
										<td>${item.freeAccess ? '+' : '-'}</td>
									</c:if>
								</tr>
							</c:forEach>
						</table>

					</div>
					<c:if test="${userRole.name == 'admin'}">
						<p>
							<input id="btn1" type="button" onclick="confirmTestDelete()"
								value="<fmt:message key='btn.delete_tests'/>">
						</p>
					</c:if>
				</form>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>