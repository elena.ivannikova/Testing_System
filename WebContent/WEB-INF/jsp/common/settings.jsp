<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<form class="search-form" action="controller"
				method="post">
				<input type="hidden" name="command" value="changeLocale">
				<fieldset class="form-info">
					<label><fmt:message key="settings_jsp.label.set_locale" />:</label>
					<select class="select" name="locale">
						<c:forEach items="${applicationScope.locales}" var="locale">
							<c:set var="selected"
								value="${locale.key == currentLocale ? 'selected' : '' }" />
							<option value="${locale.key}" ${selected}>${locale.value}</option>
						</c:forEach>
					</select> <input type="submit" id="btn1"
						value="<fmt:message key='settings_jsp.form.submit_save_locale'/>">
				</fieldset>
			</form>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>




