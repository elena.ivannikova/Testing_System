<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty testInfo}">
				<div class="info-block">
					<div>
						<label><fmt:message key="label.test_name" />:</label> <label
							id="label-info">${testInfo.testName}</label>
					</div>
					<div>
						<label><fmt:message key="label.subject" />:</label> <label
							id="label-info">${testInfo.subjectName}</label>
					</div>
					<div>
						<label><fmt:message key="label.complexity_level" />:</label> <label
							id="label-info"><fmt:message
								key="${testInfo.complexityName}" /></label>
					</div>
					<div>
						<label><fmt:message key="create_test_jsp.question_number" />:</label>
						<label id="label-info">${testInfo.questionNumber}</label>
					</div>
					<div>
						<label><fmt:message key="create_test_jsp.duration" />:</label> <label
							id="label-info">${testInfo.duration}</label>
					</div>
					<c:if test="${userRole.name == 'admin'}">
						<div>
							<label><fmt:message key="create_test_jsp.free_access" />:</label>
							<label id="label-info">${testInfo.freeAccess==true?'+':'-'}</label>
						</div>
					</c:if>
					<c:if test="${userRole.name == 'student'}">
						<p>
							<a
								href="controller?command=startTest&testId=${testInfo.id}&freeAccess=${testInfo.freeAccess}"><input
								id="btn2" style="text-decoration: none;" type="button"
								value="<fmt:message key='btn.start_test'/>"></a>
						</p>
					</c:if>
					<c:if test="${userRole.name == 'admin'}">
						<a
							href="controller?command=showEditTestPage&testId=${testInfo.id}"><input
							id="btn2" style="text-decoration: none;" type="button"
							value="<fmt:message key='label.edit'/>"></a>
					</c:if>
					<c:if
						test="${userRole.name == 'educator' || userRole.name == 'admin'}">
						<a
							href="controller?command=showTestQuestions&testId=${testInfo.id}"><input
							id="btn2" type="button"
							value="<fmt:message key='btn.view_questions'/>"></a>
						<div style="margin-top: 1%;">
							<a
								href="controller?command=showTestRatingPage&testId=${testInfo.id}&testName=${testInfo.testName}"><input
								id="btn2" type="button" value="Рейтинг"></a>
						</div>
					</c:if>
				</div>
			</c:if>
		</div>
		<%
			session.setAttribute("testInfo", null);
		%>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>