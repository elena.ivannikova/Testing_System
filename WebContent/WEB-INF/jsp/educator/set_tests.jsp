<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user && not empty institutions}">
				<form action="controller" name="form1" method="post">
					<div class="search-form" style="width: 60%;">
						<h2>
							<fmt:message key="label.search_groups" />
						</h2>
						<hr>
						<fieldset class="form-info">
							<label><fmt:message
									key="label.group_faculty_institution_name" />:</label> <input
								type="text" name="name" value="${name}">
						</fieldset>
						<fieldset>
							<input id="btn1" name="searchGroups" type="submit"
								value="<fmt:message key='btn.search'/>" />
						</fieldset>
					</div>

					<div style="width: 70%; margin: auto;">
						<c:if test="${not empty institutions}">
							<table>
								<thead>
									<tr>
										<td><input type="checkbox" id="check-all" onclick="f()"></td>
										<td><fmt:message key="label.educational_institution" /></td>
										<td><fmt:message key="label.faculty" /></td>
										<td><fmt:message key="label.group" /></td>
									</tr>
								</thead>
								<c:forEach var="item" items="${institutions}">
									<tr>
										<td><input class="ch" type="checkbox" name="institutions"
											value="${item.groupId}"></td>
										<td>${item.universityName}</td>
										<td>${item.facultyName}</td>
										<td>${item.groupName}</td>
									</tr>
								</c:forEach>
							</table>
						</c:if>
					</div>

					<div class="search-form">
						<input type="hidden" name="command" value="showSetTestsPage">
						<h2>
							<fmt:message key="header_jspf.menu.submenu.link.search_tests" />
						</h2>
						<hr>
						<fieldset class="form-info">
							<label><fmt:message key="label.test_name" />:</label> <input
								type="text" name="testName" value="${testName}">
							<%@include file="/WEB-INF/jspf/categories.jspf"%>
							<%@include file="/WEB-INF/jspf/subjects.jspf"%>
						</fieldset>
						<fieldset>
							<input id="btn1" type="submit" name="search"
								value="<fmt:message key='header_jspf.menu.submenu.link.search'/>" />
						</fieldset>

					</div>
					<c:if test="${not empty foundTests}">
						<table>
							<thead>
								<tr>
									<c:if test="${userRole.name == 'educator'}">
										<td><input type="checkbox" id="check-all" onclick="f()"></td>
									</c:if>
									<td><fmt:message key="label.test_name" /></td>
									<td><fmt:message key="label.subject" /></td>
									<td><fmt:message key="create_test_jsp.duration" /></td>
									<td><fmt:message key="label.complexity_level" /></td>
									<td><fmt:message key="create_test_jsp.question_number" /></td>
								</tr>
							</thead>
							<c:forEach var="item" items="${foundTests}">
								<tr>
									<c:if test="${userRole.name == 'educator'}">
										<td><input class="ch" type="checkbox" name="foundTests"
											value="${item.id}"></td>
									</c:if>
									<td><a
										href="controller?command=showTestInfo&testId=${item.id}">${item.testName}</a></td>
									<td>${item.subjectName}</td>
									<td>${item.duration}</td>
									<td><fmt:message key="${item.complexityName}" /></td>
									<td>${item.questionNumber}</td>
								</tr>
							</c:forEach>
						</table>
					</c:if>
					<div class="search-form">
						<c:if test="${not empty foundTests && not empty institutions}">
							<fieldset class="form-info">
								<label style="width: 45%;"><fmt:message
										key="label.end_date_test_pass" />:</label> <input style="width: 45%;"
									type="date" name="dateEnd" value="">
							</fieldset>
							<fieldset>
								<input id="btn1" type="submit" name="confirm"
									value="<fmt:message key='btn.confirm'/>" />
							</fieldset>
						</c:if>
					</div>
				</form>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>