<%@page import="java.util.Calendar"%>
<%@page import="java.sql.Date"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user && not empty institutions}">
				<form class="search-form" name="form1" action="controller"
					method="post">
					<input type="hidden" name="command" value="showStudentRatingPage">
					<fieldset class="form-info">
						<%@include file="/WEB-INF/jspf/institutions.jspf"%>
						<%@include file="/WEB-INF/jspf/faculties.jspf"%>
						<%@include file="/WEB-INF/jspf/groups.jspf"%>
					</fieldset>
					<fieldset>
						<input id="btn1" type="submit" name="search"
							value="<fmt:message key='btn.show'/>">
					</fieldset>
				</form>
				<c:if test="${not empty testResults && not empty testSet}">
					<table>
						<thead>
							<tr>
								<th></th>
								<c:forEach var="item" items="${testSet}">
									<th>${item}</th>
								</c:forEach>
							</tr>
						</thead>
						<c:forEach var="entry" items="${testResults}">
							<tr style="background: none;">
								<th>${entry.key.lastName} ${entry.key.firstName} (<fmt:message
										key="label.group" /> ${entry.key.groupName})
								</th>
								<c:forEach var="item" items="${entry.value}">
									<td ${item.deadline==true ? 'style="background-color: #ff8a8a;"':'' }>${item.testResult}%</td>
								</c:forEach>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>