<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty user}">
				<form class="search-form" action="controller" name="form1"
					method="post">
					<input type="hidden" name="command" value="addInstitution">
					<fieldset class="form-info">
						<%@include file="/WEB-INF/jspf/institutions.jspf"%>
						<%@include file="/WEB-INF/jspf/faculties.jspf"%>
						<%@include file="/WEB-INF/jspf/groups.jspf"%>
					</fieldset>
					<c:if test="${not empty groups}">
						<fieldset>
							<input id="btn1" type="submit" name="confirm"
								value='<fmt:message key="btn.confirm"/>'>
						</fieldset>
					</c:if>
				</form>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>