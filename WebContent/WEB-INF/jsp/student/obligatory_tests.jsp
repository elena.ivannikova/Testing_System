<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Tests" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<c:if test="${not empty studentTestsList}">
				<table>
					<thead>
						<tr>
							<td><fmt:message key="label.test"/></td>
							<td><fmt:message key="label.subject"/></td>
							<td><fmt:message key="label.test_start"/></td>
							<td><fmt:message key="label.test_end"/></td>
							<td><fmt:message key="label.complexity_level"/></td>
							<td><fmt:message key="create_test_jsp.duration"/></td>
							<td><fmt:message key="create_test_jsp.question_number"/></td>
						</tr>
					</thead>
					<c:set var="k" value="0" />
					<c:forEach var="item" items="${studentTestsList}">
						<c:set var="k" value="${k+1}" />
						<tr style="${item.deadline==true ? 'background-color: #ff8a8a;' : ''}">
							<c:if test="${item.deadline==true}">
								<td>${item.testName}</td>
							</c:if>
							<c:if test="${item.deadline==false}">
								<td><a
									href="controller?command=showTestInfo&testId=${item.testId}">${item.testName}</a></td>
							</c:if>
							<td>${item.subjectName}</td>

							<c:set var="dd" scope="request">
								<fmt:message key='date_pattern' />
							</c:set>
							<td><fmt:formatDate pattern="${dd}"
									value='${item.dateStart}' /></td>
							<td><fmt:formatDate pattern="${dd}"
									value='${item.dateEnd}' /></td>
							<td><fmt:message key="${item.complexityName}"/></td>
							<td>${item.duration}</td>
							<td>${item.questionNumber}</td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>