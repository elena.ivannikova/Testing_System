<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="users" type="java.util.List"%>
<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>

<table>
	<thead>
		<tr>
			<td><input type="checkbox" id="check-all" onclick="f()"></td>
			<td><fmt:message key="label.last_name" /></td>
			<td><fmt:message key="label.first_name" /></td>
			<td><fmt:message key="label.group" /></td>
			<td><fmt:message key="label.faculty" /></td>
			<td><fmt:message key="label.educational_institution" /></td>
		</tr>
	</thead>
	<c:forEach var="item" items="${users}">
		<tr>
			<td><input type="checkbox" class="ch" name="users"
				value="${item}"></td>
			<td>${item.userLastName}</td>
			<td>${item.userFirstName}</td>
			<td>${item.groupName}</td>
			<td>${item.facultyName}</td>
			<td>${item.universityName}</td>
		</tr>
	</c:forEach>
</table>