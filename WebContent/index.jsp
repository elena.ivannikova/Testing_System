<%@include file="/WEB-INF/jspf/directive/taglib.jspf"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<c:set var="title" value="Login" />
<%@include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<div class="content">
			<%@include file="/WEB-INF/jspf/header.jspf"%>
			<div class="index-content">
				<h2><fmt:message key="main_page_jsp.paragraph_1"/></h2>
				<br />
				<p><fmt:message key="main_page_jsp.paragraph_2"/></p>
				<p><fmt:message key="main_page_jsp.paragraph_3"/></p>
				<p>
					<br />
				<h2><fmt:message key="main_page_jsp.paragraph_4"/></h2>
				<br />
				<p><fmt:message key="main_page_jsp.paragraph_5"/></p>
				<ul class="main-list">
					<li><fmt:message key="main_page_jsp.paragraph_5_1"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_5_2"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_5_3"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_5_4"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_5_5"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_5_6"/></li>
				</ul>
				<p><fmt:message key="main_page_jsp.paragraph_6"/></p>
				<br />
				<h2><fmt:message key="main_page_jsp.paragraph_7"/></h2>
				<br />
				<ul class="main-list">
					<li><fmt:message key="main_page_jsp.paragraph_7_1"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_7_2"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_7_3"/></li>
					<li><fmt:message key="main_page_jsp.paragraph_7_4"/></li>
				</ul>
				<br />
				<p><fmt:message key="main_page_jsp.paragraph_8"/></p>
			</div>
		</div>
		<%@include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
</html>