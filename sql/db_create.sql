UNLOCK TABLES;

CREATE DATABASE IF NOT EXISTS `testing` 
DEFAULT CHARACTER SET utf8 
DEFAULT COLLATE utf8_unicode_ci;

USE `testing`;

-- Delete tables

DROP TABLE IF EXISTS `testing`.`user_test_answer`;
DROP TABLE IF EXISTS `testing`.`user_test`;
DROP TABLE IF EXISTS `testing`.`user_group_tmp`;
DROP TABLE IF EXISTS `testing`.`user_group`;
DROP TABLE IF EXISTS `testing`.`answer`;
DROP TABLE IF EXISTS `testing`.`question`;
DROP TABLE IF EXISTS `testing`.`test`;
DROP TABLE IF EXISTS `testing`.`user`;
DROP TABLE IF EXISTS `testing`.`group`;
DROP TABLE IF EXISTS `testing`.`faculty`;
DROP TABLE IF EXISTS `testing`.`university`;
DROP TABLE IF EXISTS `testing`.`subject`;
DROP TABLE IF EXISTS `testing`.`category`;
DROP TABLE IF EXISTS `testing`.`language`;
DROP TABLE IF EXISTS `testing`.`complexity`;
DROP TABLE IF EXISTS `testing`.`country`;
DROP TABLE IF EXISTS `testing`.`role`;

-- Create table `role`

CREATE TABLE `testing`.`role` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `name` VARCHAR(25) NOT NULL UNIQUE);

-- Create table `country`

CREATE TABLE `testing`.`country` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `name` VARCHAR(75) NOT NULL UNIQUE);

-- Create table `complexity`

CREATE TABLE `testing`.`complexity` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `name` VARCHAR(25) NOT NULL UNIQUE);
  
-- Create table `language`

CREATE TABLE `testing`.`language` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY,
  `name` VARCHAR(20) NOT NULL UNIQUE);

-- Create table `category`

CREATE TABLE `testing`.`category` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(100) NOT NULL UNIQUE,
  `language_id` INT UNSIGNED NOT NULL DEFAULT 1,
  FOREIGN KEY (`language_id`)
    REFERENCES `testing`.`language` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
  
-- Create table `subject`

CREATE TABLE `testing`.`subject` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(100) NOT NULL UNIQUE,
  `category_id` BIGINT UNSIGNED NOT NULL,
  FOREIGN KEY (`category_id`)
  REFERENCES `testing`.`category` (`id`)
  ON DELETE CASCADE ON UPDATE CASCADE);

-- Create table `university`

CREATE TABLE `testing`.`university` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(100) NOT NULL UNIQUE,
  `country_id` INT UNSIGNED NOT NULL,
  FOREIGN KEY (`country_id`)
  REFERENCES `testing`.`country` (`id`)
  ON DELETE CASCADE ON UPDATE CASCADE);

-- Create table `faculty`

CREATE TABLE `testing`.`faculty` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(50) NOT NULL UNIQUE,
  `university_id` BIGINT UNSIGNED NOT NULL,
  FOREIGN KEY (`university_id`)
    REFERENCES `testing`.`university` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
    
-- Create table `group`

CREATE TABLE `testing`.`group` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(50) NOT NULL UNIQUE,
  `faculty_id` BIGINT UNSIGNED NOT NULL,
  FOREIGN KEY (`faculty_id`)
    REFERENCES `testing`.`faculty` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
    
-- Create table `user`

CREATE TABLE `testing`.`user` (
  `id` SERIAL PRIMARY KEY,
  `first_name` VARCHAR(30) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(50) NOT NULL UNIQUE,
  `role_id` INT UNSIGNED NOT NULL DEFAULT '3',
  `login` VARCHAR(25) NOT NULL UNIQUE,
  `password` VARCHAR(128) NOT NULL,
  `locked` BIT(1) NOT NULL DEFAULT 0,
  `registr_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `test_limit` INT NOT NULL DEFAULT 3,
  FOREIGN KEY (`role_id`)
    REFERENCES `testing`.`role` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
    
-- Create table `test`

CREATE TABLE `testing`.`test` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(50) NOT NULL UNIQUE,
  `duration` TIME NOT NULL DEFAULT '00:30:00',
  `subject_id` BIGINT UNSIGNED NOT NULL,
  `complexity_id` INT UNSIGNED NOT NULL,
  `free_access` BIT(1) NOT NULL DEFAULT 1,
  FOREIGN KEY (`subject_id`)
    REFERENCES `testing`.`subject` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`complexity_id`)
    REFERENCES `testing`.`complexity` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
    
-- Create table `question`

CREATE TABLE `testing`.`question` (
  `id` SERIAL PRIMARY KEY,
  `text` VARCHAR(1000) NOT NULL,
  `test_id` BIGINT UNSIGNED NOT NULL,
  `ordinal` INT NOT NULL,
  FOREIGN KEY (`test_id`)
    REFERENCES `testing`.`test` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
    
-- Create table `answer`

CREATE TABLE `testing`.`answer` (
  `id` SERIAL PRIMARY KEY,
  `text` VARCHAR(500) NOT NULL,
  `correct` BIT(1) NOT NULL DEFAULT 0,
  `question_id` BIGINT UNSIGNED NOT NULL,
  `ordinal` INT NOT NULL,
  FOREIGN KEY (`question_id`)
    REFERENCES `testing`.`question` (`id`)
    ON DELETE CASCADE ON UPDATE CASCADE);
    
-- Create table `user_group`
    
CREATE TABLE `testing`.`user_group` (
 `user_id` BIGINT UNSIGNED NOT NULL,
 `group_id` BIGINT UNSIGNED NOT NULL,
 FOREIGN KEY (`user_id`)
   REFERENCES `testing`.`user` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY (`group_id`)
   REFERENCES `testing`.`group` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE);
   
-- Create table `user_group_tmp`
    
CREATE TABLE `testing`.`user_group_tmp` (
 `user_id` BIGINT UNSIGNED NOT NULL,
 `group_id` BIGINT UNSIGNED NOT NULL,
 FOREIGN KEY (`user_id`)
   REFERENCES `testing`.`user` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE,
 FOREIGN KEY (`group_id`)
   REFERENCES `testing`.`group` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE);
   
-- Create table `user_test`
    
CREATE TABLE `testing`.`user_test` (
 `id` SERIAL PRIMARY KEY,
 `user_id` BIGINT UNSIGNED NOT NULL,
 `test_id` BIGINT UNSIGNED NOT NULL,
 `date_pass` DATETIME,
 `date_start` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `date_end` DATETIME,
   FOREIGN KEY (`user_id`)
   REFERENCES `testing`.`user` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE,
   FOREIGN KEY (`test_id`)
   REFERENCES `testing`.`test` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE);
   
-- Create table `user_test_answer`
    
CREATE TABLE `testing`.`user_test_answer` (
 `user_test_id` BIGINT UNSIGNED NOT NULL,
 `answer_id` BIGINT UNSIGNED NOT NULL,
 `correct` BIT(1),
   FOREIGN KEY (`user_test_id`)
   REFERENCES `testing`.`user_test` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE,
   FOREIGN KEY (`answer_id`)
   REFERENCES `testing`.`answer` (`id`)
   ON DELETE CASCADE ON UPDATE CASCADE);