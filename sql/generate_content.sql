USE `testing`;

LOCK TABLES `role` WRITE;
INSERT INTO `role` VALUES (1,'admin'),
(2,'educator'),
(3,'student');
UNLOCK TABLES;  

LOCK TABLES `country` WRITE;
INSERT INTO `country` VALUES (1,'UA'),
(2,'US');
UNLOCK TABLES;
  
LOCK TABLES `complexity` WRITE;
INSERT INTO `complexity` VALUES (1,'low'),
(2,'below_medium'),(3,'medium'),(4,'above_medium'),(5,'high');
UNLOCK TABLES;

LOCK TABLES `testing`.`language` WRITE;
INSERT INTO `testing`.`language` VALUES (1, 'ru'), (2, 'en');
UNLOCK TABLES;

LOCK TABLES `category` WRITE;
INSERT INTO `testing`.`category` (`id`, `name`) VALUES ('1', 'Информационные технологии');
INSERT INTO `testing`.`category` (`id`, `name`) VALUES ('2', 'Иностранные языки');
UNLOCK TABLES;
  
LOCK TABLES `subject` WRITE;
INSERT INTO `subject` VALUES (1,'Java - основы программирования', 1),
(2,'C++ - основы программирования', 1),(3, 'C# - основы программирования', 1),(4, 'Английский - основы', 2);
UNLOCK TABLES;
  
LOCK TABLES `university` WRITE;
INSERT INTO `university` VALUES (1,'ХНУРЭ - Харьковский национальный университет радиоэлектроники',1),
(2,'НФаУ - Национальный фармацевтический университет',1);
UNLOCK TABLES;

LOCK TABLES `faculty` WRITE;
INSERT INTO `testing`.`faculty` (`id`, `name`, `university_id`) VALUES ('1', 'Копьютерных наук', '1');
UNLOCK TABLES;

LOCK TABLES `group` WRITE;
INSERT INTO `testing`.`group` (`id`, `name`, `faculty_id`) VALUES ('1', 'КН-16-2', '1');
UNLOCK TABLES;

LOCK TABLES `user` WRITE;
INSERT INTO `testing`.`user` (`first_name`, `last_name`, `email`, `role_id`, `login`, `password`) VALUES ('Елена', 'Близнюк', 'iwannikowa.alena@yandex.ua', '1', 'blyzniuk', SHA('blyzniuk'));
INSERT INTO `testing`.`user` (`first_name`, `last_name`, `email`, `login`, `password`) VALUES ('Павел', 'Кравченко', 'kravchenko@yandex.ua', 'kravchenko', SHA('kravchenko'));
UNLOCK TABLES;

LOCK TABLES `test` WRITE;
INSERT INTO `testing`.`test` (`id`, `name`, `subject_id`, `complexity_id`, `free_access`) VALUES ('1', 'Java - основы', '1', '1', 0);
INSERT INTO `testing`.`test` (`id`, `name`, `subject_id`, `complexity_id`, `free_access`) VALUES ('2', 'ООП в Java', '1', '1', 0);
INSERT INTO `testing`.`test` (`id`, `name`, `subject_id`, `complexity_id`, `free_access`) VALUES ('3', 'C# -  Основы', '3', '1', 1);
UNLOCK TABLES;

LOCK TABLES `user_test` WRITE;
-- INSERT INTO `testing`.`user_test` (`user_id`, `test_id`, `date_end`) VALUES ('2', '1', '2017-01-01');
-- INSERT INTO `testing`.`user_test` (`user_id`, `test_id`, `date_end`) VALUES ('2', '2', '2017-01-01');
UNLOCK TABLES;

LOCK TABLES `question` WRITE;
-- test #1
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES (1, 'Является ли пустой файл разрешенным для компиляции файлом исходного кода?', 1, 1);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES (2, 'Какое ключевое слово используется, чтобы показать, что с методом может работать не более чем один поток одновременно?', 1, 2);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES (3, 'Какие из перечисленных объявлений переменных не допустимы в Java?', 1, 3);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES (4, 'Какие реализации java.util.Set сортируют элементы в их естественном порядке (или на основании Comparator\'а)?', 1, 4);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES (5, 'Можно ли переопределяя метод изменить его модификатор доступа с "package-private" на "protected"?', 1, 5);
-- test #2
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('6', 'Как называется \"интерпретация ссылки на объект как ссылки на базовый тип\"?', '2', 1);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('7', 'Общее поведение супер класса может быть передано подкласу с помощью ключевого слова:', '2', 2);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('8', 'Предположим, что класс В наследует класс А. Какие утверждения обязательно верны (укажите все подходящие варианты)?', '2', 3);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('9', 'Какие из следующих утверждений верны для Java 8 ?', '2', 4);
-- test #4
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('10', 'Инкремент и декремент являются унарными операторами.', '3', 1);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('11', 'Что делает оператор %?', '3', 2);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('12', 'Укажите все правильные способы объявления массива:', '3', 3);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('13', 'Среди перечисленных конструкций C# укажите объявление свойства', '3', 4);
INSERT INTO `testing`.`question` (`id`, `text`, `test_id`, `ordinal`) VALUES ('14', 'Какие из следующих выражений выполняют проверку на равенство или эквивалентность:', '3', 5);
UNLOCK TABLES;

LOCK TABLES `answer` WRITE;
-- test #1
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Да', 1, 1, 1);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('Нет', 1, 2);

INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('synchronized', 1, 2, 1);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('sealed', 2, 2);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('volatile', 2, 3);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('locked', 2, 4);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('protected', 2, 5);

INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int if;', 1, 3, 1);
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int else;', 1, 3, 2);
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int goto;', 1, 3, 3);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('int then;', 3, 4);

INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('java.util.TreeSet', 1, 4, 1);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('java.util.HashSet', 4, 2);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('java.util.LinkedHashSet', 4, 3);

INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Да', 1, 5, 1);
INSERT INTO `testing`.`answer` (`text`, `question_id`, `ordinal`) VALUES ('Нет', 5, 2);

INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Нисходящее преобразование', 0, '6', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Восходящее преобразование', 1, '6', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Свободное преобразование', 0, '6', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Полиморфизм', 0, '6', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('inherits', 0, '7', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('implements', 0, '7', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('extend', 0, '7', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('implement', 0, '7', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('extends', 1, '7', '5');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('если класс А имеет только private-конструкторы, то и конструкторы класса В должны быть private', 0, '8', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('если класс А имел статический метод, то метод в классе B с той же сигнатурой тоже должен быть статическим', 1, '8', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('если класс А - абстрактный, то класс В тоже должен быть абстрактным', 0, '8', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('если класс А объявлен как final, то класс В тоже должен быть final', 0, '8', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('если класс А объявлен как public, то и класс В должен быть public', 0, '8', '5');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Интерфейс может содержать protected и static члены, а абстрактный класс нет.', 0, '9', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Интерфейс может быть частично реализован, а абстрактный класс нет.', 0, '9', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('В отличие от интерфейсов, множественное наследование классов допускается.', 0, '9', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Абстрактный класс может содержать protected и static члены, а интерфейс нет.', 1, '9', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('В отличие от классов, множественное наследование интерфейсов допускается.', 1, '9', '5');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Абстрактный класс может быть частично реализован, а интерфейс нет.', 0, '9', '6');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Да', 1, '10', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Нет', 0, '10', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Возвращает остаток от деления', 1, '11', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Переводит дробное число в проценты', 0, '11', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Возвращает процентное соотношение двух операндов', 0, '11', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('Форматирует значения разных типов в строку', 0, '11', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int k[];', 0, '12', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int k[3];', 0, '12', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int[] k;', 1, '12', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int[3] k;', 0, '12', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('int[] k = new int [3];', 1, '12', '5');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('string Name {get{return “Name”;}}', 1, '13', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('string Name;', 0, '13', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('sting GetName() { return “Name”;} ', 0, '13', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('string this [int i] {get {return “Name”;}}', 0, '13', '4');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('a == b ', 1, '14', '1');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('a = = = b', 0, '14', '2');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('a.Equals(b)', 1, '14', '3');
INSERT INTO `testing`.`answer` (`text`, `correct`, `question_id`, `ordinal`) VALUES ('a = b', 0, '14', '4');

UNLOCK TABLES;



