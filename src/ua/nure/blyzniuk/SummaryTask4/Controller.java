package ua.nure.blyzniuk.SummaryTask4;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandContainer;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;

/**
 * Servlet implementation class Controller
 * 
 * @author Elena Blyzniuk
 */
@WebServlet(name = "controller", urlPatterns = { "/controller" })
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger LOG = Logger.getLogger(Controller.class);

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	/**
	 * Main method of this controller.
	 */
	private void process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		LOG.debug("Controller starts");
		
		HttpSession session = request.getSession();

		// extract command name from the request
		String commandName = request.getParameter("command");
		LOG.trace("Request parameter: command --> " + commandName);

		// obtain command object by its name
		Command command = CommandContainer.get(commandName);
		LOG.trace("Obtained command --> " + command);

		// execute command and get forward address
		CommandHelper helper = new CommandHelper();
		try {
			helper = command.execute(request, response);
		} catch (AppException ex) {
			helper = command.getTargetOperation();
			session.setAttribute("errorMessage", ex.getMessage());
		}
		String forward = helper.getPage();

		LOG.trace("Target address --> " + forward);
		LOG.debug("Controller finished, now go to address --> " + forward);

		if (helper.isRedirect()) {
			response.sendRedirect(getServletContext().getContextPath() + forward);
		} else {
			request.getRequestDispatcher(forward).forward(request, response);
		}
	}
}
