package ua.nure.blyzniuk.SummaryTask4;

/**
 * Path holder (jsp pages, controller commands).
 * 
 * @author Elena Blyzniuk
 */
public final class Path {

	// common pages
	public static final String PAGE_MAIN = "/index.jsp";
	public static final String PAGE_LOGIN = "/WEB-INF/jsp/common/login.jsp";
	public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/common/error.jsp";
	public static final String PAGE_REGISTRATION = "/WEB-INF/jsp/common/registration.jsp";
	public static final String PAGE_PERSONAL_ACCOUNT = "/WEB-INF/jsp/common/personal_account.jsp";
	public static final String PAGE_CHANGE_PASSWORD = "/WEB-INF/jsp/common/change_password.jsp";
	public static final String PAGE_SETTINGS = "/WEB-INF/jsp/common/settings.jsp";
	public static final String PAGE_CHANGE_LOCALE = "/WEB-INF/jsp/common/change_locale.jsp";
	public static final String PAGE_TEST_INFO = "/WEB-INF/jsp/common/test_info.jsp";
	public static final String PAGE_SEARCH_TESTS = "/WEB-INF/jsp/common/search_tests.jsp";
	public static final String PAGE_EDIT_USER = "/WEB-INF/jsp/common/edit_user.jsp";

	// student pages
	public static final String PAGE_OBLIGATORY_TESTS = "/WEB-INF/jsp/student/obligatory_tests.jsp";
	public static final String PAGE_QUESTION = "/WEB-INF/jsp/common/question.jsp";
	public static final String PAGE_TEST_RESULT = "/WEB-INF/jsp/student/test_result.jsp";

	// educator and student
	public static final String PAGE_ADD_INSTITUTION = "/WEB-INF/jsp/educator_student/add_institution.jsp";

	// admin and educator
	public static final String PAGE_CONFIRM_USERS = "/WEB-INF/jsp/admin_educator/confirm_users.jsp";

	// educator pages
	public static final String PAGE_STUDENT_RATING = "/WEB-INF/jsp/educator/student_rating.jsp";
	public static final String PAGE_SET_TESTS = "/WEB-INF/jsp/educator/set_tests.jsp";

	// admin pages
	public static final String PAGE_EDIT_INSTITUTION = "/WEB-INF/jsp/admin/edit_institution.jsp";
	public static final String PAGE_EDIT_FACULTY = "/WEB-INF/jsp/admin/edit_faculty.jsp";
	public static final String PAGE_EDIT_CATEGORY = "/WEB-INF/jsp/admin/edit_category.jsp";
	public static final String PAGE_EDIT_TEST = "/WEB-INF/jsp/admin/edit_test.jsp";
	public static final String PAGE_EDIT_SUBJECT = "/WEB-INF/jsp/admin/edit_subject.jsp";
	public static final String PAGE_EDIT_GROUP = "/WEB-INF/jsp/admin/edit_group.jsp";
	public static final String PAGE_CREATE_INSTITUTION = "/WEB-INF/jsp/admin/create_institution.jsp";
	public static final String PAGE_CREATE_FACULTY = "/WEB-INF/jsp/admin/create_faculty.jsp";
	public static final String PAGE_CREATE_GROUP = "/WEB-INF/jsp/admin/create_group.jsp";
	public static final String PAGE_CREATE_CATEGORY = "/WEB-INF/jsp/admin/create_category.jsp";
	public static final String PAGE_CREATE_SUBJECT = "/WEB-INF/jsp/admin/create_subject.jsp";
	public static final String PAGE_CREATE_TEST = "/WEB-INF/jsp/admin/create_test.jsp";
	public static final String PAGE_CREATE_QUESTION = "/WEB-INF/jsp/admin/create_question.jsp";
	public static final String PAGE_SEARCH_USERS = "/WEB-INF/jsp/admin/search_users.jsp";
	public static final String PAGE_SEARCH_INSTITUTIONS = "/WEB-INF/jsp/admin/search_institutions.jsp";
	public static final String PAGE_SEARCH_FACULTIES = "/WEB-INF/jsp/admin/search_faculties.jsp";
	public static final String PAGE_SEARCH_GROUPS = "/WEB-INF/jsp/admin/search_groups.jsp";
	public static final String PAGE_SEARCH_CATEGORIES = "/WEB-INF/jsp/admin/search_categories.jsp";
	public static final String PAGE_SEARCH_SUBJECTS = "/WEB-INF/jsp/admin/search_subjects.jsp";
	public static final String PAGE_TEST_RATING = "/WEB-INF/jsp/admin/test_rating.jsp";

	// commands
	public static final String COMMAND_START_STUDENT = "/controller?command=showObligatoryTestsPage";
	public static final String COMMAND_START_EDUCATOR = "/controller?command=showConfirmUsersPage";
	public static final String COMMAND_START_ADMIN = "/controller?command=showConfirmUsersPage";
	public static final String COMMAND_REGISTER = "/controller?command=register";
	public static final String COMMAND_LOGOUT = "/controller?command=logout";
	public static final String COMMAND_LOGIN = "/controller?command=login";
	public static final String COMMAND_SHOW_LOGIN_PAGE = "/controller?command=showLoginPage";
	public static final String COMMAND_SHOW_TEST_INFO = "/controller?command=showTestInfo";
	public static final String COMMAND_PERFORM_TEST = "/controller?command=chooseQuestion&questionOrdinal=";
	public static final String COMMAND_SHOW_CHANGE_PASSWORD_PAGE = "/controller?command=showChangePasswordPage";
	public static final String COMMAND_SHOW_PERSONAL_ACCOUNT = "/controller?command=showPersonalAccount";
	public static final String COMMAND_SHOW_REGISTRATION_PAGE = "/controller?command=showRegistrationPage";
	public static final String COMMAND_SHOW_EDIT_USER_PAGE = "/controller?command=showEditUserPage";
	public static final String COMMAND_SEARCH_USERS = "/controller?command=searchUsers";
	public static final String COMMAND_CREATE_INSTITUTION = "/controller?command=createInstitution";
	public static final String COMMAND_CREATE_GROUP = "/controller?command=createGroup";
	public static final String COMMAND_SHOW_CREATE_CATEGORY_PAGE = "/controller?command=showCreateCategoryPage";
	public static final String COMMAND_SHOW_CREATE_FACULTY_PAGE = "/controller?command=showCreateFacultyPage";
	public static final String COMMAND_SHOW_CREATE_SUBJECT_PAGE = "/controller?command=showCreateSubjectPage";
	public static final String COMMAND_SHOW_CREATE_INSTITUTION_PAGE = "/controller?command=showCreateInstitutionPage";
	public static final String COMMAND_SHOW_EDIT_CATEGORY_PAGE = "/controller?command=showEditCategoryPage";
	public static final String COMMAND_SHOW_EDIT_FACULTY_PAGE = "/controller?command=showEditFacultyPage";
	public static final String COMMAND_SHOW_EDIT_INSTITUTION_PAGE = "/controller?command=showEditInstitutionPage";
	public static final String COMMAND_SHOW_EDIT_SUBJECT_PAGE = "/controller?command=showEditSubjectPage";
	public static final String COMMAND_SHOW_EDIT_GROUP_PAGE = "/controller?command=showEditGroupPage";
	public static final String COMMAND_SHOW_EDIT_TEST_PAGE = "/controller?command=showEditTestPage";
	public static final String COMMAND_SHOW_SEARCH_CATEGORIES_PAGE = "/controller?command=showSearchCategoriesPage";
	public static final String COMMAND_SHOW_SEARCH_FACULTIES_PAGE = "/controller?command=showSearchFacultiesPage";
	public static final String COMMAND_SHOW_SEARCH_GROUPS_PAGE = "/controller?command=showSearchGroupsPage";
	public static final String COMMAND_SHOW_SEARCH_INSTITUTIONS_PAGE = "/controller?command=showSearchInstitutionsPage";
	public static final String COMMAND_SHOW_SEARCH_SUBJECTS_PAGE = "/controller?command=showSearchSubjectsPage";
	public static final String COMMAND_SHOW_SEARCH_USERS_PAGE = "/controller?command=showSearchUsersPage";
	public static final String COMMAND_SHOW_CREATE_GROUP_PAGE = "/controller?command=showCreateGroupPage";
	public static final String COMMAND_SHOW_CONFIRM_USERS_PAGE = "/controller?command=showConfirmUsersPage";
	public static final String COMMAND_SHOW_CREATE_QUESTION_PAGE = "/controller?command=showCreateQuestionPage";
	public static final String COMMAND_SEARCH_TESTS = "/controller?command=searchTests";
	public static final String COMMAND_SHOW_TEST_RESULT = "/controller?command=showTestResult";
	public static final String COMMAND_SHOW_TEST_INFO_PAGE = "/controller?command=showTestInfoPage";
	public static final String COMMAND_SHOW_SET_TESTS_PAGE = "/controller?command=showSetTestsPage";
	public static final String COMMAND_SHOW_ADD_INSTITUTION_PAGE = "/controller?command=showAddInstitutionPage";
	public static final String COMMAND_CONFIRM_CREATE_QUESTION = "/controller?command=confirmCreateQuestion";
	public static final String COMMAND_SHOW_CREATE_TEST_PAGE = "/controller?command=showCreateTestPage";

}