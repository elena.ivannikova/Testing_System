package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.io.Serializable;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Answer;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Question;

/**
 * Container for question. Contains answers and information about question.
 * It is used when passing tests, creating tests and viewing information about test.
 * 
 * @author Elena Blyzniuk
 */
public class QuestionAnswerBean implements Serializable {
	
	private static final long serialVersionUID = 4178960878938365076L;
	
	Question question;
	List<Answer> answers;
	int answerNumber;
	Boolean confirmed = false;
	
	public Boolean getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}
	public int getAnswerNumber() {
		return answers.size();
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public List<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
}
