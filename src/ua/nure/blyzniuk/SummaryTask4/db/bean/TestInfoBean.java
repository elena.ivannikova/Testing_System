package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.sql.Time;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Provide records for virtual table:
 * <pre>
 * |test.id|test.name|subject.name|test.duration|complexity.name|question_number|free_access
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class TestInfoBean extends Identify<Long> implements Comparable<TestInfoBean> {
	
	private static final long serialVersionUID = 4511186918495366153L;

	private String testName;
	private String subjectName;
	private Time duration;
	private String complexityName;
	private int questionNumber;
	private Boolean freeAccess;

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public String getComplexityName() {
		return complexityName;
	}

	public void setComplexityName(String complexityName) {
		this.complexityName = complexityName;
	}

	public int getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(int questionNumber) {
		this.questionNumber = questionNumber;
	}

	public Boolean getFreeAccess() {
		return freeAccess;
	}

	public void setFreeAccess(Boolean freeAccess) {
		this.freeAccess = freeAccess;
	}
	
	@Override
	public int compareTo(TestInfoBean o) {
		return testName.compareTo(o.testName);
	}
}
