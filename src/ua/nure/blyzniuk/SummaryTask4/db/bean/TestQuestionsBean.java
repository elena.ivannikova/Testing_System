package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.io.Serializable;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Test;

/**
 * Container for test. Contains questions, answers and information about test.
 * It is used when passing tests, creating tests and viewing information about test.
 * 
 * @author Elena Blyzniuk
 */
public class TestQuestionsBean implements Serializable {

	private static final long serialVersionUID = 6839495719079796252L;
	
	private Long userTestId;
	private Test test;
	private int questionNumber;
	private List<QuestionAnswerBean> questions;

	public int getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(int questionNumber) {
		this.questionNumber = questionNumber;
	}

	public Long getUserTestId() {
		return userTestId;
	}

	public void setUserTestId(Long userTestId) {
		this.userTestId = userTestId;
	}

	public List<QuestionAnswerBean> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionAnswerBean> questions) {
		this.questions = questions;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public QuestionAnswerBean getQuestionByOrdinal(int ordinal) {
		QuestionAnswerBean question = null;
		for (QuestionAnswerBean questionAnswerBean : questions) {
			if (questionAnswerBean.getQuestion().getOrdinal() == ordinal) {
				question = questionAnswerBean;
			}
		}
		return question;
	}
}
