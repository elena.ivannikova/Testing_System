package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.io.Serializable;

/**
 * Provide records for virtual table:
 * <pre>
 * |group.name|faculty.name|university.name|
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class UserGroupBean implements Serializable {
	
	private static final long serialVersionUID = -3703682353749683289L;
	
	private String groupName;
	private String facultyName;
	private String universityName;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getFacultyName() {
		return facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}
}
