package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.io.Serializable;

/**
 * Provide records for virtual table:
 * <pre>
 * |user.id|group.id|user.first_name|user.last_name|group.name|faculty.name|university.name|
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class UserGroupDetailBean implements Serializable {
	
	private static final long serialVersionUID = -4135268110236264175L;
	
	private long userId;
	private long groupId;
	private String userFirstName;
	private String userLastName;
	private String groupName;
	private String facultyName;
	private String universityName;

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getFacultyName() {
		return facultyName;
	}

	public void setFacultyName(String facultyName) {
		this.facultyName = facultyName;
	}

	public String getUniversityName() {
		return universityName;
	}

	public void setUniversityName(String universityName) {
		this.universityName = universityName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	@Override
	public String toString() {
		return "" + userId + " " + groupId;
	}
}
