package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.sql.Date;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Provide records for virtual table:
 * <pre>
 * |user.id|user.first_name|user.last_name|user.email|user.login|user.registr_date|role.name|
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class UserInfoBean extends Identify<Long> {

	private static final long serialVersionUID = 1047404241841269248L;
	
	private String firstName;
	private String lastName;
	private String email;
	private String login;
	private Date registrDate;
	private String roleName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Date getRegistrDate() {
		return registrDate;
	}

	public void setRegistrDate(Date registrDate) {
		this.registrDate = registrDate;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}
