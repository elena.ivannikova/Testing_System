package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * Provide records for virtual table:
 * <pre>
 * |test.id|test.name|subject.name|user_test.date_start|user_test.date_end|complexity.name|test.duration|question_number|deadline|
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class UserObligatoryTestsBean implements Serializable {
	
	private static final long serialVersionUID = 2933917060928531833L;
	
	private Long testId;
	private String testName;
	private String subjectName;
	private Date dateStart;
	private Date dateEnd;
	private String complexityName;
	private Time duration;
	private int questionNumber;
	private boolean deadline;
	
	public boolean isDeadline() {
		return deadline;
	}
	public void setDeadline(boolean deadline) {
		this.deadline = deadline;
	}
	public Long getTestId() {
		return testId;
	}
	public void setTestId(Long testId) {
		this.testId = testId;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Date getDateStart() {
		return dateStart;
	}
	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}
	public Date getDateEnd() {
		return dateEnd;
	}
	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}
	public String getComplexityName() {
		return complexityName;
	}
	public void setComplexityName(String complexityName) {
		this.complexityName = complexityName;
	}
	public Time getDuration() {
		return duration;
	}
	public void setDuration(Time duration) {
		this.duration = duration;
	}
	public int getQuestionNumber() {
		return questionNumber;
	}
	public void setQuestionNumber(int questionNumber) {
		this.questionNumber = questionNumber;
	}
	
}
