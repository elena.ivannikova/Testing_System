package ua.nure.blyzniuk.SummaryTask4.db.bean;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

/**
 * Provide records for virtual table:
 * <pre>
 * |test.name|subject.name|user_test.date_start|user_test.date_end|complexity.name|test.duration|question_number|deadline|
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class UserPassedTestsBean implements Serializable {

	private static final long serialVersionUID = 2636820636118640735L;
	
	private String testName;
	private String subjectName;
	private Date datePass;
	private String complexityName;
	private Time duration;
	private int questionNumber;
	private Boolean freeAccess;
	private double testResult;

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public int getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(int questionNumber) {
		this.questionNumber = questionNumber;
	}

	public Boolean getFreeAccess() {
		return freeAccess;
	}

	public void setFreeAccess(Boolean freeAccess) {
		this.freeAccess = freeAccess;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Date getDatePass() {
		return datePass;
	}

	public void setDatePass(Date datePass) {
		this.datePass = datePass;
	}

	public String getComplexityName() {
		return complexityName;
	}

	public void setComplexityName(String complexityName) {
		this.complexityName = complexityName;
	}

	public double getTestResult() {
		return testResult;
	}

	public void setTestResult(double testResult) {
		this.testResult = testResult;
	}
}
