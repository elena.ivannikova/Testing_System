package ua.nure.blyzniuk.SummaryTask4.db.bean;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Provide records for virtual table:
 * <pre>
 * |user_test.id|test.id|user.id|group.id|user.first_name|user.last_name|group.name|test.name|subject.name|test_result|deadline|
 * </pre>
 * 
 * @author Elena Blyzniuk
 */
public class UserTestResultsBean extends Identify<Long> implements Comparable<UserTestResultsBean> {

	private static final long serialVersionUID = -2860167406359148664L;

	private Long testId;
	private Long userId;
	private long groupId;
	private String firstName;
	private String lastName;
	private String groupName;
	private String testName;
	private String subjectName;
	private double testResult;
	private boolean deadline;

	public boolean isDeadline() {
		return deadline;
	}

	public void setDeadline(boolean deadline) {
		this.deadline = deadline;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public double getTestResult() {
		return testResult;
	}

	public void setTestResult(double testResult) {
		this.testResult = testResult;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public int compareTo(UserTestResultsBean o) {
		return testName.compareTo(o.testName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserTestResultsBean other = (UserTestResultsBean) obj;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserTestResultsBean [id=" + id + ", testId=" + testId + ", userId=" + userId + ", groupId=" + groupId
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", groupName=" + groupName + ", testName="
				+ testName + ", subjectName=" + subjectName + ", testResult=" + testResult + "]";
	}
}
