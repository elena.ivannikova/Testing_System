package ua.nure.blyzniuk.SummaryTask4.db.bean.comparator;

/**
 * Complexity entity.
 * 
 * @author Elena Blyzniuk
 */
public enum Complexity {
	LOW("low"), BELOW_MEDIUM("below_medium"), MEDIUM("medium"), ABOVE_MEDIUM("above_medium"), HIGH("high");
	
	private String value;
	
	private Complexity(String v) {
		value = v;
	}

	public static Complexity fromValue(String v) {
	for (Complexity c : Complexity.values()) {
		if (c.value.equals(v)) {
			return c;
		}
	}
	throw new IllegalArgumentException(v);
}

	public String getName() {
		return name().toLowerCase();
	}
}
