package ua.nure.blyzniuk.SummaryTask4.db.bean.comparator;

/**
 * Enum for test comparators.
 * 
 * @author Elena Blyzniuk
 */
public enum TestComparator {
	COMPLEXITY, QUESTIONS, NAME;
	
	public static String[] getValues(){
		String[] str = new String[TestComparator.values().length];
		for (int i = 0; i < str.length; i++) {
			str[i] = TestComparator.values()[i].toString();
		}
		return str;
	}
}
