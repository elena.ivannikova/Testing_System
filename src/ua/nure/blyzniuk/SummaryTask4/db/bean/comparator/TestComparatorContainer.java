package ua.nure.blyzniuk.SummaryTask4.db.bean.comparator;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;

/**
 * Container for test comparators.
 * 
 * @author Elena Blyzniuk
 */
public class TestComparatorContainer {

	private static final Logger LOG = Logger.getLogger(TestComparatorContainer.class);

	private static Map<TestComparator, Comparator<TestInfoBean>> comparators = new TreeMap<>();

	static {
		
		comparators.put(TestComparator.COMPLEXITY, new TestComplexityComparator());
		comparators.put(TestComparator.QUESTIONS, new TestQuestionNumberComparator());
		comparators.put(TestComparator.NAME, new TestNameComparator());
		
		LOG.debug("Comparator container was successfully initialized");
		LOG.trace("Number of comparators --> " + comparators.size());
	}
	
	/**
	 * Returns test comparator from the container.
	 * 
	 * @param comparator a comparator to find in container
	 * @return comparator from the container
	 */
	public static Comparator<TestInfoBean> get(TestComparator comparator) {
		if (comparator == null || !comparators.containsKey(comparator)) {
			LOG.trace("Comparator not found, name --> " + comparator);
			return null;//comparators.get("noComparator");
		}
		return comparators.get(comparator);
	}

}
