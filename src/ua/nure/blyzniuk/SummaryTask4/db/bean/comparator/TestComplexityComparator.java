package ua.nure.blyzniuk.SummaryTask4.db.bean.comparator;

import java.util.Comparator;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;

/**
 * Provides test comparator by complexity.
 * 
 * @author Elena Blyzniuk
 */
public class TestComplexityComparator implements Comparator<TestInfoBean> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(TestInfoBean o1, TestInfoBean o2) {
		int result = Complexity.fromValue(o1.getComplexityName()).ordinal() - Complexity.fromValue(o2.getComplexityName()).ordinal();
		if (result == 0) {
			result = o1.compareTo(o2);
		}
		return result;
	}
}
