package ua.nure.blyzniuk.SummaryTask4.db.bean.comparator;

import java.util.Comparator;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;

/**
 * Provides test comparator by name.
 * 
 * @author Elena Blyzniuk
 */
public class TestNameComparator implements Comparator<TestInfoBean> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(TestInfoBean o1, TestInfoBean o2) {
		return o1.compareTo(o2);
	}
}
