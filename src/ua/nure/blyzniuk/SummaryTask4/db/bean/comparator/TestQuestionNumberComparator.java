package ua.nure.blyzniuk.SummaryTask4.db.bean.comparator;

import java.util.Comparator;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;

/**
 * Provides test comparator by questions number.
 * 
 * @author Elena Blyzniuk
 */
public class TestQuestionNumberComparator implements Comparator<TestInfoBean> {

	/**
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(TestInfoBean o1, TestInfoBean o2) {
		int result = o1.getQuestionNumber() - o2.getQuestionNumber();
		if (result == 0) {
			result = o1.compareTo(o2);
		}
		return result;
	}
}
