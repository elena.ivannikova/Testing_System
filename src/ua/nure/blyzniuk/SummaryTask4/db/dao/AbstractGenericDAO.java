/**
 * 
 */
package ua.nure.blyzniuk.SummaryTask4.db.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.mysql.DBManagerImpl;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * An implementation for entities state management.
 * 
 * @author Elena Blyzniuk
 *
 * @param <T> type of entity
 * @param <PK> type of primary key
 */
public abstract class AbstractGenericDAO<T extends Serializable, PK extends Serializable> implements GenericDAO<T, PK> {
	
	private static final Logger LOG = Logger.getLogger(AbstractGenericDAO.class);

	/**
	 * Provides connection to database.
	 */
	protected DBManagerImpl dbManager = DBManagerImpl.getInstance();

	/**
	 * Return query to select by primary key.
	 * 
	 * @return query to select by primary key.
	 */
	public String getSelectByPKQuery() {
		return getSelectQuery() + "WHERE id=? ";
	}

	/**
	 * Return query to select by foreign key.
	 * 
	 * @return query to select by foreign key.
	 */
	public abstract String getSelectByFKQuery();

	/**
	 * Returns select query.
	 * 
	 * @return select query
	 */
	public abstract String getSelectQuery();

	/**
	 * Returns create query.
	 * 
	 * @return create query
	 */
	public abstract String getCreateQuery();

	/**
	 * Returns update query.
	 * 
	 * @return update query
	 */
	public abstract String getUpdateQuery();

	/**
	 * Returns delete query.
	 * 
	 * @return delete query
	 */
	public abstract String getDeleteQuery();

	/**
	 * Extracts entity from the result set.
	 * 
	 * @param rs result set to extract entity
	 * @return entity extracted from result set
	 * @throws DBException
	 */
	protected abstract T extract(ResultSet rs) throws DBException;

	/**
	 * Parses result set obtained as a result of a query.
	 * 
	 * @param rs result set to be parsed
	 * @return list of entities obtained as a result of query
	 * @throws DBException
	 */
	public List<T> parseResultSet(ResultSet rs) throws DBException {
		List<T> result = new LinkedList<T>();
		try {
			while (rs.next()) {
				result.add(extract(rs));
			}
		} catch (Exception e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return result;
	}

	/**
	 * Prepares statement for insert.
	 * 
	 * @param statement a statement to be prepared
	 * @param object an object to be inserted
	 * @throws DBException
	 */
	protected abstract void prepareStatementForInsert(PreparedStatement statement, T object) throws DBException;

	/**
	 * Prepares statement for update.
	 * 
	 * @param statement a statement to be prepared
	 * @param object an object to be updated
	 * @throws DBException
	 */
	protected abstract void prepareStatementForUpdate(PreparedStatement statement, T object) throws DBException;

	/**
	 * Prepares statement for delete.
	 * 
	 * @param statement a statement to be prepared
	 * @param object an object to be deleted
	 * @throws DBException
	 */
	protected abstract void prepareStatementForDelete(PreparedStatement statement, T object) throws DBException;

	/**
	 * Prepares statement for select.
	 * 
	 * @param statement a statement to be prepared
	 * @param key primary key
	 * @throws DBException
	 */
	protected void prepareStatementForSelect(PreparedStatement statement, PK key) throws DBException {
		try {
			statement.setLong(1, (long) key);
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.GenericDAO#persist(java.io.Serializable)
	 */
	@Override
	public T persist(T object) throws DBException {
		T persistInstance = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = getCreateQuery();
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			prepareStatementForInsert(statement, object);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DBException("On persist modify more then 1 record: " + count);
			}
			statement = con.prepareStatement(getSelectQuery() + "WHERE id=last_insert_id();");
			rs = statement.executeQuery();
			List<T> list = parseResultSet(rs);
			if ((list == null) || (list.size() != 1)) {
				throw new DBException("Exception on findByPK new persist data.");
			}
			persistInstance = list.iterator().next();
			con.commit();
		} catch (Exception e) {
			LOG.error(e);
			dbManager.rollback(con);  
			String s = "Error while creating.";
			if (e.getLocalizedMessage().matches("Duplicate entry '.*' for key '.*'")) {
				Matcher m = Pattern.compile("(?m)(?<=(.*key ))'.*?'$").matcher(e.getLocalizedMessage());
				if (m.find()) {
					s = m.group();
				}
			}
			throw new DBException(s);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return persistInstance;
	}

	/**
	 * Obtains entities by foreign key.
	 * 
	 * @param key foreign key
	 * @return entity obtained by foreign key
	 * @throws DBException
	 */
	public List<T> getByFK(PK key) throws DBException {
		List<T> list;
		String sql = getSelectByFKQuery();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			prepareStatementForSelect(statement, key);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			LOG.error(e);
			dbManager.rollback(con); 
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.GenericDAO#getByPK(java.io.Serializable)
	 */
	@Override
	public T getByPK(PK key) throws DBException {
		List<T> list;
		String sql = getSelectByPKQuery();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			prepareStatementForSelect(statement, key);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			LOG.error(e);
			dbManager.rollback(con); 
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		if (list.size() > 1) {
			throw new DBException("Received more than one record.");
		}
		return list.iterator().next();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.GenericDAO#update(java.io.Serializable)
	 */
	@Override
	public boolean update(T object) throws DBException {
		boolean result = false;
		String sql = getUpdateQuery();
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			prepareStatementForUpdate(statement, object);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DBException("On update modify more then 1 record: " + count);
			}
			con.commit();
			result = true;
		} catch (Exception e) {
			LOG.error(e);
			dbManager.rollback(con); 
			String s = "Error while modifying data.";
			if (e.getLocalizedMessage().matches("Duplicate entry '.*' for key '.*'")) {
				Matcher m = Pattern.compile("(?m)(?<=(.*key ))'.*?'$").matcher(e.getLocalizedMessage());
				if (m.find()) {
					s = m.group();
				}
			}
			throw new DBException(s);
		} finally {
			dbManager.close(statement);
			dbManager.close(con);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.GenericDAO#delete(java.io.Serializable)
	 */
	@Override
	public boolean delete(T object) throws DBException {
		String sql = getDeleteQuery();
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			prepareStatementForDelete(statement, object);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DBException("On delete modify more then 1 record: " + count);
			}
		} catch (Exception e) {
			LOG.error(e);
			dbManager.rollback(con); 
			throw new DBException(e);
		} finally {
			dbManager.close(statement);
			dbManager.close(con);
		}
		return true;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.GenericDAO#getAll()
	 */
	@Override
	public List<T> getAll() throws DBException {
		List<T> list;
		String sql = getSelectQuery();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			LOG.error(e);
			dbManager.rollback(con); 
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

	/**
	 * Obtains connection from the pool.
	 * 
	 * @return connection from the pool
	 * @throws DBException
	 */
	protected Connection getConnection() throws DBException {
		return DBManagerImpl.getInstance().getConnection();
	}
}
