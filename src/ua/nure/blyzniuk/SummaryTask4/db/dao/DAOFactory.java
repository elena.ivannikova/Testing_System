package ua.nure.blyzniuk.SummaryTask4.db.dao;

import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.*;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl.*;

/**
 * A factory to obtain DAO objects.
 * 
 * @author Elena Blyzniuk
 *
 */
public class DAOFactory {

	/**
	 * Returns answer DAO.
	 * 
	 * @return answer DAO
	 */
	public static AnswerDAO getAnswerDAO() {
		return new AnswerDAO();
	}

	/**
	 * Returns category DAO.
	 * 
	 * @return category DAO
	 */
	public static CategoryDAO getCategoryDAO() {
		return new CategoryDAOImpl();
	}

	/**
	 * Returns complexity DAO.
	 * 
	 * @return complexity DAO
	 */
	public static ComplexityDAO getComplexityDAO() {
		return new ComplexityDAO();
	}

	/**
	 * Returns country DAO.
	 * 
	 * @return country DAO
	 */
	public static CountryDAO getCountryDAO() {
		return new CountryDAOImpl();
	}

	/**
	 * Returns faculty DAO.
	 * 
	 * @return faculty DAO
	 */
	public static FacultyDAO getFacultyDAO() {
		return new FacultyDAOImpl();
	}

	/**
	 * Returns group DAO.
	 * 
	 * @return group DAO
	 */
	public static GroupDAO getGroupDAO() {
		return new GroupDAOImpl();
	}

	/**
	 * Returns language DAO.
	 * 
	 * @return language DAO
	 */
	public static LanguageDAO getLanguageDAO() {
		return new LanguageDAOImpl();
	}

	/**
	 * Returns question DAO.
	 * 
	 * @return question DAO
	 */
	public static QuestionDAO getQuestionDAO() {
		return new QuestionDAO();
	}

	/**
	 * Returns subject DAO.
	 * 
	 * @return subject DAO
	 */
	public static SubjectDAO getSubjectDAO() {
		return new SubjectDAOImpl();
	}

	/**
	 * Returns test DAO.
	 * 
	 * @return test DAO
	 */
	public static TestDAO getTestDAO() {
		return new TestDAOImpl();
	}

	/**
	 * Returns university DAO.
	 * 
	 * @return university DAO
	 */
	public static UniversityDAO getUniversityDAO() {
		return new UniversityDAOImpl();
	}

	/**
	 * Returns user group DAO.
	 * 
	 * @return user group DAO
	 */
	public static UserGroupDAO getUserGroupDAO() {
		return new UserGroupDAOImpl();
	}

//	/**
//	 * Returns user test answer DAO.
//	 * 
//	 * @return user test answer DAO
//	 */
//	public static UserTestAnswerDAO getUserTestAnswerDAO() {
//		return new UserTestAnswerDAO();
//	}
	
	/**
	 * Returns user DAO.
	 * 
	 * @return user DAO
	 */
	public static UserDAO getUserDAO() {
		return new UserDAOImpl();
	}

	/**
	 * Returns user test DAO.
	 * 
	 * @return user test DAO
	 */
	public static UserTestDAO getUserTestDAO() {
		return new UserTestDAOImpl();
	}
}
