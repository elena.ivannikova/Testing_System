package ua.nure.blyzniuk.SummaryTask4.db.dao;

import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides connection to database.
 * 
 * @author Elena Blyzniuk
 *
 * @param <Context> connection to database
 */
public interface DBManager<Context> {
	
	/**
	 * Returns connection to database.
	 * 
	 * @return connection to database
	 * @throws DBException
	 */
	Context getConnection() throws DBException;
	
	/**
	 * Closes connection to database.
	 * 
	 * @param con connection to close.
	 */
	void close(Context con);
	
}
