package ua.nure.blyzniuk.SummaryTask4.db.dao;

/**
 * Root of all entities which have the field name.
 * 
 * @author Elena Blyzniuk
 */
public class Entity extends Identify<Long> {

	private static final long serialVersionUID = 3356919747838338400L;

	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
