package ua.nure.blyzniuk.SummaryTask4.db.dao;

import java.io.Serializable;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Interface for entities state management.
 * 
 * @author Elena Blyzniuk
 *
 * @param <T> type of entity
 * @param <PK> type of primary key
 */
public interface GenericDAO<T extends Serializable, PK extends Serializable> {
	
	/**
	 * Create new record in the table.
	 * 
	 * @param object entity to be inserted
	 * @return inserted object
	 * @throws DBException
	 */
	public T persist(T object) throws DBException;

	/**
	 * Obtains entity by primary key.
	 * 
	 * @param key primary key
	 * @return entity obtained by primary key
	 * @throws DBException
	 */
	public T getByPK(PK key) throws DBException;
	
	/**
	 * Updates record in the table.
	 * 
	 * @param object entity to be modified
	 * @return true if update was successful
	 * @throws DBException
	 */
	public boolean update(T object) throws DBException;

	/**
	 * Deletes record from the table
	 * 
	 * @param object entity to be deleted
	 * @return true if deletion was successful
	 * @throws DBException
	 */
	public boolean delete(T object) throws DBException;

	/**
	 * Obtains all records from the table.
	 * 
	 * @return list of entities form the table.
	 * @throws DBException
	 */
	public List<T> getAll() throws DBException;
	
}
