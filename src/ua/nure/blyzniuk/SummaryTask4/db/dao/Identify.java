package ua.nure.blyzniuk.SummaryTask4.db.dao;

import java.io.Serializable;

/**
 * Basic class for entities.
 * 
 * @author Elena Blyzniuk
 *
 * @param <T> parameter for primary key
 */
public class Identify<T extends Serializable> implements Serializable {
	
	private static final long serialVersionUID = -8762412104057066893L;
	
	protected T id;

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}
}
