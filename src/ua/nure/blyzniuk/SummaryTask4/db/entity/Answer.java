package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Answer entity.
 * 
 * @author Elena Blyzniuk
 */
public class Answer extends Identify<Long> {

	private static final long serialVersionUID = -5822098760232225692L;
	
	private String text;
	private Boolean correct;
	private long questionId;
	private int ordinal;

	public Answer() {
	}

	public Answer(Answer answer) {
		this.id = answer.id;
		this.text = answer.text;
		this.questionId = answer.questionId;
	}

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean getCorrect() {
		return correct;
	}

	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}

	public long getQuestionId() {
		return questionId;
	}

	public void setQuestionId(long questionId) {
		this.questionId = questionId;
	}

	@Override
	public String toString() {
		return "Answer [text=" + text + ", correct=" + correct + ", questionId=" + questionId + ", ordinal=" + ordinal
				+ "]";
	}
}
