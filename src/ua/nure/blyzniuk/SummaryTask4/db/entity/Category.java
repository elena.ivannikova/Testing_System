package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * Category entity.
 * 
 * @author Elena Blyzniuk
 */
public class Category extends Entity {
	
	private static final long serialVersionUID = 413074230223643159L;

	private int languageId;

	public int getLanguageId() {
		return languageId;
	}

	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	
	public static Category createCategory(String categoryName, int languageId) {
		Category category = new Category();
		category.setName(categoryName);
		category.setLanguageId(languageId);
		return category;
	}
	
	public static Category createCategory(long id, String categoryName) {
		Category category = new Category();
		category.setId(id);
		category.setName(categoryName);
		return category;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + "]";
	}
}
