package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * Complexity entity.
 * 
 * @author Elena Blyzniuk
 */
public class Complexity extends Entity {
	
	private static final long serialVersionUID = 3453472251509320051L;

	@Override
	public String toString() {
		return "Complexity [id=" + id + ", name=" + name + "]";
	}
}
