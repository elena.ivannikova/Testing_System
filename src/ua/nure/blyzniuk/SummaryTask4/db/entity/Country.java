package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Country entity.
 * 
 * @author Elena Blyzniuk
 */
public class Country extends Identify<Integer> {

	private static final long serialVersionUID = -1858338867992696133L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + "]";
	}
}
