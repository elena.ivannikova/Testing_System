package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * Faculty entity.
 * 
 * @author Elena Blyzniuk
 */
public class Faculty extends Entity {

	private static final long serialVersionUID = -1531168706551149600L;
	
	private Long universityId;

	public static Faculty createFaculty(long id, String name) {
		Faculty faculty = new Faculty();
		faculty.setId(id);
		faculty.setName(name);
		return faculty;
	}

	public static Faculty createFaculty(String facultyName, long institutionId) {
		Faculty faculty = new Faculty();
		faculty.setName(facultyName);
		faculty.setUniversityId(institutionId);
		return faculty;
	}

	public Long getUniversityId() {
		return universityId;
	}

	public void setUniversityId(Long universityId) {
		this.universityId = universityId;
	}

	@Override
	public String toString() {
		return "Faculty [id=" + id + ", name=" + name + ", universityId=" + universityId + "]";
	}
}
