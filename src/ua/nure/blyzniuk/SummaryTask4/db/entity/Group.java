package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * Group entity.
 * 
 * @author Elena Blyzniuk
 */
public class Group extends Entity {
	
	private static final long serialVersionUID = 2765903634578573819L;
	
	private long facultyId;
	
	public static Group createGroup(long id, String name) {
		Group group = new Group();
		group.setId(id);
		group.setName(name);
		return group;
	}
	
	public static Group createGroup(String name, long facultyId) {
		Group group = new Group();
		group.setName(name);
		group.setFacultyId(facultyId);
		return group;
	}

	public long getFacultyId() {
		return facultyId;
	}

	public void setFacultyId(long facultyId) {
		this.facultyId = facultyId;
	}
}
