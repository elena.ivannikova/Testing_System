package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Language entity.
 * 
 * @author Elena Blyzniuk
 */
public class Language extends Identify<Integer> {
	
	private static final long serialVersionUID = 7754913020054112534L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Language [id=" + id + ", name=" + name + "]";
	}
}
