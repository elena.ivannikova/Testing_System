package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * Question entity.
 * 
 * @author Elena Blyzniuk
 */
public class Question extends Identify<Long> {

	private static final long serialVersionUID = -143594713776699804L;
	
	private String text;
	private long testId;
	private int ordinal;

	public int getOrdinal() {
		return ordinal;
	}

	public void setOrdinal(int ordinal) {
		this.ordinal = ordinal;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getTestId() {
		return testId;
	}

	public void setTestId(long testId) {
		this.testId = testId;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", text=" + text + ", testId=" + testId + ", ordinal=" + ordinal + "]";
	}
}
