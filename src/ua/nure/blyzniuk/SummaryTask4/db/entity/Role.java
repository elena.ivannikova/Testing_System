package ua.nure.blyzniuk.SummaryTask4.db.entity;

/**
 * Role entity.
 * 
 * @author Elena Blyzniuk
 */
public enum Role {
	ADMIN, EDUCATOR, STUDENT;

	public static Role getRole(User user) {
		int roleId = user.getRoleId();
		if (roleId <= Role.values().length) {
			return Role.values()[roleId - 1];
		}
		return null;
	}

	public String getName() {
		return name().toLowerCase();
	}
}
