package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * Subject entity.
 * 
 * @author Elena Blyzniuk
 */
public class Subject extends Entity {

	private static final long serialVersionUID = -258174969503512728L;
	
	private long categoryId;

	private Subject() {
	}
	
	public static Subject createSubject(Long id, String name) {
		Subject subject = new Subject();
		subject.setId(id);
		subject.setName(name);
		return subject;
	}

	public static Subject createSubject(String subjectName, long categoryId){
		Subject subject = new Subject();
		subject.setName(subjectName);
		subject.setCategoryId(categoryId);
		return subject;
	}
	
	public static Subject createSubject(){
		return new Subject();
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "Subject [id=" + id + ", name=" + name + ", categoryId=" + categoryId + "]";
	}
}
