package ua.nure.blyzniuk.SummaryTask4.db.entity;

import java.sql.Time;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * Test entity.
 * 
 * @author Elena Blyzniuk
 */
public class Test extends Entity {

	private static final long serialVersionUID = -5266902313975096945L;
	
	Time duration;
	int complexityId;
	long subjectId;
	Boolean freeAccess;

	public Boolean getFreeAccess() {
		return freeAccess;
	}

	public void setFreeAccess(Boolean freeAccess) {
		this.freeAccess = freeAccess;
	}

	public Time getDuration() {
		return duration;
	}

	public void setDuration(Time duration) {
		this.duration = duration;
	}

	public int getComplexityId() {
		return complexityId;
	}

	public void setComplexityId(int complexityId) {
		this.complexityId = complexityId;
	}

	public long getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(long subjectId) {
		this.subjectId = subjectId;
	}

	@Override
	public String toString() {
		return "Test [id=" + id + ", name=" + name + ", duration=" + duration + ", complexityId=" + complexityId
				+ ", subjectId=" + subjectId + "]";
	}
}
