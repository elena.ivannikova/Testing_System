package ua.nure.blyzniuk.SummaryTask4.db.entity;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Entity;

/**
 * University entity.
 * 
 * @author Elena Blyzniuk
 */
public class University extends Entity {

	private static final long serialVersionUID = 5821809852130290213L;
	
	private int countryId;

	public static University createUniversity(long id, String name) {
		University university = new University();
		university.setId(id);
		university.setName(name);
		return university;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
}
