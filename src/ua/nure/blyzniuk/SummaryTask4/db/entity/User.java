package ua.nure.blyzniuk.SummaryTask4.db.entity;

import java.sql.Date;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * User entity.
 * 
 * @author Elena Blyzniuk
 */
public class User extends Identify<Long> {
	
	private static final long serialVersionUID = -7734737524221562227L;
	
	private String firstName;
	private String lastName;
	private String email;
	private int roleId;
	private String login;
	private String password;
	private boolean locked;
	private Date registrDate;
	private int testLimit;
	
	public int getTestLimit() {
		return testLimit;
	}

	public void setTestLimit(int testLimit) {
		this.testLimit = testLimit;
	}

	public User() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public Date getRegistrDate() {
		return registrDate;
	}

	public void setRegistrDate(Date registrDate) {
		this.registrDate = registrDate;
	}
	
	public static User createUser(String firstName, String lastName, String email, int roleId, String login, String password){
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setRoleId(roleId);
		user.setLogin(login);
		user.setPassword(password);
		return user;
	}
	
	public static User createUser(long id, String firstName, String lastName, String email, String login){
		User user = new User();
		user.setId(id);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setLogin(login);
		return user;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", roleId=" + roleId + ", login=" + login + ", locked=" + locked
				+ ", registrDate=" + registrDate + ", testLimit=" + testLimit + "]";
	}
}
