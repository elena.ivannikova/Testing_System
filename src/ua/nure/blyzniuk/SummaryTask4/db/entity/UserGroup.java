package ua.nure.blyzniuk.SummaryTask4.db.entity;

import java.io.Serializable;

/**
 * UserGroup entity.
 * 
 * @author Elena Blyzniuk
 */
public class UserGroup implements Serializable {
	
	private static final long serialVersionUID = -2216787921100662911L;
	
	private long userId;
	private long groupId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
}
