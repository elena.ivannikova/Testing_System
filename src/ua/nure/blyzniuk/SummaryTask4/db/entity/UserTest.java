package ua.nure.blyzniuk.SummaryTask4.db.entity;

import java.sql.Date;

import ua.nure.blyzniuk.SummaryTask4.db.dao.Identify;

/**
 * UserTest entity.
 * 
 * @author Elena Blyzniuk
 */
public class UserTest extends Identify<Long> {

	private static final long serialVersionUID = 6939920779151191498L;
	
	private long testId;
	private long userId;
	private Date datePass;
	private Date dateStart;
	private Date dateEnd;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getTestId() {
		return testId;
	}

	public void setTestId(long testId) {
		this.testId = testId;
	}

	public Date getDatePass() {
		return datePass;
	}

	public void setDatePass(Date datePass) {
		this.datePass = datePass;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	@Override
	public String toString() {
		return "UserTest [id=" + id + ", testId=" + testId + ", userId=" + userId + ", datePass=" + datePass + ", dateStart=" + dateStart + ", dateEnd="
				+ dateEnd + "]";
	}
}
