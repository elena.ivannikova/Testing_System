package ua.nure.blyzniuk.SummaryTask4.db.entity;

import java.io.Serializable;

/**
 * UserTestAnswer entity.
 * 
 * @author Elena Blyzniuk
 */
public class UserTestAnswer implements Serializable {
	
	private static final long serialVersionUID = 2314701620417679935L;
	
	long userTestId;
	long answerId;
	Boolean correct;

	public long getUserTestId() {
		return userTestId;
	}

	public void setUserTestId(long userTestId) {
		this.userTestId = userTestId;
	}

	public long getAnswerId() {
		return answerId;
	}

	public void setAnswerId(long answerId) {
		this.answerId = answerId;
	}

	public Boolean getCorrect() {
		return correct;
	}

	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}

}
