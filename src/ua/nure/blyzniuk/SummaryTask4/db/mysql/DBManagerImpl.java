package ua.nure.blyzniuk.SummaryTask4.db.mysql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.DBManager;
import ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * An implementation to provide connections to database and to close the
 * connections.
 * 
 * @author Elena Blyzniuk
 */
public final class DBManagerImpl implements DBManager<Connection> {

	private static final Logger LOG = Logger.getLogger(DBManagerImpl.class);

	// //////////////////////////////////////////////////////////
	// singleton
	// //////////////////////////////////////////////////////////

	private static DBManagerImpl instance;

	private DataSource ds;

	/**
	 * Constructs DBManager instance.
	 * 
	 * @return DBManager instance
	 */
	public static synchronized DBManagerImpl getInstance() {
		if (instance == null) {
			try {
				instance = new DBManagerImpl();
			} catch (DBException e) {
				LOG.error(ErrorMessages.ERR_CANNOT_OBTAIN_DATA_SOURCE, e);
			}
		}
		return instance;
	}

	private DBManagerImpl() throws DBException {
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			ds = (DataSource) envContext.lookup("jdbc/ST4DB");
			LOG.trace("Data source ==> " + ds);
		} catch (NamingException ex) {
			LOG.error(ErrorMessages.ERR_CANNOT_OBTAIN_DATA_SOURCE, ex);
			throw new DBException(ErrorMessages.ERR_CANNOT_OBTAIN_DATA_SOURCE, ex);
		}
	}

	/**
	 * Returns a DB connection from the Pool Connections.
	 * 
	 * @return DB connection.
	 */
	@Override
	public Connection getConnection() throws DBException {
		Connection con = null;
		try {
			con = ds.getConnection();
		} catch (SQLException ex) {
			LOG.error(ErrorMessages.ERR_CANNOT_OBTAIN_CONNECTION, ex);
			throw new DBException(ErrorMessages.ERR_CANNOT_OBTAIN_CONNECTION, ex);
		}
		return con;
	}

	/**
	 * Closes a connection.
	 * 
	 * @param con
	 *            connection to be closed.
	 */
	@Override
	public void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException ex) {
				LOG.error(ErrorMessages.ERR_CANNOT_CLOSE_CONNECTION, ex);
			}
		}
	}

	// //////////////////////////////////////////////////////////
	// DB util methods
	// //////////////////////////////////////////////////////////

	/**
	 * Closes a statement object.
	 */
	public void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException ex) {
				LOG.error(ErrorMessages.ERR_CANNOT_CLOSE_STATEMENT, ex);
			}
		}
	}

	/**
	 * Closes a result set object.
	 */
	public void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {
				LOG.error(ErrorMessages.ERR_CANNOT_CLOSE_RESULTSET, ex);
			}
		}
	}

	/**
	 * Closes resources.
	 */
	public void close(Connection con, Statement stmt, ResultSet rs) {
		close(rs);
		close(stmt);
		close(con);
	}

	/**
	 * Rollbacks a connection.
	 * 
	 * @param con
	 *            connection to be rollbacked.
	 */
	public void rollback(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException ex) {
				LOG.error("Cannot rollback transaction", ex);
			}
		}
	}
}
