package ua.nure.blyzniuk.SummaryTask4.db.mysql;

/**
 * Container for the fields of the database tables.
 * 
 * @author Elena Blyzniuk
 */
public final class Fields {
	// entities
	public static final String ENTITY_ID = "id";

	public static final String ENTITY_NAME = "name";

	// user
	public static final String USER_FIRST_NAME = "first_name";
	public static final String USER_LAST_NAME = "last_name";
	public static final String USER_EMAIL = "email";
	public static final String USER_ROLE_ID = "role_id";
	public static final String USER_ROLE_NAME = "role_name";
	public static final String USER_LOGIN = "login";
	public static final String USER_PASSWORD = "password";
	public static final String USER_LOCKED = "locked";
	public static final String USER_REGISTR_DATE = "registr_date";
	public static final String USER_TEST_LIMIT = "test_limit";

	// test
	public static final String SUBJECT_ID = "subject_id";
	public static final String COMPLEXITY_ID = "complexity_id";
	public static final String TEST_DATE_START = "date_start";
	public static final String TEST_DATE_END = "date_end";
	public static final String TEST_DATE_PASS = "date_pass";

	// user test bean
	public static final String USER_ID = "user_id";
	public static final String TEST_ID = "test_id";
	public static final String TEST_NAME = "test_name";
	public static final String TEST_RESULT = "test_result";
	public static final String SUBJECT_NAME = "subject_name";
	public static final String COMPLEXITY_NAME = "complexity_name";
	public static final String FREE_ACCESS = "free_access";

	// test info bean
	public static final String TEST_COMPLEXITY = "complexity";
	public static final String TEST_QUESTION_NUMBER = "question_number";
	public static final String TEST_DURATION = "duration";
	
	// answer
	public static final String TEXT = "text";
	public static final String CORRECT = "correct";
	public static final String ORDINAL = "ordinal";
	public static final String QUESTION_ID = "question_id";
	
	// subject
	public static final String CATEGORY_ID = "category_id";
	
	// university
	public static final String COUNTRY_ID = "country_id";
	
	// faculty
	public static final String UNIVERSITY_ID = "university_id";
	
	// group
	public static final String FACULTY_ID = "faculty_id";
	
	// user group bean
	public static final String GROUP_ID = "group_id";
	public static final String GROUP_NAME = "group_name";
	public static final String FACULTY_NAME = "faculty_name";
	public static final String UNIVERSITY_NAME = "university_name";
	
	// category
	public static final String LANGUAGE_ID = "language_id";
	
	public static final String USER_TEST_ID = "user_test_id";
	
	public static final String LIMIT_REACHED = "limit_reached";
	
	public static final String DEADLINE = "deadline";
	
	public static final String RESULT = "result";
}
