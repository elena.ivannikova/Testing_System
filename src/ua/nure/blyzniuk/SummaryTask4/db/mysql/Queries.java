package ua.nure.blyzniuk.SummaryTask4.db.mysql;

/**
 * Container for SQL queries.
 * 
 * @author Elena Blyzniuk
 */
public class Queries {

	private static String getQuery(String... strings) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < strings.length; i++) {
			sb.append(strings[i]);
		}
		return sb.toString();
	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	// UserTestDAO
	//////////////////////////////////////////////////////////////////////////////////////////////
	
	public static final String SQL_GET_TOTAL_RATING_BY_TEST_ID = getQuery(
			"SELECT DISTINCT(user_id), t_id as test_id, test_name, (SELECT last_name FROM user WHERE user.id = user_id) as 'last_name', ",
			"(SELECT first_name FROM user WHERE user.id = user_id) as 'first_name', (SELECT login FROM user WHERE user.id = user_id) ",
			"as 'login', MAX(test_result) as 'result' FROM (SELECT user_test.id as 'ut_id', test.id as 't_id', user_test.user_id as 'user_id', ",
			"test.name as 'test_name', subject.name as 'subject_name', user_test.date_pass, user_test.date_start, user_test.date_end, ",
			"complexity.name as 'complexity_name', duration, (select count(question.id) from question, test where question.test_id = ",
			"test.id and test.id=t_id) as 'question_number', test.free_access, (SELECT test_result FROM (SELECT tt_id, utt_id, COUNT(qq_id) / ",
			"(SELECT COUNT(*) FROM question WHERE question.test_id=tt_id)*100 as 'test_result' FROM (SELECT user_test.id as 'utt_id', ",
			"test.id as 'tt_id', question.id as 'qq_id', COUNT(answer.id) / (SELECT COUNT(answer.id) FROM question, answer WHERE question.id = ",
			"answer.question_id AND question.id = qq_id) as 'count' FROM test, question, answer, user_test_answer, user_test WHERE ",
			"question.test_id = test.id AND answer.question_id = question.id AND user_test_answer.answer_id = answer.id AND user_test.id = ",
			"user_test_answer.user_test_id AND answer.correct = user_test_answer.correct GROUP BY utt_id, question_id HAVING COUNT=1) as t ",
			"group by utt_id) tt WHERE utt_id=ut_id) as 'test_result' from user_test, user_test_answer, test, question, answer, subject, ",
			"complexity where user_test.id = user_test_answer.user_test_id and test.id = user_test.test_id and test.id = question.test_id and ",
			"user_test_answer.answer_id = answer.id and answer.question_id = question.id and test.subject_id=subject.id and test.complexity_id = ",
			"complexity.id group by ut_id) as ttt WHERE t_id=? GROUP BY user_id ORDER BY test_result DESC ");

	public static final String SQL_GET_ALL_STUDENT_TEST_BEANS = getQuery(
			"SELECT DISTINCT(ut.id), t.id as test_id, ut.id ",
			"as user_id, t.name as test_name, s.name as subject_name, ut.date_pass, ut.date_start, ut.date_end, ",
			"c.name as complexity_name, t.duration, (SELECT COUNT(*) FROM question WHERE test_id=t.id) ",
			"as question_number FROM test t, subject s, user_test ut, complexity c, question q WHERE ",
			"t.id=ut.test_id AND t.subject_id=s.id AND t.complexity_id=c.id AND q.test_id=t.id ");

	public static final String SQL_GET_OBLIGATORY_TESTS_BY_STUDENT_ID = getQuery(
			"SELECT test_id, test_name, subject_name, date_start, date_end, complexity_name, duration, question_number ",
			"FROM (SELECT DISTINCT(ut.id), t.id as test_id, t.name as test_name, s.name as subject_name, ut.date_start, ",
			"ut.date_end, c.name as complexity_name, t.duration, (SELECT COUNT(*) FROM question WHERE test_id=t.id) as ",
			"question_number FROM test t, subject s, user_test ut, complexity c, question q WHERE t.id=ut.test_id AND ",
			"t.subject_id=s.id AND t.complexity_id=c.id AND q.test_id=t.id AND ut.user_id=? AND ut.date_pass IS NULL) as t ");

	public static final String SQL_SET_USER_TEST = "INSERT INTO testing.user_test (user_id, test_id, date_end) VALUES (?, ?, ?) ";

	public static final String SQL_INSERT_USER_TEST = "INSERT INTO testing.user_test (user_id, test_id, date_pass) VALUES (?, ?, CURRENT_TIMESTAMP) ";

	public static final String SQL_SELECT_LAST_INSERT_USER_TEST = "SELECT * FROM testing.user_test WHERE id=last_insert_id() ";

	public static final String SQL_GET_USER_TEST_BY_STUDENT_ID_AND_TEST_ID = getQuery(
			"SELECT * FROM testing.user_test WHERE user_id=? AND test_id=? ",
			"AND date_end IS NOT NULL AND date_pass IS NULL ");

	public static final String SQL_GET_ALL_USER_TEST_RESULTS_BY_EDUCATOR_ID = getQuery(
			"SELECT user_test.id as 'user_test_id', test.id as 'test_id', user.id as 'user_id', g.id as 'group_id', ",
			"user.first_name, user.last_name, g.name 'group_name', test.name as 'test_name', subject.name as 'subject_name', date_end<CURRENT_TIMESTAMP as deadline ",
			"FROM user_test, user, test, subject, `group` as g, user_group, faculty, university WHERE date_end IS NOT NULL AND ",
			"user_test.user_id=user.id AND user_test.test_id=test.id AND test.subject_id=subject.id AND user_group.group_id=g.id ",
			"AND user_group.user_id=user.id AND g.id IN (SELECT group_id FROM user_group WHERE user_group.user_id=?) AND ",
			"g.faculty_id = faculty.id AND faculty.university_id = university.id ");

	public static final String SQL_IS_TEST_LIMIT_REACHED = getQuery(
			"SELECT ((SELECT test_limit FROM testing.user WHERE id=?) = (SELECT COUNT(*) FROM test, user_test ",
			"WHERE user_test.user_id=? AND user_test.test_id=test.id AND free_access=1 AND CAST(date_pass AS DATE)=",
			"CAST(CURRENT_TIMESTAMP AS DATE))) as limit_reached ");

	public static final String SQL_GET_PASSED_TESTS_BY_STUDENT_ID = getQuery(
			"SELECT test_name, subject_name, date_pass, complexity_name, duration, question_number, free_access, ",
			"test_result FROM (SELECT user_test.id as 'ut_id', test.id as 't_id', user_test.user_id as 'user_id', ",
			"test.name as 'test_name', subject.name as 'subject_name', user_test.date_pass, user_test.date_start, ",
			"user_test.date_end, complexity.name as 'complexity_name', duration, (select count(question.id) from ",
			"question, test where question.test_id = test.id and test.id=t_id) as 'question_number', test.free_access, ",
			"(SELECT test_result FROM (SELECT tt_id, utt_id, COUNT(qq_id) / (SELECT COUNT(*) FROM question WHERE ",
			"question.test_id=tt_id)*100 as 'test_result' FROM (SELECT user_test.id as 'utt_id', test.id as 'tt_id', ",
			"question.id as 'qq_id', COUNT(answer.id) / (SELECT COUNT(answer.id) FROM question, answer WHERE question.id = ",
			"answer.question_id AND question.id = qq_id) as 'count' FROM test, question, answer, user_test_answer, ",
			"user_test WHERE question.test_id = test.id AND answer.question_id = question.id AND user_test_answer.answer_id = ",
			"answer.id AND user_test.id = user_test_answer.user_test_id AND answer.correct = user_test_answer.correct ",
			"GROUP BY utt_id, question_id HAVING COUNT=1) as t group by utt_id) tt WHERE utt_id=ut_id) as 'test_result' from ",
			"user_test, user_test_answer, test, question, answer, subject, complexity where user_test.id = ",
			"user_test_answer.user_test_id and test.id = user_test.test_id and test.id = question.test_id and ",
			"user_test_answer.answer_id = answer.id and answer.question_id = question.id and test.subject_id=subject.id ",
			"and test.complexity_id = complexity.id group by ut_id having user_id=?) as ttt ORDER BY date_pass DESC ");

	//////////////////////////////////////////////////////////////////////////////////////////////
	// UserTestAnswerDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_ALL_USER_TEST_ANSWER = "SELECT * FROM testing.user_test_answer ";

	public static final String SQL_CREATE_USER_TEST_ANSWER = "INSERT INTO testing.user_test_answer VALUES (?, ?, ?) ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// UserGroupDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_CREATE_USER_GROUP = "INSERT INTO testing.user_group VALUES (?,?) ";

	public static final String SQL_CREATE_USER_GROUP_TMP = "INSERT INTO testing.user_group_tmp VALUES (?,?) ";

	public static final String SQL_GET_ALL_STUDENTS_TO_CONFIRM = getQuery(
			"SELECT u.id as user_id, g.id as group_id, u.first_name, u.last_name, g.name as group_name, ",
			"f.name as faculty_name, un.name as university_name FROM `user` u, `user_group_tmp`, `group` g, ",
			"faculty f, university un WHERE u.role_id=3 AND u.id = user_group_tmp.user_id AND f.id = ",
			"g.faculty_id AND user_group_tmp.group_id = g.id AND un.id = f.university_id AND g.id IN ",
			"(SELECT group_id FROM user_group WHERE user_id=?) ");

	public static final String SQL_GET_ALL_EDUCATORS_TO_CONFIRM = getQuery(
			"SELECT u.id as user_id, g.id as group_id, u.first_name, u.last_name, g.name as group_name, ",
			"f.name as faculty_name, un.name as university_name FROM `user` u, `user_group_tmp`, `group` g, ",
			"faculty f, university un WHERE u.role_id=2 AND u.id = user_group_tmp.user_id AND f.id = g.faculty_id ",
			"AND un.id = f.university_id AND user_group_tmp.group_id = g.id ");

	public static final String SQL_GET_ALL_USER_GROUP_DETAIL_BEAN_BY_USER_ID = getQuery(
			"SELECT u.id as user_id, g.id as group_id, u.first_name, u.last_name, g.name as group_name, ",
			"f.name as faculty_name, un.name as university_name FROM `user` u, `user_group`, `group` g, faculty f, ",
			"university un WHERE u.id=? AND user_group.group_id = g.id AND u.id = user_group.user_id AND f.id = ",
			"g.faculty_id AND un.id = f.university_id HAVING group_name LIKE(?) OR faculty_name LIKE(?) ",
			"OR university_name LIKE(?) ");

	public static final String SQL_DELETE_USER_GROUP_TMP = "DELETE FROM testing.user_group_tmp WHERE user_id=? AND group_id=? ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// UserDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_FIND_ALL_USERS = "SELECT * FROM testing.user ";

	public static final String SQL_INSERT_USER = "INSERT INTO testing.user(first_name, last_name, email, role_id, login, password) VALUES (?, ?, ?, ?, ?, ?);";

	public static final String SQL_UPDATE_USER = "UPDATE testing.user SET first_name=?, last_name=?, email=?, login=? WHERE id=? ";

	public static final String SQL_GET_STUDENTS_BY_GROUPS_ID = getQuery(
			"SELECT user.id, user.first_name, user.last_name, user.email, ",
			"user.role_id, user.login, user.password, user.locked, user.registr_date, user.test_limit ",
			"FROM user, user_group WHERE role_id=3 and user_group.user_id=user.id AND user_group.group_id IN (");

	public static final String SQL_SEARCH_USERS_BY_CONDITIONS = getQuery(
			"SELECT DISTINCT(user.id), user.first_name, user.last_name, user.email, user.login, user.registr_date, ",
			"role.name as 'role_name' FROM testing.user, role, university, faculty, `group` g, user_group WHERE first_name ",
			"OR last_name OR login OR email LIKE(?) AND locked=? AND role.id = user.role_id ");

	public static final String SQL_SEARCH_USERS_BY_INSTITUTION_ID = getQuery(
			"AND user.id = user_group.user_id AND g.id = user_group.group_id AND g.faculty_id = faculty.id AND university.id = ",
			"faculty.university_id AND university.id=? ");

	public static final String SQL_SEARCH_USERS_BY_FACULTY_ID = "AND user.id = user_group.user_id AND g.id = user_group.group_id AND g.faculty_id = faculty.id AND faculty.id=? ";

	public static final String SQL_SEARCH_USERS_BY_GROUP_ID = "AND user.id = user_group.user_id AND user_group.group_id=? ";

	public static final String SQL_GET_INSTITUTIONS_BY_USER_ID = getQuery(
			"SELECT g.name as group_name, f.name as faculty_name, un.name as university_name FROM `user` u, `user_group`, `group` g, ",
			"faculty f, university un WHERE u.id=? AND user_group.group_id = g.id AND u.id = user_group.user_id AND f.id = ",
			"g.faculty_id AND un.id = f.university_id ");

	//////////////////////////////////////////////////////////////////////////////////////////////
	// UniversityDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_USER_ID = getQuery(
			"SELECT DISTINCT(university.id), university.name, university.country_id FROM university, ",
			"faculty, `group` g, user_group, user WHERE user.id = user_group.user_id AND user_group.group_id = ",
			"g.id AND g.faculty_id = faculty.id AND faculty.university_id = university.id AND user.id=? ");

	public static final String SQL_GET_ALL_EDUCATIONAL_INSTITUTIONS = "SELECT * FROM testing.university ";

	public static final String SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_NAME_AND_COUNTRY_NAME = "SELECT * FROM testing.university WHERE country_id=(SELECT id FROM country WHERE name=?) AND name LIKE(?) ";

	public static final String SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_COUNTRY_NAME = "SELECT * FROM testing.university WHERE country_id=(SELECT id FROM country WHERE name=?) ";

	public static final String SQL_CREATE_EDUCATIONAL_INSTITUTION = "INSERT INTO testing.university (name, country_id) VALUES(?,?) ";

	public static final String SQL_UPDATE_EDUCATIONAL_INSTITUTION = "UPDATE testing.university SET name=? WHERE id=? ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// AnswerDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_ANSWERS_BY_QUESTION_ID = "SELECT * FROM testing.answer WHERE question_id=? ";

	public static final String SQL_CREATE_ANSWER = "INSERT INTO testing.answer (text,correct,question_id,ordinal) VALUES(?,?,?,?) ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// CategoryDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_ALL_CATEGORIES = "SELECT * FROM testing.category ";

	public static final String SQL_GET_CATEGORIES_BY_NAME_AND_LANGUAGE_NAME = "SELECT * FROM testing.category WHERE language_id=(SELECT id FROM language WHERE name=?) AND name LIKE(?) ";

	public static final String SQL_GET_CATEGORIES_BY_LANGUAGE_NAME = "SELECT * FROM testing.category WHERE language_id=(SELECT id FROM language WHERE name=?) ";

	public static final String SQL_CREATE_CATEGORY = "INSERT INTO testing.category (name,language_id) VALUES (?,?) ";

	public static final String SQL_UPDATE_CATEGORY = "UPDATE testing.category SET name=? WHERE id=? ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// ComplexityDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_ALL_COMPLEXITY = "SELECT * FROM testing.complexity ORDER BY id ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// CountryDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_COUNTRY_BY_NAME = "SELECT * FROM testing.country WHERE name=? ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// FacultyDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_CREATE_FACULTY = "INSERT INTO testing.faculty (name, university_id) VALUES(?,?) ";

	public static final String SQL_UPDATE_FACULTY = "UPDATE testing.faculty SET name=? WHERE id=? ";

	public static final String SQL_GET_ALL_FACULTIES = "SELECT * FROM testing.faculty ";

	public static final String SQL_GET_FACULTY_BY_INSTITUTION_ID = "SELECT * FROM testing.faculty WHERE university_id=? ";

	public static final String SQL_GET_ALL_FACULTIES_BY_NAME_AND_INSTITUTION_ID = "SELECT * FROM testing.faculty WHERE university_id=? AND name LIKE(?) ";

	public static final String SQL_GET_ALL_FACULTIES_BY_USER_ID_AND_INSTITUTION_ID = getQuery(
			"SELECT DISTINCT(faculty.id), faculty.name, faculty.university_id FROM university, faculty, ",
			"`group` g, user_group, user WHERE user.id = user_group.user_id AND user_group.group_id = g.id ",
			"AND g.faculty_id = faculty.id AND faculty.university_id = university.id AND user.id=? AND university.id=? ");

	//////////////////////////////////////////////////////////////////////////////////////////////
	// GroupDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_CREATE_GROUP = "INSERT INTO testing.group (name,faculty_id) VALUES (?,?) ";

	public static final String SQL_UPDATE_GROUP = "UPDATE testing.group SET name=? WHERE id=? ";

	public static final String SQL_GET_ALL_GROUPS = "SELECT * FROM testing.group ";

	public static final String SQL_GET_GROUPS_BY_FACULTY_ID = "SELECT * FROM testing.group WHERE faculty_id=? ";

	public static final String SQL_GET_ALL_GROUPS_BY_USER_ID_AND_FACULTY_ID = getQuery(
			"SELECT DISTINCT(g.id), g.name, g.faculty_id FROM university, faculty, `group` g, user_group, user ",
			"WHERE user.id = user_group.user_id AND user_group.group_id = g.id AND g.faculty_id = faculty.id AND ",
			"faculty.university_id = university.id AND user.id=? AND faculty.id=? ");

	public static final String SQL_GET_ALL_GROUPS_FOR_USER_CHOISE = getQuery(
			"SELECT g.id, g.name, g.faculty_id from `group` g, faculty WHERE g.faculty_id = faculty.id ",
			"AND faculty.id=? and g.id NOT IN(SELECT g.id FROM testing.`group` g, user_group where faculty_id=? ",
			"and user_id=? and user_group.group_id=g.id) ");

	public static final String SQL_SEARCH_GROUPS_BY_CONDITIONS = getQuery(
			"SELECT DISTINCT(g.id), g.name, g.faculty_id FROM testing.`group` g, faculty, university WHERE g.name ",
			"LIKE(?) AND faculty.university_id = university.id AND g.faculty_id = faculty.id ");

	//////////////////////////////////////////////////////////////////////////////////////////////
	// LanguageDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_GET_LANGUAGE_BY_NAME = "SELECT * FROM testing.language WHERE name=? ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// QuestionDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_CREATE_QUESTION = "INSERT INTO testing.question (text, test_id, ordinal) VALUES (?,?,?)";

	public static final String SQL_GET_ALL_QUESTIONS_BY_TEST_ID = "SELECT * FROM testing.question WHERE test_id=? ";
	
	public static final String SQL_GET_ALL_QUESTIONS = "SELECT * FROM testing.question ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// SubjectDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_CREATE_SUBJECT = "INSERT INTO testing.subject (name, category_id) VALUES (?,?) ";

	public static final String SQL_UPDATE_SUBJECT = "UPDATE testing.subject SET name=? WHERE id=? ";

	public static final String SQL_GET_ALL_SUBJECTS = "SELECT * FROM testing.subject ";

	public static final String SQL_GET_SUBJECTS_BY_CATEGORY_ID = "SELECT * FROM testing.subject WHERE category_id=?";

	public static final String SQL_GET_SUBJECTS_BY_NAME_AND_CATEGORY_ID = "SELECT * FROM testing.subject WHERE category_id=? AND name LIKE(?) ";

	//////////////////////////////////////////////////////////////////////////////////////////////
	// TestDAO
	//////////////////////////////////////////////////////////////////////////////////////////////

	public static final String SQL_CREATE_TEST = "INSERT INTO testing.test (name, duration, subject_id, complexity_id, free_access) VALUES (?,?,?,?,?)";

	public static final String SQL_DELETE_TEST = "DELETE FROM testing.test WHERE id=? ";

	public static final String SQL_UPDATE_TEST = "UPDATE testing.test SET name=?, complexity_id=?, duration=?, free_access=? WHERE id=? ";

	public static final String SQL_GET_ALL_TESTS = "SELECT * FROM testing.test ";

	public static final String SQL_SEARCH_TESTS_BY_CONDITION = getQuery(
			"SELECT test.id, test.name as 'test_name', subject.name as subject_name, test.duration, complexity.name ",
			"as complexity_name, COUNT(question.id) as question_number, test.free_access from question, test, subject, ",
			"complexity, category WHERE question.test_id=test.id AND test.complexity_id = complexity.id AND ",
			"test.subject_id = subject.id AND category.id = subject.category_id AND test.name LIKE(?) AND ",
			"category.language_id=? ");
	
	public static final String SQL_GET_TEST_INFO_BY_TEST_ID = getQuery(
			"SELECT test.id as 'id', test.name as test_name, subject.name as subject_name, test.duration, ",
			"complexity.name as complexity_name, COUNT(question.id) as question_number, test.free_access from ",
			"question, test, subject, complexity WHERE question.test_id=test.id AND test.complexity_id = ",
			"complexity.id AND test.subject_id = subject.id GROUP BY test.id HAVING id=? ");
	
	public static final String SQL_GET_TEST_RESULT_BY_USER_TEST_ID = getQuery(
			"SELECT test_result FROM (SELECT t_id, ut_id, COUNT(q_id) / (SELECT COUNT(*) FROM question WHERE ",
			"question.test_id=t_id)*100 as 'test_result' FROM (SELECT user_test.id as 'ut_id', test.id as 't_id', ",
			"question.id as 'q_id', COUNT(answer.id) / (SELECT COUNT(answer.id) FROM question, answer WHERE ",
			"question.id = answer.question_id AND question.id = q_id) as 'count' FROM test, question, answer, ",
			"user_test_answer, user_test WHERE question.test_id = test.id AND answer.question_id = question.id ",
			"AND user_test_answer.answer_id = answer.id AND user_test.id = user_test_answer.user_test_id AND ",
			"answer.correct = user_test_answer.correct GROUP BY ut_id, question_id HAVING COUNT=1) as t group by ",
			"ut_id) tt WHERE ut_id=? ");
	
	public static final String SQL_DELETE_TESTS_BY_ID =  "DELETE FROM testing.test WHERE id IN (";
}
