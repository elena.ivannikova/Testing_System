package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.CORRECT;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ORDINAL;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.QUESTION_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEXT;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_ANSWER;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ANSWERS_BY_QUESTION_ID;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Answer;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for answer entity.
 * 
 * @author Elena Blyzniuk
 */
public class AnswerDAO extends AbstractGenericDAO<Answer, Long> {
	
	private static final Logger LOG = Logger.getLogger(AnswerDAO.class);

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		return SQL_GET_ANSWERS_BY_QUESTION_ID;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_ANSWER;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	public void prepareStatementForInsert(PreparedStatement statement, Answer object) throws DBException {
		int n = 0;
		try {
			statement.setString(++n, object.getText());
			statement.setBoolean(++n, object.getCorrect());
			statement.setLong(++n, object.getQuestionId());
			statement.setInt(++n, object.getOrdinal());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}

	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Answer object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Answer object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Answer extract(ResultSet rs) throws DBException {
		Answer answer = new Answer();
		try {
			answer.setId(rs.getLong(ENTITY_ID));
			answer.setText(rs.getString(TEXT));
			answer.setCorrect(rs.getBoolean(CORRECT));
			answer.setQuestionId(rs.getLong(QUESTION_ID));
			answer.setOrdinal(rs.getInt(ORDINAL));
		} catch (SQLException e) {
			LOG.error(e);
		}
		return answer;
	}

}
