package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.LANGUAGE_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_CATEGORY;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_CATEGORIES;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_CATEGORY;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for category entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class CategoryDAO extends AbstractGenericDAO<Category, Long> {
	
	private static final Logger LOG = Logger.getLogger(CategoryDAO.class);

	/**
	 * Obtains all categories by language name.
	 * 
	 * @param languageName
	 *            short language name
	 * @return categories obtained by language name
	 * @throws DBException
	 */
	public abstract List<Category> getAllByLanguageName(String languageName) throws DBException;

	/**
	 * Obtains all categories by name language name.
	 * 
	 * @param name
	 *            category name or a part of the name
	 * @param languageName
	 *            language name
	 * @return categories obtained by name and language name
	 * @throws DBException
	 */
	public abstract List<Category> getAllByNameAndLanguageName(String name, String languageName) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_CATEGORIES;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_CATEGORY;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_CATEGORY;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Category object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setInt(2, object.getLanguageId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Category object) throws DBException {
		try {
			int n = 0;
			statement.setString(++n, object.getName());
			statement.setLong(++n, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Category object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Category extract(ResultSet rs) throws DBException {
		Category category = new Category();
		try {
			category.setId(rs.getLong(ENTITY_ID));
			category.setName(rs.getString(ENTITY_NAME));
			category.setLanguageId(rs.getInt(LANGUAGE_ID));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return category;
	}
}
