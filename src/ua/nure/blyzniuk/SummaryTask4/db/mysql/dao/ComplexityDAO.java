package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Complexity;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.*;

/**
 * Provides DAO methods for complexity entity.
 * 
 * @author Elena Blyzniuk
 */
public class ComplexityDAO extends AbstractGenericDAO<Complexity, Long> {
	
	private static final Logger LOG = Logger.getLogger(ComplexityDAO.class);

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_COMPLEXITY;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Complexity object) throws DBException {
		throw new UnsupportedOperationException();

	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Complexity object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Complexity object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Complexity extract(ResultSet rs) throws DBException {
		Complexity complexity = new Complexity();
		try {
			complexity.setId(rs.getLong(ENTITY_ID));
			complexity.setName(rs.getString(ENTITY_NAME));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return complexity;
	}
}
