package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.UNIVERSITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_FACULTY;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_FACULTIES;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_FACULTY_BY_INSTITUTION_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_FACULTY;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for faculty entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class FacultyDAO extends AbstractGenericDAO<Faculty, Long> {

	private static final Logger LOG = Logger.getLogger(FacultyDAO.class);

	/**
	 * Obtains faculties by user id and educational institution id.
	 * 
	 * @param userId
	 *            user id
	 * @param universityId
	 *            educational institution id
	 * @return faculties obtained by user id and educational institution id
	 * @throws DBException
	 */
	public abstract List<Faculty> getAllByUserIdAndInstitutionId(Long userId, Long universityId) throws DBException;

	/**
	 * Obtains faculties by name and educational institution id.
	 * 
	 * @param name
	 *            faculty name
	 * @param universityId
	 *            educational institution id
	 * @return faculties obtained by name and educational institution id
	 * @throws DBException
	 */
	public abstract List<Faculty> getAllByNameAndInstitutionId(String name, Long universityId) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		return SQL_GET_FACULTY_BY_INSTITUTION_ID;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_FACULTIES;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_FACULTY;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_FACULTY;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForSelect(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForSelect(PreparedStatement statement, Long key) throws DBException {
		try {
			statement.setLong(1, key);
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Faculty object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setLong(2, object.getUniversityId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Faculty object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setLong(2, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Faculty object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Faculty extract(ResultSet rs) throws DBException {
		Faculty faculty = new Faculty();
		try {
			faculty.setId(rs.getLong(ENTITY_ID));
			faculty.setName(rs.getString(ENTITY_NAME));
			faculty.setUniversityId(rs.getLong(UNIVERSITY_ID));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return faculty;
	}
}
