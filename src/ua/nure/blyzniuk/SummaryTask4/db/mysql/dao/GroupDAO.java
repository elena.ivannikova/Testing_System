package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.FACULTY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_GROUP;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_GROUPS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_GROUPS_BY_FACULTY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_GROUP;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for group entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class GroupDAO extends AbstractGenericDAO<Group, Long> {
	
	private static final Logger LOG = Logger.getLogger(GroupDAO.class);

	/**
	 * Obtains groups by name, educational institution id, faculty id.
	 * 
	 * @param name
	 *            group name
	 * @param institutionId
	 *            educational institution id
	 * @param facultyId
	 *            faculty id
	 * @return groups obtained by specified conditions
	 * @throws DBException
	 */
	public abstract List<Group> searchGroupsByConditions(String name, Long institutionId, Long facultyId)
			throws DBException;

	/**
	 * Obtains groups for user to add by user id and faculty id.
	 * 
	 * @param facultyId
	 *            faculty id
	 * @param userId
	 *            user id
	 * @return groups obtained by specified conditions
	 * @throws DBException
	 */
	public abstract List<Group> getAllForUserChoise(long facultyId, long userId) throws DBException;

	/**
	 * @param userId
	 * @param facultyId
	 * @return
	 * @throws DBException
	 */
	public abstract List<Group> getAllByUserIdAndFacultyId(Long userId, Long facultyId) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		return SQL_GET_GROUPS_BY_FACULTY_ID;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_GROUPS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_GROUP;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_GROUP;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForSelect(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForSelect(PreparedStatement statement, Long key) throws DBException {
		try {
			statement.setLong(1, key);
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Group object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setLong(2, object.getFacultyId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Group object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setLong(2, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Group object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Group extract(ResultSet rs) throws DBException {
		Group group = new Group();
		try {
			group.setId(rs.getLong(ENTITY_ID));
			group.setName(rs.getString(ENTITY_NAME));
			group.setFacultyId(rs.getLong(FACULTY_ID));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return group;
	}
}
