package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ORDINAL;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEXT;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_QUESTION;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_QUESTIONS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_QUESTIONS_BY_TEST_ID;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Question;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for question entity.
 * 
 * @author Elena Blyzniuk
 */
public class QuestionDAO extends AbstractGenericDAO<Question, Long> {
	
	private static final Logger LOG = Logger.getLogger(QuestionDAO.class);

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_QUESTIONS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_QUESTION;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement, java.io.Serializable)
	 */
	@Override
	public void prepareStatementForInsert(PreparedStatement statement, Question object) throws DBException {
		int n = 0;
		try {
			statement.setString(++n, object.getText());
			statement.setLong(++n, object.getTestId());
			statement.setInt(++n, object.getOrdinal());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement, java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Question object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement, java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Question object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Question extract(ResultSet rs) throws DBException {
		Question question = new Question();
		try {
			question.setId(rs.getLong(ENTITY_ID));
			question.setText(rs.getString(TEXT));
			question.setTestId(rs.getLong(TEST_ID));
			question.setOrdinal(rs.getInt(ORDINAL));
		} catch (SQLException e) {
			LOG.error(e);
			e.printStackTrace();
		}
		return question;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		return SQL_GET_ALL_QUESTIONS_BY_TEST_ID;
	}

}
