package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.CATEGORY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_SUBJECT;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_SUBJECTS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_SUBJECTS_BY_CATEGORY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_SUBJECT;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for subject entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class SubjectDAO extends AbstractGenericDAO<Subject, Long> {
	
	private static final Logger LOG = Logger.getLogger(SubjectDAO.class);

	/**
	 * Obtains subjects by name and category id.
	 * 
	 * @param name
	 *            subject name
	 * @param categoryId
	 *            category id
	 * @return subject obtained by name and category id
	 * @throws DBException
	 */
	public abstract List<Subject> getAllByNameAndCategoryId(String name, Long categoryId) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		return SQL_GET_SUBJECTS_BY_CATEGORY_ID;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_SUBJECTS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_SUBJECT;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_SUBJECT;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Subject object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setLong(2, object.getCategoryId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Subject object) throws DBException {
		try {
			int n = 0;
			statement.setString(++n, object.getName());
			statement.setLong(++n, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Subject object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Subject extract(ResultSet rs) throws DBException {
		Subject subject = Subject.createSubject();
		try {
			subject.setId(rs.getLong(ENTITY_ID));
			subject.setName(rs.getString(ENTITY_NAME));
			subject.setCategoryId(rs.getLong(CATEGORY_ID));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return subject;
	}

}
