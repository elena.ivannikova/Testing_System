package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.COMPLEXITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.SUBJECT_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_DURATION;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_TEST;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_DELETE_TEST;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_TESTS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_TEST;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Test;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for test entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class TestDAO extends AbstractGenericDAO<Test, Long> {
	
	private static final Logger LOG = Logger.getLogger(TestDAO.class);

	/**
	 * Delete tests by specified id.
	 * 
	 * @param testsId
	 *            tests id
	 * @return true if tests were successfully deleted
	 * @throws DBException
	 */
	public abstract boolean deleteTestsById(long[] testsId) throws DBException;

	/**
	 * Inserts answers when a user has finished passing test.
	 * 
	 * @param testing
	 *            test container which provides answers to insert
	 * @return true if answers were successfully inserted
	 * @throws DBException
	 */
	public abstract boolean insertAnswers(TestQuestionsBean testing) throws DBException;

	/**
	 * Creates test from the test container.
	 * 
	 * @param testing
	 *            test container to create test
	 * @return true if the test was successfully created
	 * @throws DBException
	 */
	public abstract boolean createTest(TestQuestionsBean testing) throws DBException;

	/**
	 * Obtains test result by user test id.
	 * 
	 * @param userTestId
	 *            user test id
	 * @return test result as a double number
	 * @throws DBException
	 */
	public abstract Double getTestResultByUserTestId(long userTestId) throws DBException;

	/**
	 * Get test results as a list of test info containers from the result set.
	 * 
	 * @param rs
	 *            result set to obtain test results
	 * @return test results obtained from the result set
	 * @throws SQLException
	 */
	protected abstract List<TestInfoBean> getTestResultList(ResultSet rs) throws SQLException;

	/**
	 * Obtains information about the test by test id.
	 * 
	 * @param testId
	 *            test id to obtain test information
	 * @return test information as a test result container
	 * @throws DBException
	 */
	public abstract TestInfoBean getByTestId(Long testId) throws DBException;

	/**
	 * Search tests by name, language id, category id and subject id by user
	 * role condition.
	 * 
	 * @param languageId
	 *            language id
	 * @param role
	 *            user role
	 * @param name
	 *            test name
	 * @param categoryId
	 *            category id
	 * @param subjectId
	 *            subject id
	 * @return tests obtained as a result of the search as test info containers
	 * @throws DBException
	 */
	public abstract List<TestInfoBean> searchTestsByCondition(int languageId, Role role, String name, Long categoryId,
			Long subjectId) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_TESTS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_TEST;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_TEST;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		return SQL_DELETE_TEST;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, Test object) throws DBException {
		int n = 0;
		try {
			statement.setString(++n, object.getName());
			statement.setTime(++n, object.getDuration());
			statement.setLong(++n, object.getSubjectId());
			statement.setInt(++n, object.getComplexityId());
			statement.setBoolean(++n, object.getFreeAccess());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, Test object) throws DBException {
		int n = 0;
		try {
			statement.setString(++n, object.getName());
			statement.setInt(++n, object.getComplexityId());
			statement.setTime(++n, object.getDuration());
			statement.setBoolean(++n, object.getFreeAccess());
			statement.setLong(++n, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, Test object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected Test extract(ResultSet rs) throws DBException {
		Test test = new Test();
		try {
			test.setId(rs.getLong(ENTITY_ID));
			test.setName(rs.getString(ENTITY_NAME));
			test.setDuration(rs.getTime(TEST_DURATION));
			test.setSubjectId(rs.getLong(SUBJECT_ID));
			test.setComplexityId(rs.getInt(COMPLEXITY_ID));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return test;
	}
}
