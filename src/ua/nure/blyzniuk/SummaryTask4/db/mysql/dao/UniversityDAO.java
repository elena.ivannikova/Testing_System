package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.COUNTRY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_EDUCATIONAL_INSTITUTION;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_EDUCATIONAL_INSTITUTIONS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_USER_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_EDUCATIONAL_INSTITUTION;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for university entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class UniversityDAO extends AbstractGenericDAO<University, Long> {
	
	private static final Logger LOG = Logger.getLogger(UniversityDAO.class);

	/**
	 * Obtains educational institutions by name and country name.
	 * 
	 * @param name
	 *            educational institution name
	 * @param countryName
	 *            country name
	 * @return educational institutions obtained by name and country name
	 * @throws DBException
	 */
	public abstract List<University> getByNameAndCountryName(String name, String countryName) throws DBException;

	/**
	 * Obtains educational institutions country name.
	 * 
	 * @param countryName
	 *            country name
	 * @return educational institutions obtained by and country name
	 * @throws DBException
	 */
	public abstract List<University> getAllByCountryName(String countryName) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		return SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_USER_ID;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_EDUCATIONAL_INSTITUTIONS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_CREATE_EDUCATIONAL_INSTITUTION;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_EDUCATIONAL_INSTITUTION;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, University object) throws DBException {
		try {
			statement.setString(1, object.getName());
			statement.setLong(2, object.getCountryId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, University object) throws DBException {
		try {
			int n = 0;
			statement.setString(++n, object.getName());
			statement.setLong(++n, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, University object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected University extract(ResultSet rs) throws DBException {
		University university = new University();
		try {
			university.setId(rs.getLong(ENTITY_ID));
			university.setName(rs.getString(ENTITY_NAME));
			university.setCountryId(rs.getInt(COUNTRY_ID));
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
		return university;
	}
}
