package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_EMAIL;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_FIRST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_LAST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_LOCKED;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_LOGIN;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_PASSWORD;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_REGISTR_DATE;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_ROLE_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_TEST_LIMIT;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_FIND_ALL_USERS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_INSERT_USER;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_UPDATE_USER;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.security.Password.hash;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for user entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class UserDAO extends AbstractGenericDAO<User, Long> {
	
	private static final Logger LOG = Logger.getLogger(UserDAO.class);

	/**
	 * Obtains user by specified login and password.
	 * 
	 * @param login
	 *            user login
	 * @param password
	 *            user password
	 * @return user obtained by specified login and password
	 * @throws DBException
	 */
	public abstract User findUserByLoginPassword(String login, String password) throws DBException;

	/**
	 * Update user's password.
	 * 
	 * @param userId
	 *            user id
	 * @param oldPassword
	 *            old password
	 * @param newPassword
	 *            new password
	 * @return true if password was successfully updated
	 * @throws DBException
	 */
	public abstract boolean updatePassword(Long userId, String oldPassword, String newPassword) throws DBException;

	/**
	 * Obtains students by groups id.
	 * 
	 * @param groupsId
	 *            id of groups
	 * @return students obtained by groups id
	 * @throws DBException
	 */
	public abstract List<User> getStudentsByGroupsId(long[] groupsId) throws DBException;

	/**
	 * Searches students by name, locked condition, educational institution id,
	 * faculty id, group id.
	 * 
	 * @param name
	 *            student name
	 * @param locked
	 *            locked condition
	 * @param institutionId
	 *            educational institution id
	 * @param facultyId
	 *            faculty id
	 * @param groupId
	 *            group id
	 * @return list of student found by specified conditions as user info
	 *         containers
	 * @throws DBException
	 */
	public abstract List<UserInfoBean> searchUsersByConditions(String name, int locked, Long institutionId,
			Long facultyId, Long groupId) throws DBException;

	/**
	 * @param userId
	 * @return
	 * @throws DBException
	 */
	public abstract List<UserGroupBean> getInstitutionsByUserId(Long userId) throws DBException;

	/**
	 * Locks or unlocks users by specified id.
	 * 
	 * @param usersId
	 *            users' id
	 * @param lock
	 *            type of operation (true=lock, false=unlock)
	 * @return true if the specified users were successfully locked or unlocked
	 * @throws DBException
	 */
	public abstract boolean setLock(long[] usersId, boolean lock) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_FIND_ALL_USERS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return SQL_INSERT_USER;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return SQL_UPDATE_USER;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, User object) throws DBException {
		int n = 0;
		try {
			statement.setString(++n, object.getFirstName());
			statement.setString(++n, object.getLastName());
			statement.setString(++n, object.getEmail());
			statement.setInt(++n, object.getRoleId());
			statement.setString(++n, object.getLogin());
			statement.setString(++n, hash(object.getPassword()));
		} catch (Exception e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, User object) throws DBException {
		int n = 0;
		try {
			statement.setString(++n, object.getFirstName());
			statement.setString(++n, object.getLastName());
			statement.setString(++n, object.getEmail());
			statement.setString(++n, object.getLogin());
			statement.setLong(++n, object.getId());
		} catch (Exception e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, User object) throws DBException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected User extract(ResultSet rs) throws DBException {
		User user = new User();
		try {
			user.setId(rs.getLong(ENTITY_ID));
			user.setFirstName(rs.getString(USER_FIRST_NAME));
			user.setLastName(rs.getString(USER_LAST_NAME));
			user.setEmail(rs.getString(USER_EMAIL));
			user.setRoleId(rs.getInt(USER_ROLE_ID));
			user.setLogin(rs.getString(USER_LOGIN));
			user.setPassword(rs.getString(USER_PASSWORD));
			user.setLocked(rs.getInt(USER_LOCKED) == 1);
			user.setRegistrDate(rs.getDate(USER_REGISTR_DATE));
			user.setTestLimit(rs.getInt(USER_TEST_LIMIT));
		} catch (SQLException e) {
			LOG.error(e);
		}
		return user;
	}
}
