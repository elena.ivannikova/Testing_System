package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import java.sql.ResultSet;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupDetailBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.DBManagerImpl;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public abstract class UserGroupDAO {
	
	protected DBManagerImpl dbManager = DBManagerImpl.getInstance();

	/**
	 * Rejects the requests of users' to add an educational institution.
	 * 
	 * @param userGroups
	 *            user groups list
	 * @return true if the requests were successfully rejected
	 * @throws DBException
	 */
	public abstract boolean rejectUserGroups(List<UserGroup> userGroups) throws DBException;

	/**
	 * Confirms the requests of users' to add an educational institution.
	 * 
	 * @param userGroups
	 *            user groups list
	 * @return true if the requests were successfully confirmed
	 * @throws DBException
	 */
	public abstract boolean confirmUserGroups(List<UserGroup> userGroups) throws DBException;

	/**
	 * Obtains information about educational institutions of educator as an
	 * informational container.
	 * 
	 * @param userId
	 *            educator id
	 * @param name
	 *            group, educational institution or faculty name
	 * @return list of informational containers about educational institutions
	 * @throws DBException
	 */
	public abstract List<UserGroupDetailBean> getAllByUserId(Long userId, String name) throws DBException;

	/**
	 * Obtains all educators waiting for confirmation.
	 * 
	 * @return list of educators waiting for confirmation obtained as the
	 *         informational containers
	 * @throws DBException
	 */
	public abstract List<UserGroupDetailBean> getAllEducatorsToConfirm() throws DBException;

	/**
	 * Obtains all students waiting for confirmation by educator id.
	 * 
	 * @param educatorId
	 *            educator id
	 * @return list of students waiting for confirmation obtained as the
	 *         informational containers
	 * @throws DBException
	 */
	public abstract List<UserGroupDetailBean> getAllStudentsToConfirm(long educatorId) throws DBException;

	/**
	 * Extracts user group information into container from the result set.
	 * 
	 * @param rs
	 *            result set to extract user group
	 * @return user group information
	 * @throws DBException
	 */
	protected abstract UserGroupDetailBean extract(ResultSet rs) throws DBException;

	/**
	 * Parses result set and extracts user group information into containers
	 * from the result set.
	 * 
	 * @param rs
	 *            result set to parse
	 * @return list of user groups obtained form the result set
	 * @throws DBException
	 */
	protected abstract List<UserGroupDetailBean> parseResultSet(ResultSet rs) throws DBException;

	/**
	 * Inserts user groups into temporary or permanent table by condition.
	 * 
	 * @param object
	 *            user group object to be inserted
	 * @param sql
	 *            SQL query as a condition to insert
	 * @return true if the object was successfully inserted
	 * @throws DBException
	 */
	protected abstract boolean insertByCondition(UserGroup object, String sql) throws DBException;

	/**
	 * Inserts user group object into permanent table. It is used when users'
	 * requests are confirmed.
	 * 
	 * @param object
	 *            user group object to be inserted
	 * @return true if the object was successfully inserted
	 * @throws DBException
	 */
	public abstract boolean insertFinal(UserGroup object) throws DBException;

	/**
	 * Inserts user group object into temporary table. It is used when users are
	 * waiting for confirmations.
	 * 
	 * @param object
	 *            user group object to be inserted
	 * @return true if the object was successfully inserted
	 * @throws DBException
	 */
	public abstract boolean insert(UserGroup object) throws DBException;
}
