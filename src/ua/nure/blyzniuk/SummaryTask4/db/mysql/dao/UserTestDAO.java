package ua.nure.blyzniuk.SummaryTask4.db.mysql.dao;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_DATE_END;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_DATE_PASS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_DATE_START;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_STUDENT_TEST_BEANS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestRatingBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserObligatoryTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserPassedTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserTestResultsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserTest;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides DAO methods for user test entity.
 * 
 * @author Elena Blyzniuk
 */
public abstract class UserTestDAO extends AbstractGenericDAO<UserTest, Long> {
	
	private static final Logger LOG = Logger.getLogger(UserTestDAO.class);
	
	public abstract List<TestRatingBean> getTestRatingsByTestId(Long id) throws DBException;

	/**
	 * Obtains obligatory tests by student id.
	 * 
	 * @param id
	 *            student id
	 * @return a list of obligatory tests obtained by student id
	 * @throws DBException
	 */
	public abstract List<UserObligatoryTestsBean> getObligatoryTestsByStudentId(Long id) throws DBException;

	/**
	 * Obtains passed tests by student id.
	 * 
	 * @param id
	 *            student id
	 * @return a list of passed tests obtained by student id
	 * @throws DBException
	 */
	public abstract List<UserPassedTestsBean> getPassedTestsByStudentId(Long id) throws DBException;

	/**
	 * Sets tests for for specified users.
	 * 
	 * @param users
	 *            users to set the tests
	 * @param testsId
	 *            tests id to set to users
	 * @param dateEnd
	 *            final date to pass the test
	 * @return
	 * @throws DBException
	 */
	public abstract boolean setUserTests(List<User> users, long[] testsId, String dateEnd) throws DBException;

	/**
	 * Inserts user test into the table.
	 * 
	 * @param object
	 *            user test to be inserted
	 * @return inserted user test
	 * @throws DBException
	 */
	public abstract UserTest insert(UserTest object) throws DBException;

	/**
	 * Obtains user test by student id and test id from the table.
	 * 
	 * @param studentId
	 *            student id
	 * @param testId
	 *            test id
	 * @return user test obtained by student id and test id
	 * @throws DBException
	 */
	public abstract UserTest getByStudentIdAndTestId(Long studentId, Long testId) throws DBException;

	/**
	 * Determines whether the test limit per day is reached for the specified
	 * student.
	 * 
	 * @param userId
	 *            user id to determine the test limit
	 * @return true if the test limit per day is reached
	 * @throws DBException
	 */
	public abstract Boolean isTestLimitReached(Long userId) throws DBException;

	/**
	 * Obtains student's test's results by educator id.
	 * 
	 * @param educatorId educator id
	 * @param institutionId
	 * @param facultyId
	 * @param groupId
	 * @return
	 * @throws DBException
	 */
	public abstract List<UserTestResultsBean> getAllByEducatorId(long educatorId, Long institutionId, Long facultyId,
			Long groupId) throws DBException;

	/**
	 * Extracts user test result into the container from the result set.
	 * 
	 * @param rs
	 *            result set to extract user test result
	 * @return user test result as a container extracted form the result set
	 * @throws DBException
	 */
	protected abstract UserTestResultsBean extractUserTestResults(ResultSet rs) throws DBException;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByFKQuery()
	 */
	@Override
	public String getSelectByFKQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected UserTest extract(ResultSet rs) throws DBException {
		UserTest studentTest = new UserTest();
		try {
			studentTest.setId(rs.getLong(ENTITY_ID));
			studentTest.setUserId(rs.getLong(USER_ID));
			studentTest.setTestId(rs.getLong(TEST_ID));
			studentTest.setDatePass(rs.getDate(TEST_DATE_PASS));
			studentTest.setDateStart(rs.getDate(TEST_DATE_START));
			studentTest.setDateEnd(rs.getDate(TEST_DATE_END));
		} catch (SQLException e) {
			LOG.error(e);
		}
		return studentTest;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectQuery()
	 */
	@Override
	public String getSelectQuery() {
		return SQL_GET_ALL_STUDENT_TEST_BEANS;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getSelectByPKQuery()
	 */
	@Override
	public String getSelectByPKQuery() {
		return getSelectQuery() + "AND ut.id=? ";
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getCreateQuery()
	 */
	@Override
	public String getCreateQuery() {
		return "INSERT INTO testing.user_test (user_id, test_id) VALUES (?, ?) ";
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getUpdateQuery()
	 */
	@Override
	public String getUpdateQuery() {
		return "UPDATE testing.user_test SET date_pass=CURRENT_TIMESTAMP WHERE id=? ";
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#getDeleteQuery()
	 */
	@Override
	public String getDeleteQuery() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForInsert(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForInsert(PreparedStatement statement, UserTest object) throws DBException {
		int n = 0;
		try {
			statement.setLong(++n, object.getUserId());
			statement.setLong(++n, object.getTestId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForUpdate(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForUpdate(PreparedStatement statement, UserTest object) throws DBException {
		try {
			statement.setLong(1, object.getId());
		} catch (SQLException e) {
			LOG.error(e);
			throw new DBException(e);
		}
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.dao.AbstractGenericDAO#prepareStatementForDelete(java.sql.PreparedStatement,
	 *      java.io.Serializable)
	 */
	@Override
	protected void prepareStatementForDelete(PreparedStatement statement, UserTest object) throws DBException {
		throw new UnsupportedOperationException();
	}
}
