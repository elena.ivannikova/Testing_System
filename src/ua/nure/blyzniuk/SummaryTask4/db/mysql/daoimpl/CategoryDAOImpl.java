package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_CATEGORIES_BY_LANGUAGE_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_CATEGORIES_BY_NAME_AND_LANGUAGE_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.CategoryDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class CategoryDAOImpl extends CategoryDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.CategoryDAO#getAllByLanguageName(java.lang.String)
	 */
	@Override
	public List<Category> getAllByLanguageName(String languageName) throws DBException {
		List<Category> list;
		String sql = SQL_GET_CATEGORIES_BY_LANGUAGE_NAME;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setString(1, languageName);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.CategoryDAO#getAllByNameAndLanguageName(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Category> getAllByNameAndLanguageName(String name, String languageName) throws DBException {
		List<Category> list;
		String sql = SQL_GET_CATEGORIES_BY_NAME_AND_LANGUAGE_NAME;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			name = "%" + name + "%";
			int n = 0;
			statement.setString(++n, languageName);
			statement.setString(++n, name);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

}
