package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_COUNTRY_BY_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.CountryDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class CountryDAOImpl extends CountryDAO {

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.CountryDAO#getCountryByName(java.lang.String)
	 */
	@Override
	public Country getCountryByName(String name) throws DBException {
		Country country = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			pstmt = con.prepareStatement(SQL_GET_COUNTRY_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				country = extract(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			dbManager.rollback(con);
			throw new DBException("Cannot obtain countru by name", ex);
		} finally {
			dbManager.close(con, pstmt, rs);
		}
		return country;
	}

}
