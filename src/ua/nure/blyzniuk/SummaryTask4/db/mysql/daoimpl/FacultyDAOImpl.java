package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_FACULTIES_BY_NAME_AND_INSTITUTION_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_FACULTIES_BY_USER_ID_AND_INSTITUTION_ID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.FacultyDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class FacultyDAOImpl extends FacultyDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.FacultyDAO#getAllByUserIdAndInstitutionId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Faculty> getAllByUserIdAndInstitutionId(Long userId, Long universityId) throws DBException {
		List<Faculty> list;
		String sql = SQL_GET_ALL_FACULTIES_BY_USER_ID_AND_INSTITUTION_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, userId);
			statement.setLong(2, universityId);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.FacultyDAO#getAllByNameAndInstitutionId(java.lang.String, java.lang.Long)
	 */
	@Override
	public List<Faculty> getAllByNameAndInstitutionId(String name, Long universityId) throws DBException {
		List<Faculty> list;
		String sql = SQL_GET_ALL_FACULTIES_BY_NAME_AND_INSTITUTION_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			StringBuilder sb = new StringBuilder("%").append(name).append("%");
			statement.setLong(1, universityId);
			statement.setString(2, sb.toString());
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

}
