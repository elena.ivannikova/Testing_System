package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.FACULTY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_GROUPS_BY_USER_ID_AND_FACULTY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_GROUPS_FOR_USER_CHOISE;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_SEARCH_GROUPS_BY_CONDITIONS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.GroupDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * @author Alena
 *
 */
public class GroupDAOImpl extends GroupDAO {

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.GroupDAO#searchGroupsByConditions(java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Group> searchGroupsByConditions(String name, Long institutionId, Long facultyId) throws DBException {
		List<Group> list = new LinkedList<>();
		String sql = SQL_SEARCH_GROUPS_BY_CONDITIONS;
		Long fk = null;
		if (institutionId != null) {
			fk = institutionId;
			sql += "AND university.id=? ";
		} else if (facultyId != null) {
			fk = facultyId;
			sql += "AND faculty.id=? ";
		}
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			name = "%" + name + "%";
			int n = 0;
			statement.setString(++n, name);
			if (institutionId != null || facultyId != null) {
				statement.setLong(++n, fk);
			}
			rs = statement.executeQuery();
			while (rs.next()) {
				Group group = new Group();
				group.setId(rs.getLong(ENTITY_ID));
				group.setName(rs.getString(ENTITY_NAME));
				group.setFacultyId(rs.getLong(FACULTY_ID));
				list.add(group);
			}
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.GroupDAO#getAllForUserChoise(long, long)
	 */
	@Override
	public List<Group> getAllForUserChoise(long facultyId, long userId) throws DBException {
		List<Group> list;
		String sql = SQL_GET_ALL_GROUPS_FOR_USER_CHOISE;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			int n = 0;
			statement.setLong(++n, facultyId);
			statement.setLong(++n, facultyId);
			statement.setLong(++n, userId);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.GroupDAO#getAllByUserIdAndFacultyId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Group> getAllByUserIdAndFacultyId(Long userId, Long facultyId) throws DBException {
		List<Group> list;
		String sql = SQL_GET_ALL_GROUPS_BY_USER_ID_AND_FACULTY_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, userId);
			statement.setLong(2, facultyId);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}
}
