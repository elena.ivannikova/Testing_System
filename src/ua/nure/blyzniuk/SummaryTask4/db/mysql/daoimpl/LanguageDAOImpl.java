package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_LANGUAGE_BY_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.LanguageDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class LanguageDAOImpl extends LanguageDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.LanguageDAO#getLanguageByName(java.lang.String)
	 */
	@Override
	public Language getLanguageByName(String name) throws DBException{
		Language language = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			pstmt = con.prepareStatement(SQL_GET_LANGUAGE_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				language = extract(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			dbManager.rollback(con);
			throw new DBException("Cannot obtain language from current locale.", ex);
		} finally {
			dbManager.close(con, pstmt, rs);
		}
		return language;
	}

}
