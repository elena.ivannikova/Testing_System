package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_SUBJECTS_BY_NAME_AND_CATEGORY_ID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.SubjectDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * @author Alena
 *
 */
public class SubjectDAOImpl extends SubjectDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.SubjectDAO#getAllByNameAndCategoryId(java.lang.String, java.lang.Long)
	 */
	@Override
	public List<Subject> getAllByNameAndCategoryId(String name, Long categoryId) throws DBException {
		List<Subject> list;
		String sql = SQL_GET_SUBJECTS_BY_NAME_AND_CATEGORY_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			StringBuilder sb = new StringBuilder("%").append(name).append("%");
			statement.setLong(1, categoryId);
			statement.setString(2, sb.toString());
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

}
