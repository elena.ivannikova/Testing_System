package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.COMPLEXITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.FREE_ACCESS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.SUBJECT_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_DURATION;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.TEST_QUESTION_NUMBER;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_USER_TEST_ANSWER;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_DELETE_TESTS_BY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_TEST_INFO_BY_TEST_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_TEST_RESULT_BY_USER_TEST_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_SEARCH_TESTS_BY_CONDITION;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.QuestionAnswerBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Answer;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Question;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Test;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.AnswerDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.QuestionDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class TestDAOImpl extends TestDAO {

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#deleteTestsById(long[])
	 */
	@Override
	public boolean deleteTestsById(long[] testsId) throws DBException {
		boolean res = false;
		StringBuilder sql = new StringBuilder(SQL_DELETE_TESTS_BY_ID);
		for (int i = 0; i < testsId.length; i++) {
			sql.append(testsId[i]).append(",");
			if (i == testsId.length - 1) {
				sql.deleteCharAt(sql.length() - 1);
			}
		}
		sql.append(") ");
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql.toString());
			int count = statement.executeUpdate();
			if (count != testsId.length) {
				throw new DBException("Cannot delete specified tests.");
			}
			res = true;
			con.commit();
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(statement);
			dbManager.close(con);

		}
		return res;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#insertAnswers(ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean)
	 */
	@Override
	public boolean insertAnswers(TestQuestionsBean testing) throws DBException {
		boolean res = false;
		String sql = SQL_CREATE_USER_TEST_ANSWER;
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			int total = 0;
			for (QuestionAnswerBean questionAnswers : testing.getQuestions()) {
				for (Answer answer : questionAnswers.getAnswers()) {
					int n = 0;
					statement.setLong(++n, testing.getUserTestId());
					statement.setLong(++n, answer.getId());
					statement.setBoolean(++n, answer.getCorrect());
					statement.addBatch();
					total++;
				}
			}
			int[] inserted = statement.executeBatch();
			if (inserted.length != total) {
				throw new DBException("error_msg.test_not_exists");
			}
			con.commit();
			res = true;
		} catch (Exception e) {
			throw new DBException("error_msg.test_not_exists", e);
		} finally {
			dbManager.close(statement);
			dbManager.close(con);
		}
		return res;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#createTest(ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean)
	 */
	@Override
	public boolean createTest(TestQuestionsBean testing) throws DBException {
		boolean res = false;
		QuestionDAO questionDAO = new QuestionDAO();
		AnswerDAO answerDAO = new AnswerDAO();
		String sqlInsertTest = getCreateQuery();
		String sqlInsertQuestion = questionDAO.getCreateQuery();
		String sqlInsertAnswer = answerDAO.getCreateQuery();
		PreparedStatement stInsertTest = null;
		PreparedStatement stInsertQuestion = null;
		PreparedStatement stInsertAnswer = null;
		PreparedStatement stGetTest = null;
		PreparedStatement stGetQuestion = null;
		PreparedStatement stGetAnswer = null;
		Connection con = null;
		ResultSet rs = null;
		try {
			con = dbManager.getConnection();
			stInsertTest = con.prepareStatement(sqlInsertTest);
			stInsertQuestion = con.prepareStatement(sqlInsertQuestion);
			stInsertAnswer = con.prepareStatement(sqlInsertAnswer);
			prepareStatementForInsert(stInsertTest, testing.getTest());
			int count = stInsertTest.executeUpdate();
			if (count != 1) {
				throw new DBException("On persist modify more then 1 record: " + count);
			}
			stGetTest = con.prepareStatement(getSelectQuery() + "WHERE id=last_insert_id();");
			rs = stGetTest.executeQuery();
			List<Test> testList = parseResultSet(rs);
			if ((testList == null) || (testList.size() != 1)) {
				throw new DBException("Exception on findByPK new persist data.");
			}
			Test test = testList.iterator().next();
			int answCount = 0;
			List<Question> questionList = null;
			stGetQuestion = con.prepareStatement(questionDAO.getSelectQuery() + "WHERE id=last_insert_id();");
			for (QuestionAnswerBean q : testing.getQuestions()) {
				Question quest = q.getQuestion();
				quest.setTestId(test.getId());
				questionDAO.prepareStatementForInsert(stInsertQuestion, quest);
				stInsertQuestion.executeUpdate();
				rs = stGetQuestion.executeQuery();
				questionList = questionDAO.parseResultSet(rs);
				if (questionList == null || questionList.size() != 1) {
					throw new DBException("Error while creating test.");
				}
				quest = questionList.iterator().next();
				for (Answer a : q.getAnswers()) {
					a.setQuestionId(quest.getId());
					answerDAO.prepareStatementForInsert(stInsertAnswer, a);
					stInsertAnswer.addBatch();
					answCount++;
				}
			}
			int[] aCount = stInsertAnswer.executeBatch();
			if (aCount.length != answCount) {
				throw new DBException("Error while creating test.");
			}
			con.commit();
			res = true;
		} catch (SQLException e) {
			throw new DBException("Error while creating test.", e);
		} finally {
			dbManager.close(rs);
			dbManager.close(stInsertAnswer);
			dbManager.close(stGetAnswer);
			dbManager.close(stInsertQuestion);
			dbManager.close(stGetQuestion);
			dbManager.close(stInsertTest);
			dbManager.close(stGetTest);
			dbManager.close(con);
		}
		return res;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#getTestResultByUserTestId(long)
	 */
	@Override
	public Double getTestResultByUserTestId(long userTestId) throws DBException {
		Double result = null;
		String sql = SQL_GET_TEST_RESULT_BY_USER_TEST_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, userTestId);
			rs = statement.executeQuery();
			if (rs.next()) {
				result = rs.getDouble("test_result");
			}
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#getTestResultList(java.sql.ResultSet)
	 */
	@Override
	protected List<TestInfoBean> getTestResultList(ResultSet rs) throws SQLException {
		List<TestInfoBean> list = new LinkedList<>();
		while (rs.next()) {
			TestInfoBean item = new TestInfoBean();
			item.setId(rs.getLong(ENTITY_ID));
			item.setTestName(rs.getString(TEST_NAME));
			item.setSubjectName(rs.getString(SUBJECT_NAME));
			item.setDuration(rs.getTime(TEST_DURATION));
			item.setComplexityName(rs.getString(COMPLEXITY_NAME));
			item.setQuestionNumber(rs.getInt(TEST_QUESTION_NUMBER));
			item.setFreeAccess(rs.getBoolean(FREE_ACCESS));
			list.add(item);
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#getByTestId(java.lang.Long)
	 */
	@Override
	public TestInfoBean getByTestId(Long testId) throws DBException {
		List<TestInfoBean> list;
		String sql = SQL_GET_TEST_INFO_BY_TEST_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, testId);
			rs = statement.executeQuery();
			list = getTestResultList(rs);
		} catch (Exception e) {
			throw new DBException("Cannot obtain information about test.", e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		if (list.size() > 1) {
			throw new DBException("Received more than one record.");
		}
		return list.iterator().next();
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO#searchTestsByCondition(int, ua.nure.blyzniuk.SummaryTask4.db.entity.Role, java.lang.String, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<TestInfoBean> searchTestsByCondition(int languageId, Role role, String name, Long categoryId,
			Long subjectId) throws DBException {
		List<TestInfoBean> list = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = SQL_SEARCH_TESTS_BY_CONDITION;
		if (role.equals(Role.EDUCATOR)) {
			sql += "AND test.free_access=0 ";
		} else if (role.equals(Role.STUDENT)) {
			sql += "AND test.free_access=1 ";
		}
		Long fk = null;
		if (categoryId != null) {
			fk = categoryId;
			sql += "AND category.id=? ";
		} else if (subjectId != null) {
			fk = subjectId;
			sql += "AND subject.id=? ";
		}
		sql += " GROUP BY test.id ";

		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			name = "%" + name + "%";
			int n = 0;
			statement.setString(++n, name);
			statement.setInt(++n, languageId);
			if (categoryId != null || subjectId != null) {
				statement.setLong(++n, fk);
			}
			rs = statement.executeQuery();
			list = getTestResultList(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

}
