package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_COUNTRY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_NAME_AND_COUNTRY_NAME;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UniversityDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class UniversityDAOImpl extends UniversityDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UniversityDAO#getByNameAndCountryName(java.lang.String, java.lang.String)
	 */
	@Override
	public List<University> getByNameAndCountryName(String name, String countryName) throws DBException {
		List<University> list;
		String sql = SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_NAME_AND_COUNTRY_NAME;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			name = "%" + name + "%";
			statement.setString(1, countryName);
			statement.setString(2, name);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UniversityDAO#getAllByCountryName(java.lang.String)
	 */
	@Override
	public List<University> getAllByCountryName(String countryName) throws DBException {
		List<University> list;
		String sql = SQL_GET_EDUCATIONAL_INSTITUTIONS_BY_COUNTRY_NAME;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setString(1, countryName);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

}
