package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO;
import ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.ENTITY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.FACULTY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.GROUP_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.UNIVERSITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_EMAIL;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_FIRST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_LAST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_LOGIN;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_REGISTR_DATE;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_ROLE_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_INSTITUTIONS_BY_USER_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_STUDENTS_BY_GROUPS_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_SEARCH_USERS_BY_CONDITIONS;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_SEARCH_USERS_BY_FACULTY_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_SEARCH_USERS_BY_GROUP_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_SEARCH_USERS_BY_INSTITUTION_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.security.Password.*;

public class UserDAOImpl extends UserDAO {

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO#setLock(long[], boolean)
	 */
	public boolean setLock(long[] usersId, boolean lock) throws DBException {
		boolean result = false;
		StringBuilder sb = new StringBuilder("UPDATE testing.user SET locked=").append(lock ? 1 : 0)
				.append(" WHERE id IN(");
		PreparedStatement statement = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			for (int i = 0; i < usersId.length; i++) {
				sb.append(usersId[i]).append(",");
				if (i == usersId.length - 1) {
					sb.deleteCharAt(sb.length() - 1);
				}
			}
			sb.append(")");
			statement = con.prepareStatement(sb.toString());
			int count = statement.executeUpdate();
			if (count != usersId.length) {
				throw new DBException("Error while locking users");
			}
			con.commit();
			result = true;
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(statement);
			dbManager.close(con);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO#updatePassword(java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean updatePassword(Long userId, String oldPassword, String newPassword) throws DBException {
		boolean result = false;
		PreparedStatement statement = null;
		Connection con = null;
		try {
			String sql = "UPDATE testing.user SET password=SHA(?) WHERE id=? AND password='" + hash(oldPassword) + "'";
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			int n = 0;
			statement.setString(++n, newPassword);
			statement.setLong(++n, userId);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DBException();
			}
			con.commit();
			result = true;
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(statement);
			dbManager.close(con);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO#findUserByLoginPassword(java.lang.String, java.lang.String)
	 */
	@Override
	public User findUserByLoginPassword(String login, String password) throws DBException {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			pstmt = con.prepareStatement(getSelectQuery() + "WHERE login=? AND password=SHA(?) ");
			int n = 0;
			pstmt.setString(++n, login);
			pstmt.setString(++n, password);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = extract(rs);
			}
			con.commit();
		} catch (SQLException ex) {
			dbManager.rollback(con);
			throw new DBException(ErrorMessages.ERR_CANNOT_FIND_USER_BY_LOGIN_PASSWORD, ex);
		} finally {
			dbManager.close(con, pstmt, rs);
		}
		return user;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO#getInstitutionsByUserId(java.lang.Long)
	 */
	public List<UserGroupBean> getInstitutionsByUserId(Long userId) throws DBException {
		List<UserGroupBean> list;
		String sql = SQL_GET_INSTITUTIONS_BY_USER_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, userId);
			rs = statement.executeQuery();
			list = new LinkedList<>();
			while (rs.next()) {
				UserGroupBean userGroupBean = new UserGroupBean();
				userGroupBean.setGroupName(rs.getString(GROUP_NAME));
				userGroupBean.setFacultyName(rs.getString(FACULTY_NAME));
				userGroupBean.setUniversityName(rs.getString(UNIVERSITY_NAME));
				list.add(userGroupBean);
			}
			con.commit();
		} catch (Exception e) {
			throw new DBException("Cannot obtain insitutions by user id.", e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO#searchUsersByConditions(java.lang.String, int, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	public List<UserInfoBean> searchUsersByConditions(String name, int locked, Long institutionId, Long facultyId,
			Long groupId) throws DBException {
		List<UserInfoBean> list = new LinkedList<>();
		String sql = SQL_SEARCH_USERS_BY_CONDITIONS;
		Long fk = null;
		if (institutionId != null) {
			fk = institutionId;
			sql += SQL_SEARCH_USERS_BY_INSTITUTION_ID;
		} else if (facultyId != null) {
			fk = facultyId;
			sql += SQL_SEARCH_USERS_BY_FACULTY_ID;
		} else if (groupId != null) {
			fk = groupId;
			sql += SQL_SEARCH_USERS_BY_GROUP_ID;
		}
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			name = "%" + name + "%";
			int n = 0;
			statement.setString(++n, name);
			statement.setInt(++n, locked);
			if (institutionId != null || facultyId != null || groupId != null) {
				statement.setLong(++n, fk);
			}
			rs = statement.executeQuery();
			while (rs.next()) {
				UserInfoBean bean = new UserInfoBean();
				bean.setId(rs.getLong(ENTITY_ID));
				bean.setFirstName(rs.getString(USER_FIRST_NAME));
				bean.setLastName(rs.getString(USER_LAST_NAME));
				bean.setEmail(rs.getString(USER_EMAIL));
				bean.setLogin(rs.getString(USER_LOGIN));
				bean.setRegistrDate(rs.getDate(USER_REGISTR_DATE));
				bean.setRoleName(rs.getString(USER_ROLE_NAME));
				list.add(bean);
			}
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO#getStudentsByGroupsId(long[])
	 */
	public List<User> getStudentsByGroupsId(long[] groupsId) throws DBException {
		List<User> list;
		StringBuilder sql = new StringBuilder(SQL_GET_STUDENTS_BY_GROUPS_ID);
		for (int i = 0; i < groupsId.length; i++) {
			sql.append(groupsId[i]).append(",");
			if (i == groupsId.length - 1) {
				sql.deleteCharAt(sql.length() - 1);
			}
		}
		sql.append(")");
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql.toString());
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}
}
