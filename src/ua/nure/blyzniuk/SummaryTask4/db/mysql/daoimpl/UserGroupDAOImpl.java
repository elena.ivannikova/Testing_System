package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.FACULTY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.GROUP_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.GROUP_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.UNIVERSITY_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_FIRST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_ID;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.USER_LAST_NAME;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_USER_GROUP;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_CREATE_USER_GROUP_TMP;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_DELETE_USER_GROUP_TMP;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_EDUCATORS_TO_CONFIRM;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_STUDENTS_TO_CONFIRM;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.SQL_GET_ALL_USER_GROUP_DETAIL_BEAN_BY_USER_ID;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupDetailBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class UserGroupDAOImpl extends UserGroupDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#rejectUserGroups(java.util.List)
	 */
	@Override
	public boolean rejectUserGroups(List<UserGroup> userGroups) throws DBException {
		boolean res = false;
		String sqlDelete = SQL_DELETE_USER_GROUP_TMP;
		PreparedStatement stDelete = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			stDelete = con.prepareStatement(sqlDelete);
			for (UserGroup userGroup : userGroups) {
				stDelete.setLong(1, userGroup.getUserId());
				stDelete.setLong(2, userGroup.getGroupId());
				stDelete.addBatch();
			}
			int[] deleted = stDelete.executeBatch();
			if (deleted.length != userGroups.size()) {
				throw new DBException("Error while rejecting institutions.");
			}
			con.commit();
			res = true;
		} catch (Exception e) {
			throw new DBException("Error while rejecting institutions.", e);
		} finally {
			dbManager.close(stDelete);
			dbManager.close(con);
		}
		return res;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#confirmUserGroups(java.util.List)
	 */
	@Override
	public boolean confirmUserGroups(List<UserGroup> userGroups) throws DBException {
		boolean res = false;
		String sqlDelete = SQL_DELETE_USER_GROUP_TMP;
		String sqlInsert = SQL_CREATE_USER_GROUP;
		PreparedStatement stDelete = null;
		PreparedStatement stInsert = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			stDelete = con.prepareStatement(sqlDelete);
			stInsert = con.prepareStatement(sqlInsert);
			for (UserGroup userGroup : userGroups) {
				stDelete.setLong(1, userGroup.getUserId());
				stDelete.setLong(2, userGroup.getGroupId());
				stDelete.addBatch();
				stInsert.setLong(1, userGroup.getUserId());
				stInsert.setLong(2, userGroup.getGroupId());
				stInsert.addBatch();
			}
			int[] deleted = stDelete.executeBatch();
			int[] inserted = stInsert.executeBatch();
			if (deleted.length != userGroups.size() || inserted.length != userGroups.size()) {
				throw new DBException("Error while confirming institutions.");
			}
			con.commit();
			res = true;
		} catch (Exception e) {
			throw new DBException("Error while confirming institutions.", e);
		} finally {
			dbManager.close(stInsert);
			dbManager.close(stDelete);
			dbManager.close(con);
		}
		return res;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#getAllByUserId(java.lang.Long, java.lang.String)
	 */
	@Override
	public List<UserGroupDetailBean> getAllByUserId(Long userId, String name) throws DBException {
		List<UserGroupDetailBean> list;
		name = "%" + name + "%";
		String sql = SQL_GET_ALL_USER_GROUP_DETAIL_BEAN_BY_USER_ID;
//				"SELECT u.id as user_id, g.id as group_id, u.first_name, u.last_name, g.name as group_name, "
//				+ "f.name as faculty_name, un.name as university_name FROM `user` u, `user_group`, `group` g, faculty f, "
//				+ "university un WHERE u.id=? AND user_group.group_id = g.id AND u.id = user_group.user_id AND f.id = "
//				+ "g.faculty_id AND un.id = f.university_id HAVING group_name LIKE(?) OR faculty_name LIKE(?) "
//				+ "OR university_name LIKE(?) ";
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			int n = 0;
			statement.setLong(++n, userId);
			statement.setString(++n, name);
			statement.setString(++n, name);
			statement.setString(++n, name);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#getAllEducatorsToConfirm()
	 */
	@Override
	public List<UserGroupDetailBean> getAllEducatorsToConfirm() throws DBException {
		List<UserGroupDetailBean> list;
		String sql = SQL_GET_ALL_EDUCATORS_TO_CONFIRM;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#getAllStudentsToConfirm(long)
	 */
	@Override
	public List<UserGroupDetailBean> getAllStudentsToConfirm(long educatorId) throws DBException {
		List<UserGroupDetailBean> list;
		String sql = SQL_GET_ALL_STUDENTS_TO_CONFIRM;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, educatorId);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#extract(java.sql.ResultSet)
	 */
	@Override
	protected UserGroupDetailBean extract(ResultSet rs) throws DBException {
		UserGroupDetailBean userGroupDetailBean = new UserGroupDetailBean();
		try {
			userGroupDetailBean.setUserId(rs.getLong(USER_ID));
			userGroupDetailBean.setGroupId(rs.getLong(GROUP_ID));
			userGroupDetailBean.setUserFirstName(rs.getString(USER_FIRST_NAME));
			userGroupDetailBean.setUserLastName(rs.getString(USER_LAST_NAME));
			userGroupDetailBean.setGroupName(rs.getString(GROUP_NAME));
			userGroupDetailBean.setFacultyName(rs.getString(FACULTY_NAME));
			userGroupDetailBean.setUniversityName(rs.getString(UNIVERSITY_NAME));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userGroupDetailBean;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#parseResultSet(java.sql.ResultSet)
	 */
	@Override
	protected List<UserGroupDetailBean> parseResultSet(ResultSet rs) throws DBException {
		List<UserGroupDetailBean> result = new LinkedList<UserGroupDetailBean>();
		try {
			while (rs.next()) {
				result.add(extract(rs));
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#insertByCondition(ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup, java.lang.String)
	 */
	@Override
	protected boolean insertByCondition(UserGroup object, String sql) throws DBException {
		boolean result = false;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, object.getUserId());
			statement.setLong(2, object.getGroupId());
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DBException("On persist modify more then 1 record: " + count);
			}
			con.commit();
			result = true;
		} catch (Exception e) {
			dbManager.rollback(con);
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#insertFinal(ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup)
	 */
	@Override
	public boolean insertFinal(UserGroup object) throws DBException {
		String sql = SQL_CREATE_USER_GROUP;
		return insertByCondition(object, sql);
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO#insert(ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup)
	 */
	@Override
	public boolean insert(UserGroup object) throws DBException {
		String sql = SQL_CREATE_USER_GROUP_TMP;
		return insertByCondition(object, sql);
	}

}
