package ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl;

import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Fields.*;
import static ua.nure.blyzniuk.SummaryTask4.db.mysql.Queries.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestRatingBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserObligatoryTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserPassedTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserTestResultsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserTest;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

public class UserTestDAOImpl extends UserTestDAO {
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#getObligatoryTestsByStudentId(java.lang.Long)
	 */
	@Override
	public List<UserObligatoryTestsBean> getObligatoryTestsByStudentId(Long id) throws DBException {
		List<UserObligatoryTestsBean> list;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = SQL_GET_OBLIGATORY_TESTS_BY_STUDENT_ID;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, id);
			rs = statement.executeQuery();
			list = new LinkedList<>();
			while (rs.next()) {
				UserObligatoryTestsBean item = new UserObligatoryTestsBean();
				item.setTestId(rs.getLong(TEST_ID));
				item.setTestName(rs.getString(TEST_NAME));
				item.setSubjectName(rs.getString(SUBJECT_NAME));
				item.setDateStart(rs.getDate(TEST_DATE_START));
				item.setDateEnd(rs.getDate(TEST_DATE_END));
				item.setComplexityName(rs.getString(COMPLEXITY_NAME));
				item.setDuration(rs.getTime(TEST_DURATION));
				item.setQuestionNumber(rs.getInt(TEST_QUESTION_NUMBER));
				list.add(item);
			}
		} catch (Exception e) {
			throw new DBException("Cannot obtain obligatory tests of current user.", e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}
	
	@Override
	public List<TestRatingBean> getTestRatingsByTestId(Long id) throws DBException {
		List<TestRatingBean> list = new LinkedList<>();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = SQL_GET_TOTAL_RATING_BY_TEST_ID;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, id);
			rs = statement.executeQuery();
			
			while (rs.next()) {
				TestRatingBean tr = new TestRatingBean();
				tr.setUserId(rs.getLong(USER_ID));
				tr.setTestId(rs.getLong(TEST_ID));
				tr.setLastName(rs.getString(USER_LAST_NAME));
				tr.setFirstName(rs.getString(USER_FIRST_NAME));
				tr.setLogin(rs.getString(USER_LOGIN));
				tr.setResult(rs.getDouble(RESULT));
				list.add(tr);
			}
			
		} catch (Exception e) {
			throw new DBException("Cannot obtain test rating.", e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	@Override
	public List<UserPassedTestsBean> getPassedTestsByStudentId(Long id) throws DBException {
		List<UserPassedTestsBean> list = new LinkedList<>();
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = SQL_GET_PASSED_TESTS_BY_STUDENT_ID;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(1, id);
			rs = statement.executeQuery();
			while (rs.next()) {
				UserPassedTestsBean ut = new UserPassedTestsBean();
				ut.setTestName(rs.getString(TEST_NAME));
				ut.setSubjectName(rs.getString(SUBJECT_NAME));
				ut.setDatePass(rs.getDate(TEST_DATE_PASS));
				ut.setComplexityName(rs.getString(COMPLEXITY_NAME));
				ut.setDuration(rs.getTime(TEST_DURATION));
				ut.setQuestionNumber(rs.getInt(TEST_QUESTION_NUMBER));
				ut.setFreeAccess(rs.getBoolean(FREE_ACCESS));
				ut.setTestResult(rs.getDouble(TEST_RESULT));
				list.add(ut);
			}
		} catch (Exception e) {
			throw new DBException("Cannot obtain passed tests by student id.", e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#setUserTests(java.util.List, long[], java.lang.String)
	 */
	@Override
	public boolean setUserTests(List<User> users, long[] testsId, String dateEnd) throws DBException {
		boolean result = false;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = SQL_SET_USER_TEST;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			int count = 0;
			for (int i = 0; i < testsId.length; i++) {
				for (int j = 0; j < users.size(); j++) {
					int n = 0;
					statement.setLong(++n, users.get(j).getId());
					statement.setLong(++n, testsId[i]);
					statement.setString(++n, dateEnd);
					statement.addBatch();
					count++;
				}
			}
			int[] inserted = statement.executeBatch();
			if (inserted.length != count) {
				throw new DBException("Error while rejecting institutions.");
			}
			con.commit();
			result = true;
		} catch (Exception e) {
			dbManager.rollback(con);
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#insert(ua.nure.blyzniuk.SummaryTask4.db.entity.UserTest)
	 */
	@Override
	public UserTest insert(UserTest object) throws DBException {
		UserTest persistInstance = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		String sql = SQL_INSERT_USER_TEST;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			prepareStatementForInsert(statement, object);
			int count = statement.executeUpdate();
			if (count != 1) {
				throw new DBException("On persist modify more then 1 record: " + count);
			}
			con.commit();
			statement = con.prepareStatement(SQL_SELECT_LAST_INSERT_USER_TEST);
			rs = statement.executeQuery();
			List<UserTest> list = parseResultSet(rs);
			if ((list == null) || (list.size() != 1)) {
				throw new DBException("Exception on findByPK new persist data.");
			}
			persistInstance = list.iterator().next();
		} catch (Exception e) {
			dbManager.rollback(con);
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return persistInstance;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#getByStudentIdAndTestId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public UserTest getByStudentIdAndTestId(Long studentId, Long testId) throws DBException {
		List<UserTest> list;
		String sql = SQL_GET_USER_TEST_BY_STUDENT_ID_AND_TEST_ID;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			int n = 0;
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(++n, studentId);
			statement.setLong(++n, testId);
			rs = statement.executeQuery();
			list = parseResultSet(rs);
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (list == null || list.size() == 0) {
			return null;
		}
		if (list.size() > 1) {
			throw new DBException("Received more than one record.");
		}
		return list.iterator().next();
	}
	
	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#isTestLimitReached(java.lang.Long)
	 */
	@Override
	public Boolean isTestLimitReached(Long userId) throws DBException {
		Boolean result = null;
		String sql = SQL_IS_TEST_LIMIT_REACHED;
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			int n = 0;
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			statement.setLong(++n, userId);
			statement.setLong(++n, userId);
			rs = statement.executeQuery();
			if (rs.next()) {
				result = rs.getBoolean(LIMIT_REACHED);
			}
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		if (result == null) {
			throw new DBException("Error while calculating test limit");
		}
		return result;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#getAllByEducatorId(long, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<UserTestResultsBean> getAllByEducatorId(long educatorId, Long institutionId, Long facultyId,
			Long groupId) throws DBException {
		List<UserTestResultsBean> list;
		String sql = SQL_GET_ALL_USER_TEST_RESULTS_BY_EDUCATOR_ID;
		Long fk = null;
		if (groupId != null) {
			fk = groupId;
			sql += "AND g.id=? ";
		} else if (facultyId != null) {
			fk = facultyId;
			sql += "AND faculty.id=? ";
		} else if (institutionId != null) {
			fk = institutionId;
			sql += "AND university.id=? ";
		}
		PreparedStatement statement = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dbManager.getConnection();
			statement = con.prepareStatement(sql);
			int n = 0;
			statement.setLong(++n, educatorId);
			if (institutionId != null || facultyId != null || groupId != null) {
				statement.setLong(++n, fk);
			}
			rs = statement.executeQuery();
			list = new LinkedList<>();
			while (rs.next()) {
				list.add(extractUserTestResults(rs));
			}
		} catch (Exception e) {
			throw new DBException(e);
		} finally {
			dbManager.close(con, statement, rs);
		}
		return list;
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO#extractUserTestResults(java.sql.ResultSet)
	 */
	@Override
	protected UserTestResultsBean extractUserTestResults(ResultSet rs) throws DBException {
		UserTestResultsBean userTestResult = new UserTestResultsBean();
		TestDAO testDAO = DAOFactory.getTestDAO();
		try {
			userTestResult.setId(rs.getLong(USER_TEST_ID));
			userTestResult.setTestId(rs.getLong(TEST_ID));
			userTestResult.setUserId(rs.getLong(USER_ID));
			userTestResult.setGroupId(rs.getLong(GROUP_ID));
			userTestResult.setFirstName(rs.getString(USER_FIRST_NAME));
			userTestResult.setLastName(rs.getString(USER_LAST_NAME));
			userTestResult.setGroupName(rs.getString(GROUP_NAME));
			userTestResult.setTestName(rs.getString(TEST_NAME));
			userTestResult.setSubjectName(rs.getString(SUBJECT_NAME));
			Double result = testDAO.getTestResultByUserTestId(userTestResult.getId());
			if (result != null) {
				userTestResult.setTestResult(result);
			}
			userTestResult.setDeadline(rs.getInt(DEADLINE) == 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userTestResult;
	}
}
