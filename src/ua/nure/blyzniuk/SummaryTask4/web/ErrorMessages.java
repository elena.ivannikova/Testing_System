package ua.nure.blyzniuk.SummaryTask4.web;

/**
 * Holder for messages of exceptions.
 * 
 * @author Elena Blyzniuk
 */
public class ErrorMessages {

	private ErrorMessages() {
	}

	public static final String ERR_EMPTY_FIELDS = "error_msg.empty_fields";
	public static final String ERR_EMPTY_CATEGORY_CHOICE = "error_msg.empty_category_choice";
	public static final String ERR_EMPTY_INSTITUTION_CHOICE = "error_msg.empty_institution_choice";
	public static final String ERR_INVALID_OPERATION = "error_msg.invalid_operation";
	public static final String ERR_PASSWORD_NOT_EQUAL = "error_msg.password_not_equal";
	public static final String ERR_INVALID_EMAIL = "error_msg.invalid_email";
	public static final String ERR_INVALID_LOGIN = "error_msg.invalid_login";
	public static final String ERR_CANNOT_FIND_USER_BY_LOGIN_PASSWORD = "error_msg.cannot_find_user_by_credentials";
	public static final String ERR_USER_LOCKED = "error_msg.user_locked";
	public static final String ERR_NO_EDUCATOR_GROUPS = "error_msg.no_educator_groups";
	public static final String ERR_NO_SEARCH_RESULTS = "error_msg.no_search_results";
	public static final String ERR_ILLEGAL_DATE = "error_msg.no_illegal_date";
	public static final String ERR_EMPTY_GROUP_CHOICE = "error_msg.no_empty_group_choice";
	public static final String ERR_EMPTY_USER_CHOICE = "error_msg.no_empty_user_choice";
	public static final String ERR_EMPTY_TEST_CHOICE = "error_msg.no_empty_test_choice";
	public static final String ERR_ILLEGAL_EMAIL_FORMAT = "error_msg.illegal_email_format";
	public static final String ERR_ILLEGAL_LOGIN_FORMAT = "error_msg.illegal_login_format";
	public static final String ERR_INSTITUTION_ALREADY_EXISTS = "error_msg.update_institution";
	public static final String ERR_FACULTY_ALREADY_EXISTS = "error_msg.update_faculty";
	public static final String ERR_GROUP_ALREADY_EXISTS = "error_msg.update_group";
	public static final String ERR_SUBJECT_ALREADY_EXISTS = "error_msg.update_subject";
	public static final String ERR_CATEGORY_ALREADY_EXISTS = "error_msg.update_category";
	public static final String ERR_DELETE_EMPTY_TEST_CHOICE = "error_msg.delete_empty_test_choice";
	public static final String ERR_ILLEGAL_PASSWORD = "error_msg.illegal_password";
	public static final String ERR_ILLEGAL_TIME_FORMAT = "error_msg.illegal_time_format";
	public static final String ERR_ILLEGAL_QUESTION_NUMBER = "error_msg.illegal_question_number";
	public static final String ERR_EMPTY_GROUP_NAME = "error_msg.empty_group_name";
	public static final String ERR_EMPTY_VARIANTS = "error_msg.empty_variants";
	public static final String ERR_EMPTY_ANSWERS = "error_msg.empty_answers";
	public static final String ERR_EMPTY_QUESTION = "error_msg.empty_question_text";
	public static final String ERR_NO_FACULTIES_OF_INSTITUTION = "error_msg.no_faculties_of_institution";
	public static final String ERR_NO_GROUPS_OF_FACULTY = "error_msg.no_groups_of_faculty";
	public static final String MSG_NO_PASSES_TESTS = "error_msg.no_passed_test";

	// public static final String ERR_CANNOT_OBTAIN_USER_ORDER_BEANS = "Cannot

	public static final String ERR_CANNOT_OBTAIN_CONNECTION = "Cannot obtain a connection from the pool";

	public static final String ERR_CANNOT_OBTAIN_CURRENT_LANGUAGE = "Cannot obtain language from current locale.";
	public static final String ERR_CANNOT_OBTAIN_CURRENT_COUNTRY = "Cannot obtain country from current locale.";
	public static final String ERR_ILLEGAL_USER_ROLE = "Illegal user role.";

	public static final String ERR_CANNOT_REGISTER_USER = "Cannot register user.";

	public static final String ERR_CANNOT_CLOSE_CONNECTION = "Cannot close a connection";
	public static final String ERR_CANNOT_CLOSE_RESULTSET = "Cannot close a result set";
	public static final String ERR_CANNOT_CLOSE_STATEMENT = "Cannot close a statement";
	public static final String ERR_CANNOT_OBTAIN_DATA_SOURCE = "Cannot obtain the data source";

}
