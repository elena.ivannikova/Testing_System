package ua.nure.blyzniuk.SummaryTask4.web;

/**
 * Holder for informational messages.
 * 
 * @author Elena Blyzniuk
 */
public final class InfoMessages {
	
	public static final String MSG_REGISTRATION_SUCCESS = "Registration was successfully completed. Please, log in using your new credentials.";
	
	public static final String MSG_INSTITUTION_ADDED = "info_msg.institution_added";
	public static final String MSG_CATEGORY_ADDED = "info_msg.category_added";
	public static final String MSG_FACULTY_ADDED = "info_msg.faculty_added";
	public static final String MSG_GROUP_ADDED = "info_msg.group_added";
	public static final String MSG_SUBJECT_ADDED = "info_msg.subject_added";
	public static final String MSG_USERS_LOCKED = "info_msg.users_locked";
	public static final String MSG_USERS_UNLOCKED = "info_msg.users_unlocked";
	public static final String MSG_PASSWORD_CHANGED = "info_msg.password_changed";
	public static final String MSG_TEST_CREATED = "info_msg.test_created";
	public static final String MSG_ASSIGN_TESTS_SUCCESS = "error_msg.set_groups_tests_success";
	
}
