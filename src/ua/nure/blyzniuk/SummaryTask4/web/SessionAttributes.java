package ua.nure.blyzniuk.SummaryTask4.web;

/**
 * Holder for session attribute's names.
 * 
 * @author Elena Blyzniuk
 */
public final class SessionAttributes {
	public static final String SESS_ATTR_MESSAGE = "message";
	public static final String SESS_ATTR_ERROR_MESSAGE = "errorMessage";
	public static final String SESS_ATTR_USER = "user";
	public static final String SESS_ATTR_ROLE = "userRole";
	public static final String SESS_ATTR_LANGUAGE = "language";
	public static final String SESS_ATTR_COUNTRY = "country";
}
