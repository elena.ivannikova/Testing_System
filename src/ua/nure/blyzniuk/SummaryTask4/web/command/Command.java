package ua.nure.blyzniuk.SummaryTask4.web.command;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;

/**
 * 
 * 
 * @author Alena
 */
public abstract class Command implements Serializable {
	private static final long serialVersionUID = -7492199321652410475L;

	/**
	 * Execution method for command.
	 * 
	 * @return Address to go once the command is executed.
	 */
	public abstract CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException;
	
	public String getTargetPath() {
		return Path.PAGE_MAIN;
	}
	
	public CommandHelper getTargetOperation() {
		return new CommandHelper(getTargetPath());
	}
	
	@Override
	public final String toString() {
		return getClass().getSimpleName();
	}
}
