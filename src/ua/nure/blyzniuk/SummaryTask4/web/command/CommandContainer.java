package ua.nure.blyzniuk.SummaryTask4.web.command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.web.command.admin.*;
import ua.nure.blyzniuk.SummaryTask4.web.command.admineducator.ConfirmUsersCommand;
import ua.nure.blyzniuk.SummaryTask4.web.command.admineducator.ShowConfirmUsersPageCommand;
import ua.nure.blyzniuk.SummaryTask4.web.command.common.*;
import ua.nure.blyzniuk.SummaryTask4.web.command.educator.ShowSetTestsPageCommand;
import ua.nure.blyzniuk.SummaryTask4.web.command.educator.ShowStudentRatingPageCommand;
import ua.nure.blyzniuk.SummaryTask4.web.command.educatorstudent.*;
import ua.nure.blyzniuk.SummaryTask4.web.command.student.*;
import ua.nure.blyzniuk.SummaryTask4.web.command.outofcontrol.*;

/**
 * Holder for all commands.<br/>
 * 
 * @author Alena
 */
public class CommandContainer {
	private static final Logger LOG = Logger.getLogger(CommandContainer.class);

	private static Map<String, Command> commands = new TreeMap<String, Command>();

	static {
		// common commands
		commands.put("login", new LoginCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("searchTests", new SearchTestsCommand());
		commands.put("showSettingsPage", new ShowSettingsPageCommand());
		commands.put("register", new RegisterCommand());
		commands.put("showTestInfo", new ShowTestInfoCommand());
		commands.put("noCommand", new NoCommand());
		commands.put("showPersonalAccount", new ShowPersonalAccountCommand());
		commands.put("showEditUserPage", new ShowEditUserPageCommand());
		commands.put("editUser", new EditUserCommand());
		commands.put("showChangePasswordPage", new ShowChangePasswordPageCommand());
		commands.put("changePassword", new ChangePasswordCommand());
		commands.put("changeLocale", new ChangeLocaleCommand());
		commands.put("showLoginPage", new ShowLoginPageCommand());
		commands.put("showRegistrationPage", new ShowRegistrationPageCommand());
		commands.put("showTestInfoPage", new ShowTestInfoPageCommand());
		

		// student commands
		commands.put("showObligatoryTestsPage", new ShowObligatoryTestsPageCommand());
		commands.put("startTest", new StartTestCommand());
		commands.put("chooseQuestion", new ChooseQuestionCommand());
		commands.put("confirmAnswer", new ConfirmAnswerCommand());
		commands.put("finishTest", new FinishTestCommand());
		commands.put("addInstitution", new AddInstitutionCommand());
		commands.put("showAddInstitutionPage", new ShowAddInstitutionPageCommand());
		commands.put("showTestResult", new ShowTestResultCommand());

		// educator commands
		commands.put("showSetTestsPage", new ShowSetTestsPageCommand());
		commands.put("showStudentRatingPage", new ShowStudentRatingPageCommand());
		commands.put("showTestQuestions", new ShowTestQuestionsCommand());

		// admin commands
		commands.put("showCreateTestPage", new ShowCreateTestPageCommand());
		commands.put("showCreateQuestionPage", new ShowCreateQuestionPageCommand());
		commands.put("confirmCreateQuestion", new ConfirmCreateQuestionCommand());
		commands.put("showCreateCategoryPage", new ShowCreateCategoryPageCommand());
		commands.put("createCategory", new CreateCategoryCommand());
		commands.put("showCreateSubjectPage", new ShowCreateSubjectPageCommand());
		commands.put("createSubject", new CreateSubjectCommand());
		commands.put("showSearchCategoriesPage", new ShowSearchCategoriesPageCommand());
		commands.put("searchCategories", new SearchCategoriesCommand());
		commands.put("editCategory", new EditCategoryCommand());
		commands.put("showEditTestPage", new ShowEditTestPageCommand());
		commands.put("editTest", new EditTestCommand());
		commands.put("showCreateInstitutionPage", new ShowCreateInstitutionPageCommand());
		commands.put("showCreateFacultyPage", new ShowCreateFacultyPageCommand());
		commands.put("showCreateGroupPage", new ShowCreateGroupPageCommand());
		commands.put("createInstitution", new CreateInstitutionCommand());
		commands.put("createFaculty", new CreateFacultyCommand());
		commands.put("createGroup", new CreateGroupCommand());
		commands.put("showSearchSubjectsPage", new ShowSearchSubjectsPageCommand());
		commands.put("searchSubjects", new SearchSubjectsCommand());
		commands.put("editSubject", new EditSubjectCommand());
		commands.put("showConfirmUsersPage", new ShowConfirmUsersPageCommand());
		commands.put("confirmUsers", new ConfirmUsersCommand());
		commands.put("showSearchUsersPage", new ShowSearchUsersPageCommand());
		commands.put("searchUsers", new SearchUsersCommand());
		commands.put("lockUsers", new LockUsersCommand());
		commands.put("showSearchInstitutionsPage", new ShowSearchInstitutionsPageCommand());
		commands.put("showSearchFacultiesPage", new ShowSearchFacultiesPageCommand());
		commands.put("showSearchGroupsPage", new ShowSearchGroupsPageCommand());
		commands.put("searchInstitutions", new SearchInstitutionsCommand());
		commands.put("searchFaculties", new SearchFacultiesCommand());
		commands.put("searchGroups", new SearchGroupsCommand());
		commands.put("showSearchGroupsPage", new ShowSearchGroupsPageCommand());
		commands.put("editInstitution", new EditInstitutionCommand());
		commands.put("editFaculty", new EditFacultyCommand());
		commands.put("editGroup", new EditGroupCommand());
		commands.put("showEditCategoryPage", new ShowEditCategoryPageCommand());
		commands.put("showEditFacultyPage", new ShowEditFacultyPageCommand());
		commands.put("showEditInstitutionPage", new ShowEditInstitutionPageCommand());
		commands.put("showEditSubjectPage", new ShowEditSubjectPageCommand());
		commands.put("showEditGroupPage", new ShowEditGroupPageCommand());
		commands.put("deleteTests", new DeleteTestsCommand());
		commands.put("showTestRatingPage", new ShowTestRatingPageCommand());

		LOG.debug("Command container was successfully initialized");
		LOG.trace("Number of commands --> " + commands.size());
	}

	/**
	 * Returns command object with the given name.
	 * 
	 * @param commandName
	 *            Name of the command.
	 * @return Command object.
	 */
	public static Command get(String commandName) {
		if (commandName == null || !commands.containsKey(commandName)) {
			LOG.trace("Command not found, name --> " + commandName);
			return commands.get("noCommand");
		}
		return commands.get(commandName);
	}
}
