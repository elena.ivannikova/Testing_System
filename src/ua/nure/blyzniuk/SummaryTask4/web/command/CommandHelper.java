package ua.nure.blyzniuk.SummaryTask4.web.command;

public class CommandHelper {

	private boolean redirect;
	private String page;

	public CommandHelper(boolean redirect, String page) {
		this.redirect = redirect;
		this.page = page;
	}

	public CommandHelper(String page) {
		super();
		this.page = page;
	}

	public CommandHelper() {
	}

	public boolean isRedirect() {
		return redirect;
	}

	public String getPage() {
		return page;
	}
}
