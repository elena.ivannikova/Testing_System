package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.MSG_TEST_CREATED;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminCreateService;

public class ConfirmCreateQuestionCommand extends Command {

	private static final long serialVersionUID = 836241837645218832L;
	private static final Logger LOG = Logger.getLogger(ConfirmCreateQuestionCommand.class);
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_CREATE_QUESTION);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		// get container for test from session
		Object testQuestions = session.getAttribute("newTest");

		// get parameters from request
		String ordinal = request.getParameter("questionOrdinal");
		LOG.trace("Request parameter: questionOrdinal --> " + ordinal);
		
		String variantsNumber = request.getParameter("variantsNumber");
		LOG.trace("Request parameter: variantsNumber --> " + variantsNumber);
		
		request.setAttribute("questionOrdinal", ordinal);
		LOG.trace("Set the request attribute: questionOrdinal --> " + ordinal);
		
		request.setAttribute("variantsNumber", variantsNumber);
		LOG.trace("Set the request attribute: variantsNumber --> " + variantsNumber);

		if (request.getParameter("questionCreateSubmit") != null) {
			String[] answerText = request.getParameterValues("answerText");
			String[] correctAnswers = request.getParameterValues("correctAnswers");
			String questionText = request.getParameter("questionText");
			
			request.setAttribute("questionText", questionText);
			LOG.trace("Set the request attribute: questionText --> " + questionText);
			
			request.setAttribute("answerText", answerText);
			LOG.trace("Set the request attribute: answerText --> " + answerText);
			
			AdminCreateService.createQuestion(testQuestions, ordinal, variantsNumber, answerText, correctAnswers, questionText);
			LOG.trace("Created question by ordinal --> " + ordinal);
			
			request.setAttribute("variantsNumber", null);
			LOG.trace("Set the request attribute: variantsNumber --> " + null);
			
			request.setAttribute("questionText", null);
			LOG.trace("Set the request attribute: questionText --> " + null);
			
			request.setAttribute("answerText", null);
			LOG.trace("Set the request attribute: answerText --> " + null);

			int questionOrdinal = Integer.parseInt(ordinal) + 1;
			ordinal = String.valueOf(questionOrdinal);
			
			TestQuestionsBean testing = (TestQuestionsBean) testQuestions;

			// if the last question is reached, create new test in the database
			if (questionOrdinal > testing.getQuestionNumber()) {
				
				AdminCreateService.createTest(testing);
				
				session.setAttribute("message", MSG_TEST_CREATED);
				LOG.trace("Set the session attribute: message --> " + MSG_TEST_CREATED);
				
				// clear session attribute containing current test for creation
				session.setAttribute("newTest", null);
				LOG.trace("Set the session attribute: newTest --> " + null);
				
				return new CommandHelper(true, Path.PAGE_MAIN);
			}
			
			session.setAttribute("newTest", testQuestions);
			LOG.trace("Set the session attribute: newTest --> " + testQuestions);
		}
		
		request.setAttribute("questionOrdinal", ordinal);
		LOG.trace("Set the request attribute: questionOrdinal --> " + ordinal);
		
		return getTargetOperation();
	}
}
