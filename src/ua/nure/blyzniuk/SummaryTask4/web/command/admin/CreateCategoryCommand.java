package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.MSG_CATEGORY_ADDED;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminCreateService;

public class CreateCategoryCommand extends Command {

	private static final long serialVersionUID = -3906360481634376117L;
	private static final Logger LOG = Logger.getLogger(CreateCategoryCommand.class);

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_CREATE_CATEGORY_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		Language language = (Language) session.getAttribute("language");
		LOG.trace("Session attribute: language --> " + language);
		
		String categoryName = request.getParameter("categoryName");
		LOG.trace("Request parameter: categoryName --> " + categoryName);
		
		AdminCreateService.createCategory(categoryName, language);
		
		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, MSG_CATEGORY_ADDED);
		LOG.trace("Set the session attribute: message --> " + MSG_CATEGORY_ADDED);

		return new CommandHelper(true, getTargetPath());
	}
}
