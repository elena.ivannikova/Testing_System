package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminCreateService;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class CreateFacultyCommand extends Command {

	private static final long serialVersionUID = 8588397761198254800L;
	private static final Logger LOG = Logger.getLogger(CreateFacultyCommand.class);
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_CREATE_FACULTY_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		// obtain request parameters
		String institution = request.getParameter("institution");
		LOG.trace("Request parameter: institution --> " + institution);
		
		String facultyName = request.getParameter("facultyName");
		LOG.trace("Request parameter: facultyName --> " + facultyName);
		
		AdminCreateService.createFaculty(facultyName, institution);

		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, MSG_FACULTY_ADDED);
		LOG.trace("Set the session attribute: message --> " + MSG_FACULTY_ADDED);

		return new CommandHelper(true, getTargetPath());
	}
}
