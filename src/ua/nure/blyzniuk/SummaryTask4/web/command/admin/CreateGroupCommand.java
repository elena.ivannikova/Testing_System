package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminCreateService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class CreateGroupCommand extends Command {

	private static final long serialVersionUID = 6642059847223010950L;
	private static final Logger LOG = Logger.getLogger(CreateCategoryCommand.class);

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_CREATE_GROUP_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		LOG.debug("Command starts");

		HttpSession session = request.getSession();
		
		Country country = (Country)session.getAttribute(SessionAttributes.SESS_ATTR_COUNTRY);
		List<University> institutions = CommonService.getAllInstitutionsByCountry(country);
		LOG.trace("Found in DB: institutions --> " + institutions);
		
		request.setAttribute("institutions", institutions);
		LOG.trace("Set the request attribute: institutions --> " + institutions);

		String institution = request.getParameter("institution");
		
		if (institution != null) {
			
			request.setAttribute("institution", institution);
			
			List<Faculty> faculties = CommonService.searchFacultiesByInstitutionId(institution);
			LOG.trace("Found in DB: faculties --> " + faculties);
			
			request.setAttribute("faculties", faculties);
			LOG.trace("Set the request attribute: faculties --> " + faculties);
			
			String faculty = request.getParameter("faculty");
			LOG.trace("Get the request parameter: faculty --> " + faculty);
			
			if (faculty != null) {
				
				request.setAttribute("faculty", faculty);
				LOG.trace("Set the request attribute: faculty --> " + faculty);
				
				String groupName = request.getParameter("groupName");
				LOG.trace("Get the request parameter: groupName --> " + groupName);
				
				request.setAttribute("groupName", groupName);
				LOG.trace("Set the request attribute: groupName --> " + groupName);
				
				if (request.getParameter("confirm") != null) {
					
					AdminCreateService.createGroup(groupName, faculty);
					
					session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, MSG_GROUP_ADDED);
					LOG.trace("Set the session attribute: message --> " + MSG_GROUP_ADDED);
					
					return new CommandHelper(true, getTargetPath());
				}
			}
		}

		LOG.debug("Command finished");
		return new CommandHelper(Path.PAGE_CREATE_GROUP);
	}
}
