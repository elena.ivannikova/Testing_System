package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminCreateService;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class CreateInstitutionCommand extends Command {

	private static final long serialVersionUID = 3901486756150850115L;
	private static final Logger LOG = Logger.getLogger(CreateInstitutionCommand.class);
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_CREATE_INSTITUTION_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		Country country = (Country) session.getAttribute("country");
		String institutionName = request.getParameter("institutionName");
		
		AdminCreateService.createInstitution(institutionName, country);
		
		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, MSG_INSTITUTION_ADDED);

		LOG.debug("Command finished");
		return new CommandHelper(true, getTargetPath());
	}
}
