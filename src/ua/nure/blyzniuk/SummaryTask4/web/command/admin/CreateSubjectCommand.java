package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminCreateService;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class CreateSubjectCommand extends Command {

	private static final long serialVersionUID = -1062686286006149620L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_CREATE_SUBJECT_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		String category = request.getParameter("category");
		String subjectName = request.getParameter("subjectName");
		
		AdminCreateService.createSubject(subjectName, category);
		
		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, MSG_SUBJECT_ADDED);

		return new CommandHelper(true, getTargetPath());
	}
}
