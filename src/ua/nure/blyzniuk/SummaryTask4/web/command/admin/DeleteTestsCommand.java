package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminService;

public class DeleteTestsCommand extends Command {

	private static final long serialVersionUID = -6986388618208896131L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SEARCH_TESTS + "&confirm=confirm");
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		String[] tests = request.getParameterValues("tests");

		AdminService.deleteTestsById(tests);

		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.tests_deleted");

		return new CommandHelper(true, getTargetPath());
	}
}
