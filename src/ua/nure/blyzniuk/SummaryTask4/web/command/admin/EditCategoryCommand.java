package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminUpdateService;

public class EditCategoryCommand extends Command {

	private static final long serialVersionUID = 7161453905838410047L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_EDIT_CATEGORY_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		request.setAttribute("categoryId", request.getAttribute("categoryId"));

		if (request.getParameter("confirm") == null) {
			request.setAttribute("categoryName", request.getAttribute("categoryName"));
		} else {
			String categoryName = request.getParameter("categoryName");
			String categoryId = request.getParameter("categoryId");
			request.setAttribute("categoryName", categoryName);
			request.setAttribute("categoryId", categoryId);
			AdminUpdateService.updateCategory(categoryName, categoryId);
			session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.category_updated");
		}

		return new CommandHelper(true, getTargetPath());
	}
}
