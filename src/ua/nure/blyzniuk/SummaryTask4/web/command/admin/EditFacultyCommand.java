package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminUpdateService;

public class EditFacultyCommand extends Command {

	private static final long serialVersionUID = 590706213266094948L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_EDIT_FACULTY_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		request.setAttribute("facultyId", request.getAttribute("facultyId"));

		if (request.getParameter("confirm") == null) {
			request.setAttribute("facultyName", request.getAttribute("facultyName"));
		} else {
			String facultyName = request.getParameter("facultyName");
			String facultyId = request.getParameter("facultyId");
			request.setAttribute("facultyName", facultyName);
			request.setAttribute("facultyId", facultyId);
			AdminUpdateService.updateFaculty(facultyName, facultyId);
			session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.faculty_updated");
		}

		return new CommandHelper(true, getTargetPath());
	}
}
