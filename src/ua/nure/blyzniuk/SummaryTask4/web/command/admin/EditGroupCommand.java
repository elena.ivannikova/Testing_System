package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminUpdateService;

public class EditGroupCommand extends Command {

	private static final long serialVersionUID = -7742844790536519017L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_EDIT_GROUP_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		request.setAttribute("groupId", request.getAttribute("groupId"));

		if (request.getParameter("confirm") == null) {
			request.setAttribute("groupName", request.getAttribute("groupName"));
		} else {
			String groupName = request.getParameter("groupName");
			String groupId = request.getParameter("groupId");
			request.setAttribute("groupName", groupName);
			request.setAttribute("groupId", groupId);
			AdminUpdateService.updateGroup(groupName, groupId);
			session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.group_updated");
		}

		return new CommandHelper(true, getTargetPath());
	}
}
