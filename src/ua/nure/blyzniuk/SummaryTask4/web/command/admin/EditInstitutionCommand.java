package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminUpdateService;

public class EditInstitutionCommand extends Command {

	private static final long serialVersionUID = -4535551874155974066L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_EDIT_INSTITUTION_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		request.setAttribute("institutionId", request.getAttribute("institutionId"));

		if (request.getParameter("confirm") == null) {
			request.setAttribute("institutionName", request.getAttribute("institutionName"));
		} else {
			String institutionName = request.getParameter("institutionName");
			String institutionId = request.getParameter("institutionId");
			request.setAttribute("institutionName", institutionName);
			request.setAttribute("institutionId", institutionId);
			AdminUpdateService.updateInstitution(institutionName, institutionId);
			session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.institution_updated");
		}

		return new CommandHelper(true, getTargetPath());
	}
}
