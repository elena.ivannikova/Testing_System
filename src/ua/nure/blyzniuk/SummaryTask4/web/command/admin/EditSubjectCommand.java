package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminUpdateService;

public class EditSubjectCommand extends Command {

	private static final long serialVersionUID = -5915263518849979001L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_EDIT_SUBJECT_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		request.setAttribute("subjectId", request.getAttribute("subjectId"));

		if (request.getParameter("confirm") == null) {
			request.setAttribute("subjectName", request.getAttribute("subjectName"));
		} else {
			String subjectName = request.getParameter("subjectName");
			String subjectId = request.getParameter("subjectId");
			request.setAttribute("subjectName", subjectName);
			request.setAttribute("subjectId", subjectId);
			AdminUpdateService.updateSubject(subjectName, subjectId);
			session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.subject_updated");
		}

		return new CommandHelper(true, getTargetPath());
	}
}
