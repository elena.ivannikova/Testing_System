package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminUpdateService;

public class EditTestCommand extends Command {

	private static final long serialVersionUID = -8367326416919927442L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_EDIT_TEST_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		String testId = request.getParameter("testId");
		String testName = request.getParameter("testName");
		String complexityId = request.getParameter("complexity");
		String duration = request.getParameter("duration");
		String freeAccess = request.getParameter("freeAccess");
		
		AdminUpdateService.updateTest(testId, testName, complexityId, duration, freeAccess);
		
		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.test_updated");
		
		return new CommandHelper(getTargetPath());
	}
}
