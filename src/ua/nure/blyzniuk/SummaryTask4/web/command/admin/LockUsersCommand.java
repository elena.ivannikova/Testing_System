package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminService;

import static ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes.*;
import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class LockUsersCommand extends Command {

	private static final long serialVersionUID = -6280885126904684891L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SEARCH_USERS);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		String[] users = request.getParameterValues("users");
		boolean locking = request.getParameter("confirmLock") != null && request.getParameter("confirmUnlock") == null;
		
		AdminService.setUsersLock(locking, users);
		
		String message = locking ? MSG_USERS_LOCKED : MSG_USERS_UNLOCKED;
		session.setAttribute(SESS_ATTR_MESSAGE, message);
		
		return new CommandHelper(true, getTargetPath());
	}
}
