package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class SearchCategoriesCommand extends Command {

	private static final long serialVersionUID = 4806889720633783286L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_SEARCH_CATEGORIES_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session=request.getSession();
		
		if (request.getParameter("confirm") == null) {
			request.setAttribute("categoryName", request.getAttribute("categoryName"));
		} else {
			String categoryName = request.getParameter("categoryName");
			request.setAttribute("categoryName", categoryName);
			Language language = (Language)session.getAttribute(SessionAttributes.SESS_ATTR_LANGUAGE);
			List<Category> categories = AdminSearchService.searchCategoriesByNameAndLanguage(categoryName, language);
			request.setAttribute("categories", categories);
		}

		return getTargetOperation();
	}
}
