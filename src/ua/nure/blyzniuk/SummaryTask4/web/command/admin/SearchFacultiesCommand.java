package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class SearchFacultiesCommand extends Command {

	private static final long serialVersionUID = -4677502660300311010L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_SEARCH_FACULTIES_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		
		Country country = (Country) session.getAttribute("country");
		List<University> institutions =  CommonService.getAllInstitutionsByCountry(country);
		request.setAttribute("institutions", institutions);
		
		String institution = request.getParameter("institution");
		request.setAttribute("institution", institution);
		
		String faculty = request.getParameter("faculty");
		request.setAttribute("faculty", faculty);
		
		String facultyName = request.getParameter("facultyName");
		request.setAttribute("facultyName", facultyName);
		
		if (request.getParameter("confirm") != null) {
			List<Faculty> faculties = AdminSearchService.searchFacultiesByNameAndInstitutionId(institution, facultyName);
			request.setAttribute("faculties", faculties);
		}
		return getTargetOperation();
	}
}
