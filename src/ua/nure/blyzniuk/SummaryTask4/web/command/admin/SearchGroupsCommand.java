package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

public class SearchGroupsCommand extends Command {

	private static final long serialVersionUID = -7977612238704266681L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_SEARCH_GROUPS_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		Country country = (Country) session.getAttribute(SessionAttributes.SESS_ATTR_COUNTRY);
		List<University> institutions = CommonService.getAllInstitutionsByCountry(country);
		request.setAttribute("institutions", institutions);

		String institution = request.getParameter("institution");
		String faculty = request.getParameter("faculty");
		String groupName = request.getParameter("groupName");
		
		request.setAttribute("institution", institution);
		request.setAttribute("faculty", faculty);
		request.setAttribute("groupName", groupName);

		if (!Validation.isNullOrEmpty(institution)) {
			List<Faculty> faculties = CommonService.searchFacultiesByInstitutionId(institution);
			request.setAttribute("faculties", faculties);
		}
		
		if (request.getParameter("confirm") != null) {
			List<Group> groups = AdminSearchService.searchGroupsByConditions(groupName, institution, faculty);
			request.setAttribute("groups", groups);
		}

		return getTargetOperation();
	}
}
