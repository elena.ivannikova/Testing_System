package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class SearchInstitutionsCommand extends Command {

	private static final long serialVersionUID = 5532383580000660750L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_SEARCH_INSTITUTIONS_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		if (request.getParameter("confirm") == null) {
			request.setAttribute("institutionName", request.getAttribute("institutionName"));
		} else {
			HttpSession session = request.getSession();
			Country country = (Country) session.getAttribute("country");
			String institutionName = request.getParameter("institutionName");
			request.setAttribute("institutionName", institutionName);
			List<University> institutions = AdminSearchService.searchInstitutionsByNameAndCountry(institutionName, country);
			request.setAttribute("institutions", institutions);
		}

		return getTargetOperation();
	}
}
