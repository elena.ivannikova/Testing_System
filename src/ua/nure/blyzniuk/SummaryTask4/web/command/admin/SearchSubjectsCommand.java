package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class SearchSubjectsCommand extends Command {

	private static final long serialVersionUID = -4494102456020797130L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_SEARCH_SUBJECTS_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		Language language = (Language)session.getAttribute(SessionAttributes.SESS_ATTR_LANGUAGE);
		List<Category> categories = CommonService.getAllCategoriesByLanguage(language);
		request.setAttribute("categories", categories);

		String category = request.getParameter("category");
		request.setAttribute("category", category);
		
		String subjectName = request.getParameter("subjectName");
		request.setAttribute("subjectName", subjectName);

		if (request.getParameter("confirm") != null) {
			List<Subject> subjects = AdminSearchService.searchSubjectsByNameAndCategoryId(category, subjectName);
			request.setAttribute("subjects", subjects);
		}

		return new CommandHelper(Path.PAGE_SEARCH_SUBJECTS);
	}
}
