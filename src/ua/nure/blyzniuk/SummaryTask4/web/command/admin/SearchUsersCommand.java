package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.*;

public class SearchUsersCommand extends Command {

	private static final long serialVersionUID = -6405429587406444641L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_SEARCH_USERS);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		String userName = request.getParameter("userName");
		request.setAttribute("userName", userName);

		String locked = request.getParameter("locked");

		if (locked != null) {
			request.setAttribute("checked", "checked");
		}

		Country country = (Country) session.getAttribute(SessionAttributes.SESS_ATTR_COUNTRY);
		List<University> institutions = CommonService.getAllInstitutionsByCountry(country);
		request.setAttribute("institutions", institutions);

		String institution = request.getParameter("institution");
		String faculty = request.getParameter("faculty");
		String group = request.getParameter("group");

		if (!isNullOrEmpty(institution)) {

			request.setAttribute("institution", institution);

			List<Faculty> faculties = CommonService.searchFacultiesByInstitutionId(institution);
			request.setAttribute("faculties", faculties);

			if (!isNullOrEmpty(faculty)) {

				request.setAttribute("faculty", faculty);

				List<Group> groups = CommonService.searchGroupsByFacultyId(faculty);
				request.setAttribute("groups", groups);

				if (!isNullOrEmpty(group)) {
					request.setAttribute("group", group);
				}
			}
		}

		if (request.getParameter("confirm") != null || request.getParameter("confirmLock") != null) {

			List<UserInfoBean> users = AdminSearchService.searchUsersByConditions(userName, locked, institution,
					faculty, group);
			request.setAttribute("users", users);
		}

		return

		getTargetOperation();
	}

}
