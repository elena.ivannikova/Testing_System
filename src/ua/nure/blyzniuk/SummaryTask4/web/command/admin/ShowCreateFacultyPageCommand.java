package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowCreateFacultyPageCommand extends Command {

	private static final long serialVersionUID = -5809953011925377829L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		
		Country country = (Country) session.getAttribute("country");

		List<University> institutions = CommonService.getAllInstitutionsByCountry(country);
		
		// setting attributes
//		request.setAttribute("required", "required");
		request.setAttribute("institutions", institutions);
		request.setAttribute("institution", request.getParameter("institution"));

		return new CommandHelper(Path.PAGE_CREATE_FACULTY);
	}
}
