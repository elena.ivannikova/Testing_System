package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;

public class ShowCreateQuestionPageCommand extends Command {

	private static final long serialVersionUID = 6459049001363158136L;

	@Override
	public String getTargetPath() {
		return Path.PAGE_CREATE_QUESTION;
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		
		request.setAttribute("questionOrdinal", request.getAttribute("questionOrdinal"));
		
		request.setAttribute("variantsNumber", request.getAttribute("variantsNumber"));
		
		return new CommandHelper(getTargetPath());
	}
}
