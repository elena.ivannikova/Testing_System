package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Complexity;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminEducatorService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowCreateTestPageCommand extends Command {

	private static final long serialVersionUID = -5308728412892841260L;

	@Override
	public String getTargetPath() {
		return Path.PAGE_CREATE_TEST;
	}

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(getTargetPath());
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		String testName = request.getParameter("testName");
		String subject = request.getParameter("subject");
		String complexity = request.getParameter("complexity");
		String duration = request.getParameter("duration");
		String questionNumber = request.getParameter("questionNumber");
		String category = request.getParameter("category");
		String freeAccess = request.getParameter("freeAccess");

		request.setAttribute("testName", testName);
		request.setAttribute("subject", subject);
		request.setAttribute("complexity", complexity);
		request.setAttribute("duration", duration);
		request.setAttribute("questionNumber", questionNumber);
		request.setAttribute("category", category);
		request.setAttribute("freeAccess", freeAccess != null ? "checked" : "");

		// setting attributes
		Language language = (Language) session.getAttribute(SessionAttributes.SESS_ATTR_LANGUAGE);
		List<Category> categories = CommonService.getAllCategoriesByLanguage(language);
		request.setAttribute("categories", categories);

		// ComplexityDAO complexityDAO = new ComplexityDAO();
		List<Complexity> complexityList = AdminEducatorService.getComplexityList();
		request.setAttribute("complexityList", complexityList);
		// request.setAttribute("required", "required");

		if (category != null) {
			List<Subject> subjects = CommonService.searchSubjectsByCategoryId(category);
			request.setAttribute("subjects", subjects);
		}

		String path = getTargetPath();

		if (request.getParameter("confirm") != null) {

			TestQuestionsBean testQuestions = AdminService.initTest(testName, subject, complexity, duration,
					questionNumber, freeAccess);

			session.setAttribute("newTest", testQuestions);
			request.setAttribute("questionOrdinal", 1);

			path = Path.COMMAND_SHOW_CREATE_QUESTION_PAGE;

		}

		return new CommandHelper(path);
	}
}
