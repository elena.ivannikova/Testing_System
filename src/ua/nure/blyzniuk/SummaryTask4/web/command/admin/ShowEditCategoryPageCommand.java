package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class ShowEditCategoryPageCommand extends Command {

	private static final long serialVersionUID = -2753432023934462998L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

//		String categoryName = request.getParameter("categoryName");
		String categoryId = request.getParameter("categoryId");
		
		Category category = AdminSearchService.getCategoryById(categoryId);
		
//		request.setAttribute("categoryName", categoryName);
		request.setAttribute("category", category);
		
		return new CommandHelper(Path.PAGE_EDIT_CATEGORY);
	}
}
