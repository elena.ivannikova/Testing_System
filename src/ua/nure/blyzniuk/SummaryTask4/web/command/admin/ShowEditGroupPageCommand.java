package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class ShowEditGroupPageCommand extends Command {

	private static final long serialVersionUID = 8792052455978435962L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

//		String groupName = request.getParameter("groupName");
		String groupId = request.getParameter("groupId");
		
		Group group = AdminSearchService.getGroupById(groupId);

//		request.setAttribute("groupName", groupName);
		request.setAttribute("group", group);

		return new CommandHelper(Path.PAGE_EDIT_GROUP);
	}
}
