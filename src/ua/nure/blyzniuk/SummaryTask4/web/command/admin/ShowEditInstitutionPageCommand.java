package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class ShowEditInstitutionPageCommand extends Command {

	private static final long serialVersionUID = -8712861816355464829L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String institutionId = request.getParameter("institutionId");
		
		University institution = AdminSearchService.getInstitutionById(institutionId);

		request.setAttribute("institution", institution);

		return new CommandHelper(Path.PAGE_EDIT_INSTITUTION);
	}
}
