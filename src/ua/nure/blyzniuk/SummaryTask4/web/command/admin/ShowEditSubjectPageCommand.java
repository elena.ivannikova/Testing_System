package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class ShowEditSubjectPageCommand extends Command {

	private static final long serialVersionUID = -8228081778692751779L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		String subjectId = request.getParameter("subjectId");
		
		Subject subject = AdminSearchService.getSubjectById(subjectId);

		request.setAttribute("subject", subject);

		return new CommandHelper(Path.PAGE_EDIT_SUBJECT);
	}
}
