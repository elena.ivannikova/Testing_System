package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Complexity;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminEducatorService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowEditTestPageCommand extends Command {

	private static final long serialVersionUID = -835902379293886071L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		String testId = request.getParameter("testId");
		
		TestInfoBean testInfo = CommonService.getTestInfoById(testId);
		request.setAttribute("testInfo", testInfo);
		
		List<Complexity> complexityList = AdminEducatorService.getComplexityList(); //complexityDAO.getAll();
		request.setAttribute("complexityList", complexityList);
		
		return new CommandHelper(Path.PAGE_EDIT_TEST);
	}
}
