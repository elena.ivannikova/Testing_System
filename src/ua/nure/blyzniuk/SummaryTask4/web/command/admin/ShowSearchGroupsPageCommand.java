package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowSearchGroupsPageCommand extends Command {

	private static final long serialVersionUID = 4380187883839857305L;

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		Country country = (Country) session.getAttribute("country");
		List<University> institutions =  CommonService.getAllInstitutionsByCountry(country);
		request.setAttribute("institutions", institutions);
		
		return new CommandHelper(Path.PAGE_SEARCH_GROUPS);
	}
}
