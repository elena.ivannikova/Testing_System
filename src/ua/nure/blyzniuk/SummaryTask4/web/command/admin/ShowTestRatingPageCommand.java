package ua.nure.blyzniuk.SummaryTask4.web.command.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestRatingBean;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminSearchService;

public class ShowTestRatingPageCommand extends Command {

	private static final long serialVersionUID = 4665677142823681507L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_SEARCH_TESTS);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		String testId = request.getParameter("testId");
		
		List<TestRatingBean> rating = AdminSearchService.getTestRatingsByTestId(testId);
		request.setAttribute("rating", rating);
		request.setAttribute("testName", request.getParameter("testName"));
		
		return new CommandHelper(Path.PAGE_TEST_RATING);
	}
}
