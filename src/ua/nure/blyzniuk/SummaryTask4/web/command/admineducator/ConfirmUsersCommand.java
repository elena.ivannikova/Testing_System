package ua.nure.blyzniuk.SummaryTask4.web.command.admineducator;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminService;

public class ConfirmUsersCommand extends Command {

	private static final long serialVersionUID = 823001567948206265L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SEARCH_USERS);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();

		String[] userGroupBeans = request.getParameterValues("users");

		String reject = request.getParameter("reject");

		AdminService.confirmUserGroups(userGroupBeans, reject != null);
		
		String message = reject != null ? "info_msg.user_reject_success" : "info_msg.user_confirm_success";
		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, message);

		return new CommandHelper(true, getTargetPath());
	}
}
