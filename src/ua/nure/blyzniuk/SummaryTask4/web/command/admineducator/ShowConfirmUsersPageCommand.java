package ua.nure.blyzniuk.SummaryTask4.web.command.admineducator;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupDetailBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.AdminEducatorService;

public class ShowConfirmUsersPageCommand extends Command {

	private static final long serialVersionUID = -9075080787008081871L;
	
/*	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper();
	}*/

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		String userRole = session.getAttribute("userRole").toString();
		
		User user = (User)session.getAttribute("user");

		List<UserGroupDetailBean> users = AdminEducatorService.getAllUsersToConfirm(userRole, user);

		request.setAttribute("users", users);

		return new CommandHelper(Path.PAGE_CONFIRM_USERS);
	}
}
