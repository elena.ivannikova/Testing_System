package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import static ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes.SESS_ATTR_MESSAGE;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class ChangePasswordCommand extends Command {

	private static final long serialVersionUID = -7981258600376511507L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(true, Path.COMMAND_SHOW_CHANGE_PASSWORD_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		String oldPassword = request.getParameter("currentPassword");
		String[] newPassword = request.getParameterValues("newPassword");
		
		CommonService.changePassword(user.getId(), oldPassword, newPassword);

		session.setAttribute(SESS_ATTR_MESSAGE, MSG_PASSWORD_CHANGED);
		return new CommandHelper(true, getTargetPath());
	}
}
