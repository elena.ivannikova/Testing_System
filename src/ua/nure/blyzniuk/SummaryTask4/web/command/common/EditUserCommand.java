package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class EditUserCommand extends Command {

	private static final long serialVersionUID = -3355700764721325760L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(true, Path.COMMAND_SHOW_EDIT_USER_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		// obtain parameters from a request
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String login = request.getParameter("login");

		// obtain current user as a session attribute
		User user = (User) session.getAttribute("user");
		user = CommonService.updateUser(user.getId(), firstName, lastName, email, login);
		
		// set session attributes
		session.setAttribute("user", user);
		session.setAttribute("message", "Changes were successfully saved.");

		return new CommandHelper(true, Path.PAGE_MAIN);
	}
}
