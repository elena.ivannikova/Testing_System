package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;

/**
 * No command.
 * 
 * @author Alena
 */
public class NoCommand extends Command {
	
	private static final long serialVersionUID = -2785976616686657267L;
	private static final Logger LOG = Logger.getLogger(NoCommand.class);

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response) {
		
		LOG.debug("Command starts");
		
		HttpSession session = request.getSession();

		String message = "No such command";
		session.setAttribute("message", message);
		LOG.error("Set the session attribute: message --> " + message);

		LOG.debug("Command finished");
		return new CommandHelper(Path.PAGE_MAIN);
	}
}
