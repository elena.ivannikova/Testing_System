package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.comparator.TestComparator;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

public class SearchTestsCommand extends Command {

	private static final long serialVersionUID = -793998180105076512L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_SEARCH_TESTS);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		String testName = request.getParameter("testName");
		request.setAttribute("testName", testName);
		
		String subject = request.getParameter("subject");
		request.setAttribute("subject", subject);

		String[] sorting = TestComparator.getValues();
		request.setAttribute("sorting", sorting);
		
		User user = (User)session.getAttribute(SessionAttributes.SESS_ATTR_USER);
		Role role = Role.getRole(user);

		Language language = (Language)session.getAttribute(SessionAttributes.SESS_ATTR_LANGUAGE);
		List<Category> categories = CommonService.getAllCategoriesByLanguage(language);
		request.setAttribute("categories", categories);

		// getting parameters
		String category = request.getParameter("category");
		if (!Validation.isNullOrEmpty(category)) {
			request.setAttribute("category", category);
			List<Subject> subjects = CommonService.searchSubjectsByCategoryId(category);
			request.setAttribute("subjects", subjects);
		}
		
		String sort = request.getParameter("sorting");
		request.setAttribute("sort", sort);

		if (request.getParameter("confirm") != null) {
			List<TestInfoBean> foundTests = CommonService.searchTestsByConditions(language.getId(), role, testName, category, subject, sort);
			request.setAttribute("foundTests", foundTests);
		}

		return new CommandHelper(Path.PAGE_SEARCH_TESTS);
	}
}
