package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserPassedTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowPersonalAccountCommand extends Command {

	private static final long serialVersionUID = -1477982727236871676L;
	private static final Logger LOG = Logger.getLogger(ShowPersonalAccountCommand.class);

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_PERSONAL_ACCOUNT);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		// obtain attributes from a session
		User user = (User) session.getAttribute("user");
		LOG.trace("Session attribute: user --> " + user);
		String userRole = session.getAttribute("userRole").toString().toLowerCase();
		LOG.trace("Session attribute: userRole --> " + userRole);
		
		if (userRole.equals("student") || userRole.equals("educator")) {
			// obtain institutions by current user id
			List<UserGroupBean> institutions = CommonService.getInstitutionsByUserId(user.getId());
			// set request attribute
			request.setAttribute("institutions", institutions);
			LOG.trace("Set the request attribute: institutions --> " + institutions);
		}

		if (userRole.equals("student")) {
			// obtain passed tests by current student id
			List<UserPassedTestsBean> studentTestsList = CommonService.getPassedTestsByStudentId(user.getId());
			// set request attribute
			request.setAttribute("studentTestsList", studentTestsList);
			LOG.trace("Set the request attribute: studentTestsList --> " + studentTestsList);
		}

		LOG.debug("Command finished");
		return getTargetOperation();
	}
}
