package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowTestInfoCommand extends Command {

	private static final long serialVersionUID = -222834091615731258L;
	private static final Logger LOG = Logger.getLogger(ShowTestInfoCommand.class);

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		HttpSession session = request.getSession();
		
		LOG.debug("Command starts");

		// obtain parameters from a request
		String testId = request.getParameter("testId");
		LOG.trace("Request parameter: test id --> " + testId);

		TestInfoBean testInfo = CommonService.getTestInfoById(testId);
		LOG.trace("Found in DB: test info --> " + testInfo);

		session.setAttribute("testInfo", testInfo);
		LOG.trace("Set the request attribute: testInfo --> " + testInfo);

		LOG.debug("Command finished");
		return new CommandHelper(true, Path.COMMAND_SHOW_TEST_INFO_PAGE);
	}
}
