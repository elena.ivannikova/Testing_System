package ua.nure.blyzniuk.SummaryTask4.web.command.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowTestQuestionsCommand extends Command {

	private static final long serialVersionUID = -710432444137104659L;
	private static final Logger LOG = Logger.getLogger(ShowTestQuestionsCommand.class);

	@Override
	public String getTargetPath() {
		return Path.COMMAND_PERFORM_TEST + "1";
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Command starts");

		HttpSession session = request.getSession();
		Object userRole = session.getAttribute("userRole");

		// obtain parameters from a request
		String testId = request.getParameter("testId");
		LOG.trace("Request parameter: testId --> " + testId);

		TestQuestionsBean testing = CommonService.getTestQuestionsByTestId(testId, userRole);
		LOG.trace("Found in DB: testing --> " + testing);

		session.setAttribute("testing", testing);
		LOG.trace("Set the request attribute: testing --> " + testing);

		LOG.debug("Command finished");
		// return forward;
		return new CommandHelper(getTargetPath());
	}
}
