package ua.nure.blyzniuk.SummaryTask4.web.command.educator;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupDetailBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.EducatorService;

import static ua.nure.blyzniuk.SummaryTask4.web.InfoMessages.*;

public class ShowSetTestsPageCommand extends Command {

	private static final long serialVersionUID = -5732927030137739717L;

	private static final Logger LOG = Logger.getLogger(ShowSetTestsPageCommand.class);
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_SET_TESTS);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		// obtain user attribute from a session
		User user = (User) session.getAttribute("user");
		LOG.trace("Session attribute: user --> " + user);

		Role role = Role.getRole(user);

		// obtain parameters from a request
		String testName = request.getParameter("testName");
		String subject = request.getParameter("subject");
		String category = request.getParameter("category");

		request.setAttribute("testName", testName);
		request.setAttribute("subject", subject);
		request.setAttribute("category", category);

		Language language = (Language) session.getAttribute("language");
		List<Category> categories = CommonService.getAllCategoriesByLanguage(language);
		request.setAttribute("categories", categories);

		if (category != null) {
			List<Subject> subjects = CommonService.searchSubjectsByCategoryId(category);
			request.setAttribute("subjects", subjects);
		}

		String name = request.getParameter("name");
		request.setAttribute("name", name);
		List<UserGroupDetailBean> institutions = EducatorService.getAllGroupsByNameAndEducator(user, name);

		request.setAttribute("institutions", institutions);
		LOG.trace("Set the request attribute: institutions --> " + institutions);

		if (request.getParameter("confirm") != null) {

			String[] groups = request.getParameterValues("institutions");
			String[] foundTests = request.getParameterValues("foundTests");
			String dateEnd = request.getParameter("dateEnd");

			EducatorService.setTests(groups, foundTests, dateEnd);
			session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, MSG_ASSIGN_TESTS_SUCCESS);

			return new CommandHelper(true, getTargetPath());
		}

		if (request.getParameter("search") != null) {
			List<TestInfoBean> foundTests = CommonService.searchTestsByConditions(language.getId(), role, testName,
					category, subject, null);
			request.setAttribute("foundTests", foundTests);
		}

		LOG.debug("Command finished");
		return new CommandHelper(Path.PAGE_SET_TESTS);
	}
}
