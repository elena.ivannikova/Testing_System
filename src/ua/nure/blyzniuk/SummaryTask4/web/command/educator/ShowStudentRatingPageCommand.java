package ua.nure.blyzniuk.SummaryTask4.web.command.educator;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserTestResultsBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.EducatorService;

public class ShowStudentRatingPageCommand extends Command {

	private static final long serialVersionUID = 649988065111590630L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_STUDENT_RATING);
	}

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.web.command.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");

		String institution = request.getParameter("institution");
		String faculty = request.getParameter("faculty");
		String group = request.getParameter("group");

		request.setAttribute("institution", institution);
		request.setAttribute("faculty", faculty);
		request.setAttribute("group", group);
		
		request.setAttribute("disabled", "disabled");

		List<University> institutions = EducatorService.getAllInstitutionsByEducator(user);
		request.setAttribute("institutions", institutions);

		if (institution != null && !institution.equals("")) {
			List<Faculty> faculties = EducatorService.getFacultiesByEducatorIdAndInstitutionId(user, institution);
			request.setAttribute("faculties", faculties);
			if (faculty != null && !faculty.equals("")) {
				List<Group> groups = EducatorService.getGroupsByEducatorIdAndInstitutionId(user, faculty);
				request.setAttribute("groups", groups);
			}
		}

		if (request.getParameter("search") != null) {

			Set<String> testSet = EducatorService.getAllUserTestResultsForEducator(user, institution, faculty, group);
			request.setAttribute("testSet", testSet);
			Map<UserTestResultsBean, TreeSet<UserTestResultsBean>> testResults = EducatorService.getTestResults(user,
					institution, faculty, group);
			
			request.setAttribute("testResults", testResults);
		}

		return getTargetOperation();
	}
}
