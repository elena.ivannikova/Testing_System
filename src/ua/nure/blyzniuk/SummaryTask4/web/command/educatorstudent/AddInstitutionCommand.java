package ua.nure.blyzniuk.SummaryTask4.web.command.educatorstudent;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.EducatorStudentService;

public class AddInstitutionCommand extends Command {

	private static final long serialVersionUID = 3492598964656909460L;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_SHOW_ADD_INSTITUTION_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();

		Country country = (Country) session.getAttribute(SessionAttributes.SESS_ATTR_COUNTRY);
		List<University> institutions = CommonService.getAllInstitutionsByCountry(country);
		request.setAttribute("institutions", institutions);

		String institution = request.getParameter("institution");

		if (institution != null) {

			request.setAttribute("institution", institution);

			List<Faculty> faculties = CommonService.searchFacultiesByInstitutionId(institution);
			request.setAttribute("faculties", faculties);

			String faculty = request.getParameter("faculty");

			if (faculty != null) {

				request.setAttribute("faculty", faculty);

				User user = (User) session.getAttribute("user");

				List<Group> groups = EducatorStudentService.searchGroupsForUserChoise(faculty, user);
				request.setAttribute("groups", groups);

				String group = request.getParameter("group");
				request.setAttribute("group", group);

				if (request.getParameter("confirm") != null) {

					EducatorStudentService.addUserGroup(user, group);

					session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE,
							"info_msg.wait_for_confirmation");

					return new CommandHelper(true, Path.COMMAND_SHOW_PERSONAL_ACCOUNT);
				}
			}
		}

		return new CommandHelper(Path.PAGE_ADD_INSTITUTION);
	}
}
