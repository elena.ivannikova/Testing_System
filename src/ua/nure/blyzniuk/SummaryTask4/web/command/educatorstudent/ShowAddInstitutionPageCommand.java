package ua.nure.blyzniuk.SummaryTask4.web.command.educatorstudent;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class ShowAddInstitutionPageCommand extends Command {

	private static final long serialVersionUID = -852391418564088786L;
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.PAGE_ADD_INSTITUTION);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		HttpSession session = request.getSession();
		
		Country country = (Country)session.getAttribute(SessionAttributes.SESS_ATTR_COUNTRY);
		
		List<University> institutions = CommonService.getAllInstitutionsByCountry(country);

		request.setAttribute("institutions", institutions);
		
		request.setAttribute("institution", request.getAttribute("institution"));
		request.setAttribute("faculty", request.getAttribute("faculty"));
		request.setAttribute("group", request.getAttribute("group"));

		return getTargetOperation();
	}
}
