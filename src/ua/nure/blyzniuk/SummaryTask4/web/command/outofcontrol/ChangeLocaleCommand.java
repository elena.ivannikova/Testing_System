package ua.nure.blyzniuk.SummaryTask4.web.command.outofcontrol;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;

public class ChangeLocaleCommand extends Command {

	private static final long serialVersionUID = 4937005900213970390L;

	/**
	 * @see ua.nure.blyzniuk.SummaryTask4.web.command.Command#execute(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		return new CommandHelper(Path.PAGE_CHANGE_LOCALE);
	}
}
