package ua.nure.blyzniuk.SummaryTask4.web.command.outofcontrol;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

/**
 * @author Alena
 *
 */
public class LoginCommand extends Command {

	private static final long serialVersionUID = -7342712848085895787L;
	private static final Logger LOG = Logger.getLogger(LoginCommand.class);

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(true, Path.COMMAND_SHOW_LOGIN_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");

		HttpSession session = request.getSession();

		// obtain login and password from a request
		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);
		String password = request.getParameter("password");

		User user = CommonService.loginUser(login, password);
		LOG.trace("Found in DB: user --> " + user);
		Role userRole = CommonService.getUserRole(user);
		LOG.trace("userRole --> " + userRole);

		// setting session attributes
		session.setAttribute("user", user);
		LOG.trace("Set the session attribute: user --> " + user);
		session.setAttribute("userRole", userRole);
		LOG.trace("Set the session attribute: userRole --> " + userRole);
		LOG.info("User " + user + " logged as " + userRole.toString().toLowerCase());

		StringBuilder sb = new StringBuilder("info_msg.log_in_").append(userRole.toString().toLowerCase());
		session.setAttribute("message", sb.toString());
		LOG.trace("Set the session attribute: message --> " + sb.toString());

		LOG.debug("Command finished");
		return new CommandHelper(true, getTargetPath());
	}
}
