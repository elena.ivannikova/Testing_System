package ua.nure.blyzniuk.SummaryTask4.web.command.outofcontrol;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

public class RegisterCommand extends Command {

	private static final long serialVersionUID = 1519874823799599203L;
	private static final Logger LOG = Logger.getLogger(RegisterCommand.class);

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(true, Path.COMMAND_SHOW_REGISTRATION_PAGE);
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");
		
		HttpSession session = request.getSession();
		
		Object userRole = session.getAttribute("userRole");

		// obtain parameters from a request
		String firstName = request.getParameter("first_name");
		LOG.trace("Request parameter: first name --> " + firstName);
		String lastName = request.getParameter("last_name");
		LOG.trace("Request parameter: last name --> " + lastName);
		String email = request.getParameter("email");
		LOG.trace("Request parameter: email --> " + email);
		String login = request.getParameter("login");
		LOG.trace("Request parameter: login --> " + login);
		String password = request.getParameter("password");
		String repeatPassword = request.getParameter("repeat_password");
		String role = request.getParameter("role");

		// create new user
		User user = CommonService.registerUser(firstName, lastName, email, login, password, repeatPassword, role, userRole);
		LOG.trace("Created user: --> " + user);

		String message = "info_msg.registr_success";
		session.setAttribute("message", message);
		LOG.trace("Set the session attribute: message --> " + message);

		LOG.debug("Command finished");
		return new CommandHelper(true, Path.PAGE_MAIN);
	}
}
