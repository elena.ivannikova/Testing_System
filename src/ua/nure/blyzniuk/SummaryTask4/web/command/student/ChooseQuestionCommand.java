package ua.nure.blyzniuk.SummaryTask4.web.command.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.QuestionAnswerBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.StudentService;

public class ChooseQuestionCommand extends Command {

	private static final long serialVersionUID = -310746060458811112L;

	private static final Logger LOG = Logger.getLogger(ChooseQuestionCommand.class);

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(getTargetPath());
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Command starts");

		HttpSession session = request.getSession();
		
		TestQuestionsBean testing = (TestQuestionsBean) session.getAttribute("testing");
		LOG.trace("Session parameter: testing --> " + testing);
		
		if (testing == null) {
			throw new AppException("Current test does not exist");
		}

		// obtain parameters from a request
		String questionOrdinal = request.getParameter("questionOrdinal");
		LOG.trace("Request parameter: question ordinal --> " + questionOrdinal);

		if (questionOrdinal == null) {
			questionOrdinal = "1";
		}

		QuestionAnswerBean questionAnswers = StudentService.getQuestionAnswersByOrdinal(testing, questionOrdinal);

		request.setAttribute("questionAnswers", questionAnswers);
		LOG.trace("Set the request attribute: question answers --> " + questionAnswers);

		LOG.debug("Command finished");
		// return forward;
		return new CommandHelper(Path.PAGE_QUESTION);
	}

}
