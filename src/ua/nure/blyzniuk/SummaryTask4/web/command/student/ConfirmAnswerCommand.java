package ua.nure.blyzniuk.SummaryTask4.web.command.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.StudentService;

public class ConfirmAnswerCommand extends Command {

	private static final long serialVersionUID = 354805486492304417L;

	private static final Logger LOG = Logger.getLogger(ConfirmAnswerCommand.class);

	private int questionOrdinal;

	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(Path.COMMAND_PERFORM_TEST + (questionOrdinal - 1));
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");

		HttpSession session = request.getSession();

		if (session.getAttribute("testing") == null) {
			session.setAttribute(SessionAttributes.SESS_ATTR_ERROR_MESSAGE, "error_msg.invalid_operation");
			return new CommandHelper(getTargetPath());
		}

		// obtain parameters from a request
		String ordinal = request.getParameter("questionOrdinal");
		LOG.trace("Request parameter: question ordinal --> " + ordinal);

		try {
			questionOrdinal = Integer.parseInt(ordinal);
		} catch (NumberFormatException | NullPointerException e) {
			throw new AppException("Error while answering.");
		}

		String[] answers = request.getParameterValues("answers");
		LOG.trace("Request parameter: answers --> " + answers);

		TestQuestionsBean testing = (TestQuestionsBean) session.getAttribute("testing");
		LOG.trace("Session parameter: testing --> " + testing);

		StudentService.setQuestionAnswers(testing, questionOrdinal - 1, answers);

		if (questionOrdinal > testing.getQuestions().size()) {
			questionOrdinal = 1;
		}

		session.setAttribute("testing", testing);
		LOG.trace("Set the session attribute: testing --> " + testing);

		LOG.debug("Command finished");

		return new CommandHelper(Path.COMMAND_PERFORM_TEST + questionOrdinal);
	}
}
