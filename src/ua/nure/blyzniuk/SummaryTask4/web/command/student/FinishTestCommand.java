package ua.nure.blyzniuk.SummaryTask4.web.command.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.StudentService;

public class FinishTestCommand extends Command {

	private static final long serialVersionUID = -8159534020542375753L;
	private static final Logger LOG = Logger.getLogger(FinishTestCommand.class);
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(true, getTargetPath());
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		
		LOG.debug("Command starts");

		HttpSession session = request.getSession();

		// obtain parameters from a session
		Object testing = session.getAttribute("testing");
		LOG.trace("Session parameter: testing --> " + testing);
		
		if (testing == null) {
			throw new AppException("error_msg.invalid_operation");
		}

		StudentService.finishTest(testing);

		LOG.debug("Command finished");
		return new CommandHelper(true, Path.COMMAND_SHOW_TEST_RESULT);
	}
}
