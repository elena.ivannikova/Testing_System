package ua.nure.blyzniuk.SummaryTask4.web.command.student;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserObligatoryTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.StudentService;

public class ShowObligatoryTestsPageCommand extends Command {
	private static final long serialVersionUID = -842742010071302506L;
	private static final Logger LOG = Logger.getLogger(ShowObligatoryTestsPageCommand.class);

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Commands starts");

		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
		LOG.trace("Obtain attribute from session: user --> " + user);

		List<UserObligatoryTestsBean> studentTestsList = StudentService.getObligatoryTestsByStudentId(user.getId());
		LOG.trace("Found in DB: studentTestsList --> " + studentTestsList);

		// put user tests beans list to request
		request.setAttribute("studentTestsList", studentTestsList);
		LOG.trace("Set the request attribute: studentTestsList --> " + studentTestsList);

		LOG.debug("Commands finished");
		return new CommandHelper(Path.PAGE_OBLIGATORY_TESTS);
	}

}
