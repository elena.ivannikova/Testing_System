package ua.nure.blyzniuk.SummaryTask4.web.command.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.web.SessionAttributes;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.StudentService;

public class ShowTestResultCommand extends Command {
	
	private static final long serialVersionUID = 202992363305952796L;
	private static final Logger LOG = Logger.getLogger(ShowTestResultCommand.class);

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {

		LOG.debug("Command starts");

		HttpSession session = request.getSession();

		// obtain parameters from a session
		Object testing = session.getAttribute("testing");
		LOG.trace("Session parameter: testing --> " + testing);

		if (testing == null) {
			throw new AppException("error_msg.invalid_operation");
		}

		double testResult = StudentService.getTestResultByUserTestId(testing);
		request.setAttribute("testResult", testResult);
		LOG.trace("Set the request attribute: testResult --> " + testResult);
		
		session.setAttribute("timer1", null);
		session.setAttribute("testing", null);
		LOG.trace("Set the session attribute: testing --> " + null);
		
		session.setAttribute(SessionAttributes.SESS_ATTR_MESSAGE, "info_msg.test_finished");

		LOG.debug("Command finished");
		return new CommandHelper(Path.PAGE_TEST_RESULT);
	}
}
