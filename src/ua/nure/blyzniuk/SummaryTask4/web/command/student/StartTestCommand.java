package ua.nure.blyzniuk.SummaryTask4.web.command.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserTest;
import ua.nure.blyzniuk.SummaryTask4.web.command.Command;
import ua.nure.blyzniuk.SummaryTask4.web.command.CommandHelper;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;
import ua.nure.blyzniuk.SummaryTask4.web.logic.StudentService;

public class StartTestCommand extends Command {

	private static final long serialVersionUID = 7511834764018443096L;
	private static final Logger LOG = Logger.getLogger(StartTestCommand.class);
	
	@Override
	public CommandHelper getTargetOperation() {
		return new CommandHelper(true, getTargetPath());
	}

	@Override
	public CommandHelper execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException, AppException {
		LOG.debug("Command starts");

		HttpSession session = request.getSession();

		// obtain parameters from a request
		String testId = request.getParameter("testId");
		LOG.trace("Request parameter: test id --> " + testId);

		// obtain parameters from a session
		User user = (User) session.getAttribute("user");
		LOG.trace("Session parameter: user --> " + user);
		Object userRole = session.getAttribute("userRole");

		TestQuestionsBean testing = CommonService.getTestQuestionsByTestId(testId, userRole);
		LOG.trace("Found in DB: testing --> " + testing);

		UserTest studentTest = StudentService.getStudentTest(user, testId);

		testing.setUserTestId(studentTest.getId());
		session.setAttribute("testing", testing);
		LOG.trace("Set the request attribute: testing --> " + testing);

		String timer = StudentService.getTimer(testing);
		session.setAttribute("timer1", timer);

		LOG.debug("Command finished");
		return new CommandHelper(true, Path.COMMAND_PERFORM_TEST + "1");
	}
}
