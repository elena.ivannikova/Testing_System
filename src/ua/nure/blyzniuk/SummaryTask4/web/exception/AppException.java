package ua.nure.blyzniuk.SummaryTask4.web.exception;

/**
 * An exception that provides information on an application error.
 * 
 * @author Alena
 */
public class AppException extends Exception {
	private static final long serialVersionUID = 5641595046278189229L;

	public AppException() {
		super();
	}

	public AppException(String message, Throwable cause) {
		super(message, cause);
	}

	public AppException(String message) {
		super(message);
	}
	
	public AppException(Throwable cause) {
        super(cause);
    }

    public AppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
