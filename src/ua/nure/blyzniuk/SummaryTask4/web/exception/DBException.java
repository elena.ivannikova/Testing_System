package ua.nure.blyzniuk.SummaryTask4.web.exception;

/**
 * An exception that provides information on a database access error.
 * 
 * @author Elena Blyzniuk
 */
public class DBException extends AppException {

	private static final long serialVersionUID = 8288779062647218916L;

	public DBException() {
    }

    public DBException(String message) {
        super(message);
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBException(Throwable cause) {
        super(cause);
    }

    public DBException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
