package ua.nure.blyzniuk.SummaryTask4.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.logic.CommonService;

/**
 * Servlet Filter implementation class CommandAccessFilter
 */
@WebFilter(filterName = "CommandAccessFilter")
public class CommandAccessFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(CommandAccessFilter.class);

	// commands access
	private Map<Role, List<String>> accessMap = new HashMap<Role, List<String>>();
	private List<String> commons = new ArrayList<String>();
	private List<String> outOfControl = new ArrayList<String>();

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		LOG.debug("Filter destruction starts");
		// do nothing
		LOG.debug("Filter destruction finished");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		LOG.debug("Filter starts");
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		
		if (session == null) {
			request.getRequestDispatcher(Path.PAGE_MAIN).forward(request, response);
		} else {
			// obtain current locale from session
			if (session.getAttribute("currentLocale") == null) {
				session.setAttribute("currentLocale", "ru_UA");
			}
			String currentLocale = session.getAttribute("currentLocale").toString();

			// obtain language and country from current locale
			try {
				Language language = CommonService.getCurrentLanguage(currentLocale);
				Country country = CommonService.getCurrentCountry(currentLocale);
				session.setAttribute("language", language);
				LOG.trace("Set the session attribute: language --> " + language);
				session.setAttribute("country", country);
				LOG.trace("Set the session attribute: country --> " + country);
			} catch (AppException e) {
				session.setAttribute("errorMessage", "Unknown error.");
			}
			
			if (accessAllowed(request)) {
				LOG.debug("Filter finished");
				chain.doFilter(request, response);
			} else {
				String errorMessage = "error_msg.no_permission";
				session.setAttribute("errorMessage", errorMessage);
				LOG.trace("Set the session attribute: errorMessage --> " + errorMessage);
				request.getRequestDispatcher(Path.PAGE_MAIN).forward(request, response);
			}
		}
	}

	private boolean accessAllowed(ServletRequest request) {

		String commandName = request.getParameter("command");
		if (commandName == null || commandName.isEmpty()) {
			return false;
		}
		if (outOfControl.contains(commandName)) {
			return true;
		}
		HttpSession session = ((HttpServletRequest) request).getSession(false);
		if (session == null) {
			return false;
		}
		Role userRole = (Role) session.getAttribute("userRole");
		if (userRole == null) {
			return false;
		}
		return accessMap.get(userRole).contains(commandName) || commons.contains(commandName);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {

		System.out.println(fConfig.getFilterName());
		System.out.println(Collections.list(fConfig.getInitParameterNames()));

		LOG.debug("Filter initialization starts");

		// roles
		accessMap.put(Role.ADMIN, asList(fConfig.getInitParameter("admin")));
		accessMap.put(Role.EDUCATOR, asList(fConfig.getInitParameter("educator")));
		accessMap.put(Role.STUDENT, asList(fConfig.getInitParameter("student")));
		LOG.trace("Access map --> " + accessMap);

		// commons
		commons = asList(fConfig.getInitParameter("common"));
		LOG.trace("Common commands --> " + commons);

		// out of control
		outOfControl = asList(fConfig.getInitParameter("out-of-control"));
		LOG.trace("Out of control commands --> " + outOfControl);

		LOG.debug("Filter initialization finished");
	}

	/**
	 * Extracts parameter values from string.
	 * 
	 * @param str
	 *            parameter values string.
	 * @return list of parameter values.
	 */
	private List<String> asList(String str) {
		List<String> list = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens()) {
			list.add(st.nextToken());
		}
		return list;
	}
}
