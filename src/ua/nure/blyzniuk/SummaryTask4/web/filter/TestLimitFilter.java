package ua.nure.blyzniuk.SummaryTask4.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.blyzniuk.SummaryTask4.Path;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Servlet Filter implementation class TestLimitFilter
 */
@WebFilter(filterName = "TestLimitFilter")
public class TestLimitFilter implements Filter {

	private static final Logger LOG = Logger.getLogger(TestLimitFilter.class);

	/**
	 * Default constructor.
	 */
	public TestLimitFilter() {

	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		LOG.debug("TestLimitFilter starts");

		HttpSession session = ((HttpServletRequest) request).getSession(false);
		
		String str = request.getParameter("command");
		
		if (session.getAttribute("userRole") != null && session.getAttribute("user") != null && str.equals("startTest") && request.getParameter("freeAccess").equals("true")) {
			
			String userRole = session.getAttribute("userRole").toString();
			User user = (User) session.getAttribute("user");
			Boolean testLimitReached = null;

			try {
				testLimitReached = DAOFactory.getUserTestDAO().isTestLimitReached(user.getId());
			} catch (DBException e) {
				request.setAttribute("errorMessage", e.getLocalizedMessage());
				LOG.trace("Set the request attribute: errorMessage --> " + e.getLocalizedMessage());
			}

			if (!testLimitReached && userRole.toLowerCase().equals("student")) {
				LOG.debug("Filter finished");
				chain.doFilter(request, response);
			} else {
				String message = "error_msg.test_limit_over";
				request.setAttribute("errorMessage", message);
				LOG.trace("Set the request attribute: message --> " + message);
				request.getRequestDispatcher(Path.PAGE_MAIN).forward(request, response);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
