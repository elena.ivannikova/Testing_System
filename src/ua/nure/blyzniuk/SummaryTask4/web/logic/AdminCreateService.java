package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.QuestionAnswerBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Answer;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Question;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;

/**
 * Provides service methods for creation operations for admin role.
 * 
 * @author Elena Blyzniuk
 */
public class AdminCreateService {

	public static void createGroup(String groupName, String facultyId) throws AppException {
		throwAppExceptionIfNullOrEmpty("Group name cannot be empty.", groupName);
		throwAppExceptionIfNullOrEmpty("Cannot create new group.", facultyId);
		long id;
		try {
			id = Long.parseLong(facultyId);
		} catch (NumberFormatException e) {
			throw new AppException("Cannot create new group.");
		}
		Group group = Group.createGroup(groupName, id);
		throwAppExceptionIfNull("Cannot create new institution.", DAOFactory.getGroupDAO().persist(group));
	}

	public static void createInstitution(String institutionName, Country country) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, institutionName);
		University university = new University();
		university.setName(institutionName);
		university.setCountryId(country.getId());
		try {
			throwAppExceptionIfNull(ERR_INSTITUTION_ALREADY_EXISTS, DAOFactory.getUniversityDAO().persist(university));
		} catch (DBException ex) {
			throw new AppException(ERR_INSTITUTION_ALREADY_EXISTS);
		}
	}

	public static void createCategory(String categoryName, Language language) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, categoryName);
		Category category = Category.createCategory(categoryName, language.getId());
		try {
			throwAppExceptionIfNull(ERR_CATEGORY_ALREADY_EXISTS, DAOFactory.getCategoryDAO().persist(category));
		} catch (DBException ex) {
			throw new AppException(ERR_CATEGORY_ALREADY_EXISTS);
		}
	}

	public static void createFaculty(String facultyName, String institution) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, facultyName, institution);
		long institutionId;
		try {
			institutionId = Long.parseLong(institution);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		Faculty faculty = Faculty.createFaculty(facultyName, institutionId);
		try {
			throwAppExceptionIfNull(ERR_FACULTY_ALREADY_EXISTS, DAOFactory.getFacultyDAO().persist(faculty));
		} catch (DBException ex) {
			throw new AppException(ERR_FACULTY_ALREADY_EXISTS);
		}
	}

	public static void createSubject(String subjectName, String category) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, subjectName, category);
		long categoryId;
		try {
			categoryId = Long.parseLong(category);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		Subject subject = Subject.createSubject(subjectName, categoryId);
		try {
			throwAppExceptionIfNull(ERR_SUBJECT_ALREADY_EXISTS, DAOFactory.getSubjectDAO().persist(subject));
		} catch (DBException ex) {
			throw new AppException(ERR_SUBJECT_ALREADY_EXISTS);
		}
	}

	public static void createQuestion(Object testQuestions, String questionOrdinal, String variantsNumber,
			String[] answerText, String[] correctAnswers, String questionText) throws AppException {
		TestQuestionsBean testing = null;
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, questionOrdinal, variantsNumber);
		int qOrdinal = (int) Validation.parseLong("Error while creating question.", questionOrdinal);
		int vNumber = (int) Validation.parseLong("Error while creating question.", variantsNumber);
		try {
			testing = (TestQuestionsBean) testQuestions;
		} catch (ClassCastException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_QUESTION, questionText);
		throwAppExceptionIfNullOrSize0(ERR_EMPTY_ANSWERS, answerText);
		for (int i = 0; i < answerText.length; i++) {
			if(answerText[i].equals("")){
				throw new AppException(ERR_EMPTY_ANSWERS);
			}
		}
		throwAppExceptionIfNullOrSize0(ERR_EMPTY_VARIANTS, correctAnswers);
		List<String> cAnswers = Arrays.asList(correctAnswers);
		QuestionAnswerBean questionAnswers = new QuestionAnswerBean();
		Question question = new Question();
		question.setText(questionText);
		question.setOrdinal(qOrdinal);
		questionAnswers.setQuestion(question);
		List<Answer> answers = new ArrayList<>(vNumber);
		for (int i = 0; i < vNumber; i++) {
			Answer a = new Answer();
			a.setOrdinal(i + 1);
			a.setText(answerText[i]);
			a.setCorrect(cAnswers.contains(String.valueOf(a.getOrdinal())));
			answers.add(a);
		}
		questionAnswers.setAnswers(answers);
		testing.getQuestions().add(questionAnswers);
	}

	public static void createTest(TestQuestionsBean testQuestions) throws AppException {
		DAOFactory.getTestDAO().createTest(testQuestions);
	}
}
