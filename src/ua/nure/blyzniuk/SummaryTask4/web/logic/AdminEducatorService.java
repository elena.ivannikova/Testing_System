package ua.nure.blyzniuk.SummaryTask4.web.logic;

import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupDetailBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Complexity;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.ComplexityDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.*;

/**
 * @author Alena
 *
 */
public class AdminEducatorService {

	/**
	 * @param userRole
	 * @param user
	 * @return
	 * @throws AppException
	 */
	public static List<UserGroupDetailBean> getAllUsersToConfirm(String userRole, User user) throws AppException {
		UserGroupDAO userGroupDAO = DAOFactory.getUserGroupDAO();
		List<UserGroupDetailBean> users = null;
		Role role = Role.getRole(user);
		if (role.equals(Role.ADMIN)) {
			users = userGroupDAO.getAllEducatorsToConfirm();
		} else if (role.equals(Role.EDUCATOR)) {
			users = userGroupDAO.getAllStudentsToConfirm(user.getId());
		}
		throwAppExceptionIfNull("error_msg.no_users_to_confirm", users);
		if (users.isEmpty()) {
			throw new AppException("error_msg.no_users_to_confirm");
		}
		return users;
	}

	/**
	 * @return
	 * @throws AppException
	 */
	public static List<Complexity> getComplexityList() throws AppException {
		ComplexityDAO complexityDAO = new ComplexityDAO();
		List<Complexity> complexityList = complexityDAO.getAll();
		throwAppExceptionIfNull("Complexity list is empty.", complexityList);
		return complexityList;
	}
}
