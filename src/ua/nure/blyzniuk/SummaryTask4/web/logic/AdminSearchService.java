package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.ERR_EMPTY_CATEGORY_CHOICE;
import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.ERR_EMPTY_INSTITUTION_CHOICE;
import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.ERR_INVALID_OPERATION;
import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.ERR_NO_FACULTIES_OF_INSTITUTION;
import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.ERR_NO_SEARCH_RESULTS;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNull;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNullOrEmpty;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNullOrEmptyList;

import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.TestRatingBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.GroupDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl.UserDAOImpl;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

/**
 * Provides service methods for search operations for admin role.
 * 
 * @author Elena Blyzniuk
 */
public class AdminSearchService {

	/**
	 * @param categoryName
	 * @param language
	 * @return
	 * @throws AppException
	 */
	public static List<Category> searchCategoriesByNameAndLanguage(String categoryName, Language language)
			throws AppException {
		List<Category> categories = DAOFactory.getCategoryDAO().getAllByNameAndLanguageName(categoryName, language.getName());
		throwAppExceptionIfNullOrEmptyList(ERR_NO_SEARCH_RESULTS, categories);
		return categories;
	}

	/**
	 * @param name
	 * @param country
	 * @return
	 * @throws AppException
	 */
	public static List<University> searchInstitutionsByNameAndCountry(String name, Country country)
			throws AppException {
		List<University> institutions = DAOFactory.getUniversityDAO().getByNameAndCountryName(name, country.getName());
		throwAppExceptionIfNullOrEmptyList(ERR_NO_SEARCH_RESULTS, institutions);
		return institutions;
	}

	/**
	 * @param institutionId
	 * @param name
	 * @return
	 * @throws AppException
	 */
	public static List<Faculty> searchFacultiesByNameAndInstitutionId(String institutionId, String name)
			throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_INSTITUTION_CHOICE, institutionId);
		long currentId;
		try {
			currentId = Long.parseLong(institutionId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_NO_FACULTIES_OF_INSTITUTION);
		}
		List<Faculty> faculties = DAOFactory.getFacultyDAO().getAllByNameAndInstitutionId(name, currentId);
		throwAppExceptionIfNullOrEmptyList(ERR_NO_SEARCH_RESULTS, faculties);
		return faculties;
	}

	/**
	 * @param categoryId
	 * @param name
	 * @return
	 * @throws AppException
	 */
	public static List<Subject> searchSubjectsByNameAndCategoryId(String categoryId, String name) throws AppException {
		throwAppExceptionIfNull(ERR_EMPTY_CATEGORY_CHOICE, categoryId);
		long currentId;
		try {
			currentId = Long.parseLong(categoryId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		List<Subject> subjects = DAOFactory.getSubjectDAO().getAllByNameAndCategoryId(name, currentId);
		throwAppExceptionIfNullOrEmptyList(ERR_NO_SEARCH_RESULTS, subjects);
		return subjects;
	}

	/**
	 * @param name
	 * @param locked
	 * @param institutionId
	 * @param facultyId
	 * @param groupId
	 * @return
	 * @throws AppException
	 */
	public static List<UserInfoBean> searchUsersByConditions(String name, String locked, String institutionId,
			String facultyId, String groupId) throws AppException {
		List<UserInfoBean> users = null;
		long id = 0;
		int lock = locked == null ? 0 : 1;
		UserDAO userDAO = new UserDAOImpl();
		if (!Validation.isNullOrEmpty(groupId)) {
			id = Validation.parseLong(ERR_INVALID_OPERATION, groupId);
			users = userDAO.searchUsersByConditions(name, lock, null, null, id);
		} else {
			if (!Validation.isNullOrEmpty(facultyId)) {
				id = Validation.parseLong(ERR_INVALID_OPERATION, facultyId);
				users = userDAO.searchUsersByConditions(name, lock, null, id, null);
			} else {
				if (!Validation.isNullOrEmpty(institutionId)) {
					id = Validation.parseLong(ERR_INVALID_OPERATION, institutionId);
					users = userDAO.searchUsersByConditions(name, lock, id, null, null);
				} else {
					users = userDAO.searchUsersByConditions(name, lock, null, null, null);
				}
			}
		}
		if (users == null) {
			throw new AppException(ERR_NO_SEARCH_RESULTS);
		}
		return users;
	}

	/**
	 * @param name
	 * @param institutionId
	 * @param facultyId
	 * @return
	 * @throws AppException
	 */
	public static List<Group> searchGroupsByConditions(String name, String institutionId, String facultyId)
			throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_INSTITUTION_CHOICE, institutionId);
		List<Group> groups = null;
		long id = 0;
		GroupDAO groupDAO = DAOFactory.getGroupDAO();
		if (!Validation.isNullOrEmpty(facultyId)) {
			id = Validation.parseLong(ERR_INVALID_OPERATION, facultyId);
			groups = groupDAO.searchGroupsByConditions(name, null, id);
		} else {
			if (!Validation.isNullOrEmpty(institutionId)) {
				id = Validation.parseLong(ERR_INVALID_OPERATION, institutionId);
				groups = groupDAO.searchGroupsByConditions(name, id, null);
			} else {
				groups = groupDAO.searchGroupsByConditions(name, null, null);
			}
		}
		if (groups == null) {
			throw new AppException(ERR_NO_SEARCH_RESULTS);
		}
		return groups;
	}

	/**
	 * @param subjectId
	 * @return
	 * @throws AppException
	 */
	public static Subject getSubjectById(String subjectId) throws AppException {
		long id = Validation.parseLong(ERR_INVALID_OPERATION, subjectId);
		Subject subject = DAOFactory.getSubjectDAO().getByPK(id);
		Validation.throwAppExceptionIfNull("Cannot obtain subject by id.", subject);
		return subject;
	}

	/**
	 * @param categoryId
	 * @return
	 * @throws AppException
	 */
	public static Category getCategoryById(String categoryId) throws AppException {
		long id = Validation.parseLong(ERR_INVALID_OPERATION, categoryId);
		Category category = DAOFactory.getCategoryDAO().getByPK(id);
		Validation.throwAppExceptionIfNull("Cannot obtain category by id.", category);
		return category;
	}

	/**
	 * @param facultyId
	 * @return
	 * @throws AppException
	 */
	public static Faculty getFacultyById(String facultyId) throws AppException {
		long id = Validation.parseLong(ERR_INVALID_OPERATION, facultyId);
		Faculty faculty = DAOFactory.getFacultyDAO().getByPK(id);
		Validation.throwAppExceptionIfNull("Cannot obtain faculty by id.", faculty);
		return faculty;
	}

	/**
	 * @param groupId
	 * @return
	 * @throws AppException
	 */
	public static Group getGroupById(String groupId) throws AppException {
		long id = Validation.parseLong(ERR_INVALID_OPERATION, groupId);
		Group group = DAOFactory.getGroupDAO().getByPK(id);
		Validation.throwAppExceptionIfNull("Cannot obtain group by id.", group);
		return group;
	}

	/**
	 * @param institutionId
	 * @return
	 * @throws AppException
	 */
	public static University getInstitutionById(String institutionId) throws AppException {
		long id = Validation.parseLong(ERR_INVALID_OPERATION, institutionId);
		University institution = DAOFactory.getUniversityDAO().getByPK(id);
		Validation.throwAppExceptionIfNull("Cannot obtain institution by id.", institution);
		return institution;
	}
	
	/**
	 * @param testId
	 * @return
	 * @throws AppException
	 */
	public static List<TestRatingBean> getTestRatingsByTestId(String testId) throws AppException{
		long id = Validation.parseLong(ERR_INVALID_OPERATION, testId);
		List<TestRatingBean> ratings = DAOFactory.getUserTestDAO().getTestRatingsByTestId(id);
		if(ratings == null || ratings.size() == 0){
			throw new AppException("error_msg.ratings_not_found");
		}
		return ratings;
	}
}
