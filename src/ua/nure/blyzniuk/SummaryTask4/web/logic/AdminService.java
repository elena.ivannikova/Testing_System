package ua.nure.blyzniuk.SummaryTask4.web.logic;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.QuestionAnswerBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Test;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserGroupDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl.UserDAOImpl;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.*;

import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;

/**
 * Provides service methods for creation operations for admin role.
 * 
 * @author Elena Blyzniuk
 */
public class AdminService {

	/**
	 * Confirms or rejects users' requests.
	 * 
	 * @param userGroupBeans
	 * @param reject
	 * @throws AppException
	 */
	public static void confirmUserGroups(String[] userGroupBeans, boolean reject) throws AppException {
		if (userGroupBeans == null) {
			throw new AppException("error_msg.empty_choise");
		}
		List<UserGroup> userGroups = new ArrayList<>();
		for (int i = 0; i < userGroupBeans.length; i++) {
			String[] identity = userGroupBeans[i].split(" ");
			UserGroup userGroup = new UserGroup();
			userGroup.setUserId(Long.parseLong(identity[0]));
			userGroup.setGroupId(Long.parseLong(identity[1]));
			userGroups.add(userGroup);
		}
		UserGroupDAO userGroupDAO = DAOFactory.getUserGroupDAO();
		if (reject) {
			userGroupDAO.rejectUserGroups(userGroups);
		} else {
			userGroupDAO.confirmUserGroups(userGroups);
		}
	}

	/**
	 * Deletes tests by specified id.
	 * 
	 * @param tests
	 * @throws AppException
	 */
	public static void deleteTestsById(String[] tests) throws AppException {
		if (tests == null || tests.length == 0) {
			throw new AppException(ERR_DELETE_EMPTY_TEST_CHOICE);
		}
		long[] testsId = new long[tests.length];
		for (int i = 0; i < tests.length; i++) {
			testsId[i] = parseLong("Cannot obtain tests' list.", tests[i]);
		}
		if (!DAOFactory.getTestDAO().deleteTestsById(testsId)) {
			throw new AppException("Cannot delete specified tests.");
		}
	}

	/**
	 * Locks or unlocks users.
	 * 
	 * @param lock
	 * @param users
	 * @throws AppException
	 */
	public static void setUsersLock(boolean lock, String... users) throws AppException {
		if (users == null || users.length == 0) {
			throw new AppException(ERR_EMPTY_USER_CHOICE);
		}
		long[] usersId = new long[users.length];
		try {
			for (int i = 0; i < users.length; i++) {
				usersId[i] = Long.parseLong(users[i]);
			}
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		UserDAO userDAO = new UserDAOImpl();
		if (!userDAO.setLock(usersId, lock)) {
			throw new AppException("Cannot lock specified users");
		}
	}

	/**
	 * Initializes con
	 * 
	 * @param testName
	 * @param subject
	 * @param complexity
	 * @param duration
	 * @param questionNumber
	 * @param freeAccess
	 * @return
	 * @throws AppException
	 */
	public static TestQuestionsBean initTest(String testName, String subject, String complexity, String duration,
			String questionNumber, String freeAccess) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, testName, subject, complexity, duration, questionNumber);
		if (!duration.matches("^[0-1][0-2]:(([1-5][0-9])|(0[1-9]))$")) {
			throw new AppException(ERR_ILLEGAL_TIME_FORMAT);
		} else if (!questionNumber.matches("[1-9][0-9]*")) {
			throw new AppException(ERR_ILLEGAL_QUESTION_NUMBER);
		}
		long subjectId = parseLong("Cannot create test.", subject);
		int complexityId = (int) parseLong("Cannot create test.", complexity);
		Time time = null;
		try {
			time = Time.valueOf(duration + ":00");
		} catch (IllegalArgumentException e) {
			throw new AppException("Cannot create test.");
		}
		int qNumber = (int) parseLong("Cannot create test.", questionNumber);
		TestQuestionsBean testQuestions = new TestQuestionsBean();
		Test test = new Test();
		test.setName(testName);
		test.setSubjectId(subjectId);
		test.setComplexityId(complexityId);
		test.setDuration(time);
		test.setFreeAccess(freeAccess != null);
		testQuestions.setTest(test);
		testQuestions.setQuestionNumber(qNumber);
		List<QuestionAnswerBean> questionAnswers = new ArrayList<>(qNumber);
		testQuestions.setQuestions(questionAnswers);
		return testQuestions;
	}
}
