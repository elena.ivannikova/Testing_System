package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNullOrEmpty;

import java.sql.Time;

import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Test;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

/**
 * Provides service methods for update operations for admin role.
 * 
 * @author Elena Blyzniuk
 */
/**
 * @author Alena
 *
 */
public class AdminUpdateService {

	/**
	 * Updates category.
	 * 
	 * @param categoryName
	 * @param categoryId
	 * @throws AppException
	 */
	public static void updateCategory(String categoryName, String categoryId) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, categoryName, categoryId);
		long id;
		try {
			id = Long.parseLong(categoryId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		Category category = Category.createCategory(id, categoryName.toString());
		try {
			if (!DAOFactory.getCategoryDAO().update(category)) {
				throw new AppException(ERR_CATEGORY_ALREADY_EXISTS);
			}
		} catch (DBException ex) {
			throw new AppException(ERR_CATEGORY_ALREADY_EXISTS);
		}
	}

	/**
	 * Updates faculty.
	 * 
	 * @param facultyName
	 * @param facultyId
	 * @throws AppException
	 */
	public static void updateFaculty(String facultyName, String facultyId) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, facultyName, facultyId);
		long id;
		try {
			id = Long.parseLong(facultyId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		Faculty faculty = Faculty.createFaculty(id, facultyName);
		try {
			if (!DAOFactory.getFacultyDAO().update(faculty)) {
				throw new AppException(ERR_FACULTY_ALREADY_EXISTS);
			}
		} catch (DBException ex) {
			throw new AppException(ERR_FACULTY_ALREADY_EXISTS);
		}
	}

	/**
	 * Updates educational institution.
	 * 
	 * @param institutionName
	 * @param institutionId
	 * @throws AppException
	 */
	public static void updateInstitution(String institutionName, String institutionId) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, institutionName, institutionId);
		long id;
		try {
			id = Long.parseLong(institutionId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		University university = University.createUniversity(id, institutionName);
		try {
			if (!DAOFactory.getUniversityDAO().update(university)) {
				throw new AppException(ERR_INSTITUTION_ALREADY_EXISTS);
			}
		} catch (DBException ex) {
			throw new AppException(ERR_INSTITUTION_ALREADY_EXISTS);
		}
	}

	/**
	 * Updates subject.
	 * 
	 * @param name
	 * @param id
	 * @throws AppException
	 */
	public static void updateSubject(String name, String id) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, name, id);
		long currentId;
		try {
			currentId = Long.parseLong(id);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		Subject subject = Subject.createSubject(currentId, name);
		try {
			if (!DAOFactory.getSubjectDAO().update(subject)) {
				throw new AppException(ERR_SUBJECT_ALREADY_EXISTS);
			}
		} catch (DBException ex) {
			throw new AppException(ERR_SUBJECT_ALREADY_EXISTS);
		}
	}

	/**
	 * Updates group.
	 * 
	 * @param name
	 * @param id
	 * @throws AppException
	 */
	public static void updateGroup(String name, String id) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, name, id);
		long currentId;
		try {
			currentId = Long.parseLong(id);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		Group group = Group.createGroup(currentId, name);
		try {
			if (!DAOFactory.getGroupDAO().update(group)) {
				throw new AppException(ERR_GROUP_ALREADY_EXISTS);
			}
		} catch (DBException ex) {
			throw new AppException(ERR_GROUP_ALREADY_EXISTS);
		}
	}

	/**
	 * Updates test.
	 * 
	 * @param testId
	 * @param testName
	 * @param complexityId
	 * @param duration
	 * @param freeAccess
	 * @throws AppException
	 */
	public static void updateTest(String testId, String testName, String complexityId, String duration,
			String freeAccess) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, testId, testName, complexityId, duration);
		long tId = Validation.parseLong(ERR_INVALID_OPERATION, testId);
		int cId = (int) Validation.parseLong(ERR_INVALID_OPERATION, complexityId);
		if (!duration.matches("^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):00$")) {
			throw new AppException(ERR_ILLEGAL_TIME_FORMAT);
		}
		Time dur = Time.valueOf(duration);
		boolean access = freeAccess != null;
		Test test = new Test();
		test.setId(tId);
		test.setName(testName);
		test.setComplexityId(cId);
		test.setDuration(dur);
		test.setFreeAccess(access);
		if (!DAOFactory.getTestDAO().update(test)) {
			throw new AppException("Error while updating test");
		}
	}

}
