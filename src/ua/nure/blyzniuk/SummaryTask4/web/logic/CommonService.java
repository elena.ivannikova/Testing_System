package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.isNull;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.isNullOrEmpty;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNull;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNullOrEmpty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.bean.QuestionAnswerBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestInfoBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserPassedTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.comparator.TestComparator;
import ua.nure.blyzniuk.SummaryTask4.db.bean.comparator.TestComparatorContainer;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Answer;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Category;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Country;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Language;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Question;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Role;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Subject;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.AnswerDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.QuestionDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.TestDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl.UserDAOImpl;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

public class CommonService {

	private static final String DEFAULT_COUNTRY = "US";
	private static final String DELIMITER = "_";

	/**
	 * Obtains user by login.
	 * 
	 * @param login
	 * @param password
	 * @return
	 * @throws AppException
	 */
	public static User loginUser(String login, String password) throws AppException {
		if (isNullOrEmpty(login) || isNullOrEmpty(password)) {
			throw new AppException(ERR_EMPTY_FIELDS);
		}
		User user = null;
		try {
			user = new UserDAOImpl().findUserByLoginPassword(login, password);
		} catch (DBException e) {
			throw new AppException(e);
		}
		if (isNull(user)) {
			throw new AppException(ERR_CANNOT_FIND_USER_BY_LOGIN_PASSWORD);
		} else if (user.isLocked()) {
			throw new AppException(ERR_USER_LOCKED);
		}
		return user;
	}

	/**
	 * Obtains current language.
	 * 
	 * @param currentLocale
	 * @return
	 * @throws AppException
	 */
	public static Language getCurrentLanguage(String currentLocale) throws AppException {
		Language language = DAOFactory.getLanguageDAO()
				.getLanguageByName(currentLocale.split(DELIMITER)[0].toLowerCase());
		if (isNull(language)) {
			throw new AppException(ERR_CANNOT_OBTAIN_CURRENT_LANGUAGE);
		}
		return language;
	}

	/**
	 * Obtains current country.
	 * 
	 * @param currentLocale
	 * @return
	 * @throws AppException
	 */
	public static Country getCurrentCountry(String currentLocale) throws AppException {
		Country country = null;
		String countryStr = DEFAULT_COUNTRY;
		if (currentLocale.split(DELIMITER).length > 1) {
			countryStr = currentLocale.split(DELIMITER)[1].toUpperCase();
		}
		country = DAOFactory.getCountryDAO().getCountryByName(countryStr);
		if (isNull(country)) {
			throw new AppException(ERR_CANNOT_OBTAIN_CURRENT_COUNTRY);
		}
		return country;
	}

	/**
	 * Obtains user's role.
	 * 
	 * @param user
	 * @return
	 * @throws AppException
	 */
	public static Role getUserRole(User user) throws AppException {
		Role userRole = Role.getRole(user);
		throwAppExceptionIfNull(ERR_ILLEGAL_USER_ROLE, userRole);
		return userRole;
	}

	/**
	 * Registers user.
	 * 
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param login
	 * @param password
	 * @param repeatPassword
	 * @param role
	 * @param userRole
	 * @return
	 * @throws AppException
	 */
	public static User registerUser(String firstName, String lastName, String email, String login, String password,
			String repeatPassword, String role, Object userRole) throws AppException {
		if (isNullOrEmpty(firstName, lastName, email, login, password, repeatPassword)) {
			throw new AppException(ERR_EMPTY_FIELDS);
		} else if (!password.equals(repeatPassword)) {
			throw new AppException(ERR_PASSWORD_NOT_EQUAL);
		}else if(!email.matches("[\\w-]+(?:\\.[\\w-]+)*@(?:[a-z0-9-]+\\.)+[a-z]{2,6}$")){
			throw new AppException(ERR_ILLEGAL_EMAIL_FORMAT);
		}else if(!login.matches("[a-zA-Z]\\w{2,19}")){
			throw new AppException(ERR_ILLEGAL_LOGIN_FORMAT);
		}
		int roleId = 3;
		if (userRole != null && userRole.toString().toLowerCase().equals("admin")) {
			roleId = (int) Validation.parseLong(ERR_INVALID_OPERATION, role);
		}
		User user = User.createUser(firstName, lastName, email, roleId, login, password);
		try {
			user = new UserDAOImpl().persist(user);
		} catch (DBException e) {
			String error = e.getMessage();
			if(e.getMessage().equals("'email'")){
				error = ERR_INVALID_EMAIL;
			}else if(e.getMessage().equals("'login'")){
				error = ERR_INVALID_LOGIN;
			}
			throw new AppException(error);
		}
		throwAppExceptionIfNull(ERR_CANNOT_REGISTER_USER, user);
		return user;
	}

	/**
	 * Obtains institutions by user id.
	 * 
	 * @param userId
	 * @return
	 * @throws AppException
	 */
	public static List<UserGroupBean> getInstitutionsByUserId(long userId) throws AppException {
		UserDAO userDAO = new UserDAOImpl();
		List<UserGroupBean> list = null;
		try {
			list = userDAO.getInstitutionsByUserId(userId);
		} catch (DBException e) {
			throw new AppException("Cannot obtain institutions of current user.", e);
		}
		return list;
	}

	/**
	 * Obtains passed tests by student id.
	 * 
	 * @param userId
	 * @return
	 * @throws AppException
	 */
	public static List<UserPassedTestsBean> getPassedTestsByStudentId(long userId) throws AppException {
		List<UserPassedTestsBean> list;
		try {
			list = DAOFactory.getUserTestDAO().getPassedTestsByStudentId(userId);
		} catch (DBException e) {
			throw new AppException("Cannot obtain tests passed by current user.", e);
		}
		throwAppExceptionIfNull(MSG_NO_PASSES_TESTS, list);
		return list;
	}

	/**
	 * Updates user's personal information.
	 * 
	 * @param id
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param login
	 * @return
	 * @throws AppException
	 */
	public static User updateUser(long id, String firstName, String lastName, String email, String login)
			throws AppException {
		if (isNullOrEmpty(firstName, lastName, email, login)) {
			throw new AppException(ERR_EMPTY_FIELDS);
		} else if(!email.matches("[\\w-]+(?:\\.[\\w-]+)*@(?:[a-z0-9-]+\\.)+[a-z]{2,6}$")){
			throw new AppException(ERR_ILLEGAL_EMAIL_FORMAT);
		}else if(!login.matches("[a-zA-Z]\\w{2,19}")){
			throw new AppException(ERR_ILLEGAL_LOGIN_FORMAT);
		}
		User user = User.createUser(id, firstName, lastName, email, login);
		try {
			if (!DAOFactory.getUserDAO().update(user)) {
				throw new AppException("Cannot update current user's personal information.");
			}
		} catch (DBException e) {
			String error = e.getMessage();
			if(e.getMessage().equals("'email'")){
				error = ERR_INVALID_EMAIL;
			}else if(e.getMessage().equals("'login'")){
				error = ERR_INVALID_LOGIN;
			}
			throw new AppException(error);
		}
		return user;
	}

	/**
	 * Changes user's password
	 * 
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @throws AppException
	 */
	public static void changePassword(long userId, String oldPassword, String[] newPassword) throws AppException {
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_FIELDS, oldPassword, newPassword[0], newPassword[1]);
		UserDAO userDAO = new UserDAOImpl();
		if (!newPassword[0].equals(newPassword[1])) {
			throw new AppException(ERR_PASSWORD_NOT_EQUAL);
		}
		try {
			if (!userDAO.updatePassword(userId, oldPassword, newPassword[0])) {
				throw new AppException(ERR_ILLEGAL_PASSWORD);
			}
		} catch (DBException e) {
			throw new AppException(ERR_ILLEGAL_PASSWORD);
		}
	}

	/**
	 * Obtains all institutions by country.
	 * 
	 * @param country
	 * @return
	 * @throws AppException
	 */
	public static List<University> getAllInstitutionsByCountry(Country country) throws AppException {
		List<University> institutions = DAOFactory.getUniversityDAO().getAllByCountryName(country.getName());
		throwAppExceptionIfNull(ERR_INVALID_OPERATION, institutions);
		return institutions;
	}

	/**
	 * Obtains all categories by language.
	 * 
	 * @param language
	 * @return
	 * @throws AppException
	 */
	public static List<Category> getAllCategoriesByLanguage(Language language) throws AppException {
		List<Category> categories = DAOFactory.getCategoryDAO().getAllByLanguageName(language.getName());
		throwAppExceptionIfNull(ERR_INVALID_OPERATION, categories);
		return categories;
	}

	/**
	 * Obtains information about test.
	 * 
	 * @param id
	 * @return
	 * @throws AppException
	 */
	public static TestInfoBean getTestInfoById(String id) throws AppException {
		long currentId;
		try {
			currentId = Long.parseLong(id);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		TestInfoBean testInfo = DAOFactory.getTestDAO().getByTestId(currentId);
		if (testInfo == null) {
			throw new AppException("Cannot show information about current test.");
		}
		return testInfo;
	}

	/**
	 * Searches faculties by institution id.
	 * 
	 * @param institutionId
	 * @return
	 * @throws AppException
	 */
	public static List<Faculty> searchFacultiesByInstitutionId(String institutionId) throws AppException {
		throwAppExceptionIfNullOrEmpty("Institution name cannot be empty.", institutionId);
		long currentId;
		try {
			currentId = Long.parseLong(institutionId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		List<Faculty> faculties = DAOFactory.getFacultyDAO().getByFK(currentId);
		throwAppExceptionIfNull(ERR_NO_FACULTIES_OF_INSTITUTION, faculties);
		return faculties;
	}

	/**
	 * Searches groups by faculty id.
	 * 
	 * @param facultyId
	 * @return
	 * @throws AppException
	 */
	public static List<Group> searchGroupsByFacultyId(String facultyId) throws AppException {
		throwAppExceptionIfNullOrEmpty("Faculty name cannot be empty.", facultyId);
		long currentId;
		try {
			currentId = Long.parseLong(facultyId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		List<Group> groups = DAOFactory.getGroupDAO().getByFK(currentId);
		throwAppExceptionIfNull(ERR_NO_GROUPS_OF_FACULTY, groups);
		return groups;
	}

	/**
	 * Searches subjects by category id.
	 * 
	 * @param categoryId
	 * @return
	 * @throws AppException
	 */
	public static List<Subject> searchSubjectsByCategoryId(String categoryId) throws AppException {
		throwAppExceptionIfNullOrEmpty("Category name cannot be empty.", categoryId);
		long currentId;
		try {
			currentId = Long.parseLong(categoryId);
		} catch (NumberFormatException e) {
			throw new AppException(ERR_INVALID_OPERATION);
		}
		List<Subject> subjects = DAOFactory.getSubjectDAO().getByFK(currentId);
		throwAppExceptionIfNull("error_msg.no_subjects_by_category", subjects);
		return subjects;
	}

	/**
	 * Searches tests by the specified conditions.
	 * 
	 * @param languageId
	 * @param role
	 * @param name
	 * @param categoryId
	 * @param subjectId
	 * @param sort
	 * @return
	 * @throws AppException
	 */
	public static List<TestInfoBean> searchTestsByConditions(int languageId, Role role, String name, String categoryId,
			String subjectId, String sort) throws AppException {
		List<TestInfoBean> list = null;
		long id = 0;
		TestDAO testDAO = DAOFactory.getTestDAO();
		if (!Validation.isNullOrEmpty(subjectId)) {
			id = Validation.parseLong(ERR_INVALID_OPERATION, subjectId);
			list = testDAO.searchTestsByCondition(languageId, role, name, null, id);
		} else {
			if (!Validation.isNullOrEmpty(categoryId)) {
				id = Validation.parseLong(ERR_INVALID_OPERATION, categoryId);
				list = testDAO.searchTestsByCondition(languageId, role, name, id, null);
			} else {
				list = testDAO.searchTestsByCondition(languageId, role, name, null, null);
			}
		}
		if (list == null) {
			throw new AppException(ERR_NO_SEARCH_RESULTS);
		}
		if (!isNullOrEmpty(sort)) {
			try {
				TestComparator comparator = TestComparator.valueOf(sort.toUpperCase());
				Collections.sort(list, TestComparatorContainer.get(comparator));
			} catch (IllegalArgumentException e) {
				throw new AppException(ERR_INVALID_OPERATION);
			}
		}
		return list;
	}

	/**
	 * @param answers
	 */
	private static void resetAnswers(List<Answer> answers) {
		for (Answer answer : answers) {
			answer.setCorrect(false);
		}
	}

	/**
	 * Obtains answers for specified question.
	 * 
	 * @param question
	 * @param userRole
	 * @return
	 * @throws DBException
	 */
	private static QuestionAnswerBean getQuestionAnswersByQuestion(Question question, String userRole)
			throws DBException {
		QuestionAnswerBean questionAnswers = new QuestionAnswerBean();
		questionAnswers.setQuestion(question);
		AnswerDAO answerDAO = new AnswerDAO();
		List<Answer> answers = answerDAO.getByFK(question.getId());
		if (userRole.toLowerCase().equals("student")) {
			resetAnswers(answers);
		}
		questionAnswers.setAnswers(answers);
		return questionAnswers;
	}

	/**
	 * Obtains test questions by test id.
	 * 
	 * @param testId
	 * @param userRole
	 * @return
	 * @throws AppException
	 */
	public static TestQuestionsBean getTestQuestionsByTestId(String testId, Object userRole) throws AppException {
		long id = Validation.parseLong(ERR_INVALID_OPERATION, testId);
		throwAppExceptionIfNull(ERR_INVALID_OPERATION, userRole);
		QuestionDAO questionDAO = new QuestionDAO();
		TestQuestionsBean testQuestions = new TestQuestionsBean();
		testQuestions.setTest(DAOFactory.getTestDAO().getByPK(id));
		List<QuestionAnswerBean> questionAnswers = new ArrayList<>();
		for (Question question : questionDAO.getByFK(id)) {
			questionAnswers.add(getQuestionAnswersByQuestion(question, userRole.toString()));
		}
		if (questionAnswers.size() == 0) {
			throw new AppException("Cannot obtain test questions.");
		}
		testQuestions.setQuestions(questionAnswers);
		return testQuestions;
	}
}
