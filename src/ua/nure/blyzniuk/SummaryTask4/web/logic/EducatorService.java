package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.parseLong;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import ua.nure.blyzniuk.SummaryTask4.db.bean.UserGroupDetailBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserTestResultsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Faculty;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.University;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserDAO;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.daoimpl.UserDAOImpl;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.validation.Validation;

public class EducatorService {

	/**
	 * Obtains all institutions of current user.
	 * 
	 * @param user
	 * @return
	 * @throws AppException
	 */
	public static List<University> getAllInstitutionsByEducator(User user) throws AppException {
		List<University> list = DAOFactory.getUniversityDAO().getByFK(user.getId());
		throwAppExceptionIfNull(ERR_NO_EDUCATOR_GROUPS, list);
		return list;
	}

	/**
	 * Obtains faculties by educator id and institutions id.
	 * 
	 * @param educator
	 * @param institution
	 * @return
	 * @throws AppException
	 */
	public static List<Faculty> getFacultiesByEducatorIdAndInstitutionId(User educator, String institution)
			throws AppException {
		long institutionId = parseLong("Error while obtaining faculties.", institution);
		List<Faculty> faculties = DAOFactory.getFacultyDAO().getAllByUserIdAndInstitutionId(educator.getId(),
				institutionId);
		throwAppExceptionIfNull("Cannot obtain faculties of current educator.", faculties);
		return faculties;
	}

	/**
	 * Obtains groups by educator id and institutions id.
	 * 
	 * @param educator
	 * @param faculty
	 * @return
	 * @throws AppException
	 */
	public static List<Group> getGroupsByEducatorIdAndInstitutionId(User educator, String faculty) throws AppException {
		long facultyId = parseLong("Error while obtaining groups.", faculty);
		List<Group> groups = DAOFactory.getGroupDAO().getAllByUserIdAndFacultyId(educator.getId(), facultyId);
		throwAppExceptionIfNull("Cannot obtain faculties of current educator.", groups);
		return groups;
	}

	/**
	 * Obtains test results of the specified student groups.
	 * 
	 * @param educator
	 * @param institutionId
	 * @param facultyId
	 * @param groupId
	 * @return
	 * @throws AppException
	 */
	private static List<UserTestResultsBean> getResults(User educator, String institutionId, String facultyId,
			String groupId) throws AppException {
		Long iId, fId, gId;
		iId = institutionId == null || institutionId.equals("") ? null : Validation.parseLong("Error", institutionId);
		fId = facultyId == null || facultyId.equals("") ? null : Validation.parseLong("Error", facultyId);
		gId = groupId == null || groupId.equals("") ? null : Validation.parseLong("Error", groupId);
		List<UserTestResultsBean> userTestResults = DAOFactory.getUserTestDAO().getAllByEducatorId(educator.getId(),
				iId, fId, gId);
		throwAppExceptionIfNull(ERR_NO_SEARCH_RESULTS, userTestResults);
		return userTestResults;
	}

	/**
	 * Obtains user test results of the students.
	 * 
	 * @param educator
	 * @param institutionId
	 * @param facultyId
	 * @param groupId
	 * @return
	 * @throws AppException
	 */
	public static Set<String> getAllUserTestResultsForEducator(User educator, String institutionId, String facultyId,
			String groupId) throws AppException {
		throwAppExceptionIfNull(ERR_EMPTY_FIELDS, institutionId, facultyId, groupId);
		List<UserTestResultsBean> userTestResults = getResults(educator, institutionId, facultyId, groupId);
		Set<String> testSet = new TreeSet<>();
		for (UserTestResultsBean ut : userTestResults) {
			testSet.add(ut.getTestName());
		}
		if (testSet.size() == 0) {
			throw new AppException(ERR_NO_SEARCH_RESULTS);
		}
		return testSet;
	}

	/**
	 * Obtains test results bu user id.
	 * 
	 * @param userId
	 * @param testResults
	 * @return
	 */
	public static TreeSet<UserTestResultsBean> getTestResultsByUserId(Long userId, List<UserTestResultsBean> testResults) {
		TreeSet<UserTestResultsBean> set = new TreeSet<>();
		for (UserTestResultsBean userTestResultsBean : testResults) {
			if (userTestResultsBean.getUserId().equals(userId)) {
				set.add(userTestResultsBean);
			}
		}
		return set;
	}

	/**
	 * Obtains test results of the students of educator;s groups.
	 * 
	 * @param educator
	 * @param institutionId
	 * @param facultyId
	 * @param groupId
	 * @return
	 * @throws AppException
	 */
	public static Map<UserTestResultsBean, TreeSet<UserTestResultsBean>> getTestResults(User educator,
			String institutionId, String facultyId, String groupId) throws AppException {
		Map<UserTestResultsBean, TreeSet<UserTestResultsBean>> testResults = new HashMap<>();
		List<UserTestResultsBean> userTestResults = getResults(educator, institutionId, facultyId, groupId);
		for (UserTestResultsBean ut : userTestResults) {
			testResults.put(ut, getTestResultsByUserId(ut.getUserId(), userTestResults));
		}
		if (testResults.size() == 0) {
			throw new AppException(ERR_NO_SEARCH_RESULTS);
		}
		return testResults;
	}

	/**
	 * Obtains students by groups id.
	 * 
	 * @param groups
	 * @return
	 * @throws AppException
	 */
	private static List<User> getStudentsByGroupsId(String[] groups) throws AppException {
		long[] groupsId = new long[groups.length];
		for (int i = 0; i < groups.length; i++) {
			groupsId[i] = Validation.parseLong("Cannot obtain students' list.", groups[i]);
		}
		UserDAO userDAO = new UserDAOImpl();
		List<User> users = userDAO.getStudentsByGroupsId(groupsId);
		throwAppExceptionIfNull("Students' list is empty.", users);
		return users;
	}

	/**
	 * Assigns tests to student groups.
	 * 
	 * @param groups
	 * @param foundTests
	 * @param dateEnd
	 * @throws AppException
	 */
	public static void setTests(String[] groups, String[] foundTests, String dateEnd) throws AppException {
		Validation.throwAppExceptionIfNullOrSize0(ERR_EMPTY_GROUP_CHOICE, groups);
		Validation.throwAppExceptionIfNullOrSize0(ERR_EMPTY_TEST_CHOICE, foundTests);
		List<User> users = getStudentsByGroupsId(groups);
		long[] testsId = new long[foundTests.length];
		for (int i = 0; i < foundTests.length; i++) {
			testsId[i] = Validation.parseLong("Cannot set tests.", foundTests[i]);
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date d = format.parse(dateEnd);
			if (d.before(Calendar.getInstance().getTime())) {
				throw new AppException(ERR_ILLEGAL_DATE);
			}
			dateEnd = format.format(format.parse(dateEnd));
		} catch (ParseException | AppException e) {
			throw new AppException(ERR_ILLEGAL_DATE);
		}
		if (!DAOFactory.getUserTestDAO().setUserTests(users, testsId, dateEnd)) {

		}
	}

	/**
	 * Obtains information about educator's student groups.
	 * 
	 * @param educator
	 * @param name
	 * @return
	 * @throws AppException
	 */
	public static List<UserGroupDetailBean> getAllGroupsByNameAndEducator(User educator, String name)
			throws AppException {
		if (name == null) {
			name = "";
		}
		List<UserGroupDetailBean> institutions = DAOFactory.getUserGroupDAO().getAllByUserId(educator.getId(), name);
		if (institutions == null || institutions.size() == 0) {
			throw new AppException(ERR_NO_EDUCATOR_GROUPS);
		}
		return institutions;
	}
}
