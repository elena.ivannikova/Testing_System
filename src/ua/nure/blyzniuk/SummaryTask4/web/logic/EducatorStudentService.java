package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNull;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNullOrEmpty;

import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Group;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserGroup;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;

public class EducatorStudentService {
	
	public static void addUserGroup(User user, String groupId) throws AppException{
		throwAppExceptionIfNullOrEmpty(ERR_EMPTY_GROUP_NAME, groupId);
		long currentId;
		try {
			currentId = Long.parseLong(groupId);
		} catch (NumberFormatException e) {
			throw new AppException("Cannot obtain groups of current faculty.");
		}
		UserGroup userGroup = new UserGroup();
		userGroup.setUserId(user.getId());
		userGroup.setGroupId(currentId);
		if (!DAOFactory.getUserGroupDAO().insert(userGroup)) {
			throw new AppException("Error while linking user with institution group");
		}
	}
	
	public static List<Group> searchGroupsForUserChoise(String facultyId, User user) throws AppException {
		throwAppExceptionIfNullOrEmpty("Faculty name cannot be empty.", facultyId);
		long currentId;
		try {
			currentId = Long.parseLong(facultyId);
		} catch (NumberFormatException e) {
			throw new AppException("Cannot obtain groups by current faculty.");
		}
		List<Group> groups = DAOFactory.getGroupDAO().getAllForUserChoise(currentId, user.getId());
		throwAppExceptionIfNull(ERR_NO_GROUPS_OF_FACULTY, groups);
		return groups;
	}

}
