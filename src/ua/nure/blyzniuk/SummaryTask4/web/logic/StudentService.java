package ua.nure.blyzniuk.SummaryTask4.web.logic;

import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.parseLong;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNull;
import static ua.nure.blyzniuk.SummaryTask4.web.validation.Validation.throwAppExceptionIfNullOrEmpty;
import static ua.nure.blyzniuk.SummaryTask4.web.ErrorMessages.*;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ua.nure.blyzniuk.SummaryTask4.db.bean.QuestionAnswerBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.TestQuestionsBean;
import ua.nure.blyzniuk.SummaryTask4.db.bean.UserObligatoryTestsBean;
import ua.nure.blyzniuk.SummaryTask4.db.dao.DAOFactory;
import ua.nure.blyzniuk.SummaryTask4.db.entity.Answer;
import ua.nure.blyzniuk.SummaryTask4.db.entity.User;
import ua.nure.blyzniuk.SummaryTask4.db.entity.UserTest;
import ua.nure.blyzniuk.SummaryTask4.db.mysql.dao.UserTestDAO;
import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;
import ua.nure.blyzniuk.SummaryTask4.web.exception.DBException;

/**
 * Provides service methods for student role.
 * 
 * @author Elena Blyzniuk
 */
public class StudentService {

	/**
	 * Obtains obligatory tests by student id.
	 * 
	 * @param id
	 *            student id
	 * @return obligatory tests as a container of beans
	 * @throws AppException
	 */
	public static List<UserObligatoryTestsBean> getObligatoryTestsByStudentId(long id) throws AppException {
		List<UserObligatoryTestsBean> list = null;
		try {
			list = DAOFactory.getUserTestDAO().getObligatoryTestsByStudentId(id);
		} catch (DBException e) {
			throw new AppException(e);
		}
		throwAppExceptionIfNull("error_msg.no_obligatory_tests", list);
		for (UserObligatoryTestsBean test : list) {
			if (test.getDateEnd().before(getCurrentDate())) {
				test.setDeadline(true);
			}
		}
		return list;
	}

	private static java.sql.Date getCurrentDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = Calendar.getInstance().getTime();
		return Date.valueOf(df.format(date));
	}

	/**
	 * Obtains question and answers linked with it by testing container and
	 * question ordinal.
	 * 
	 * @param testing
	 *            test container
	 * @param ordinal
	 *            question ordinal
	 * @return question and answers linked with as a bean container.
	 * @throws AppException
	 */
	public static QuestionAnswerBean getQuestionAnswersByOrdinal(TestQuestionsBean testing, String ordinal)
			throws AppException {
		throwAppExceptionIfNull("Current test does not exist.", testing);
		throwAppExceptionIfNullOrEmpty("Error while obtaining question.", ordinal);
		int ord;
		try {
			ord = Integer.parseInt(ordinal);
		} catch (NumberFormatException e) {
			throw new AppException("Error while obtaining question.");
		}
		for (QuestionAnswerBean question : testing.getQuestions()) {
			if (question.getQuestion().getOrdinal() == ord) {
				return question;
			}
		}
		throw new AppException("Question by specified ordinal not found");
	}

	/**
	 * Puts answers into test container.
	 * 
	 * @param testing
	 *            test container to be initialized
	 * @param ordinal
	 *            question ordinal
	 * @param answers
	 *            answers to be inserted
	 * @throws AppException
	 */
	public static void setQuestionAnswers(TestQuestionsBean testing, int ordinal, String[] answers)
			throws AppException {
		throwAppExceptionIfNull(ERR_INVALID_OPERATION, testing);
		testing.getQuestions().get(ordinal - 1).setConfirmed(true);
		List<Answer> questionAnswers = testing.getQuestions().get(ordinal - 1).getAnswers();
		if (answers != null && answers.length != 0) {
			List<String> correctAnswers = Arrays.asList(answers);
			for (Answer answer : questionAnswers) {
				if (correctAnswers.contains(String.valueOf(answer.getOrdinal()))) {
					answer.setCorrect(true);
				} else {
					answer.setCorrect(false);
				}
			}
		}
		testing.getQuestions().get(ordinal - 1).setAnswers(questionAnswers);
	}

	/**
	 * Returns timer for the test.
	 * 
	 * @param testing
	 *            a testing to obtain a timer
	 * @return a string representation of a timer for the test.
	 * @throws AppException
	 */
	public static String getTimer(TestQuestionsBean testing) throws AppException {
		throwAppExceptionIfNull(ERR_INVALID_OPERATION, testing);
		SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss", Locale.ENGLISH);
		Calendar instance = Calendar.getInstance();
		instance.setTime(instance.getTime());
		instance.add(Calendar.MINUTE, testing.getTest().getDuration().getMinutes());
		java.util.Date d1 = instance.getTime();
		String timer = df.format(d1);
		throwAppExceptionIfNullOrEmpty("Error while obtaining test timer,", timer);
		return timer;
	}

	/**
	 * Provides operations to finish test inserting all information about passed
	 * test into database.
	 * 
	 * @param test
	 *            a test container to be inserted.
	 * @throws AppException
	 */
	public static void finishTest(Object test) throws AppException {
		TestQuestionsBean testing = null;
		try {
			testing = (TestQuestionsBean) test;
		} catch (ClassCastException e) {
			throw new AppException("Error while inserting test answers.");
		}
		// if (!testDAO.insertAnswers(testing)) {
		// throw new AppException("Error while inserting test answers.");
		// }
		DAOFactory.getTestDAO().insertAnswers(testing);
	}

	/**
	 * Obtains user test entity by user and test id.
	 * 
	 * @param user
	 *            current user
	 * @param testId
	 *            test id
	 * @return user test entity
	 * @throws AppException
	 */
	public static UserTest getStudentTest(User user, String testId) throws AppException {
		long id = parseLong("Cannot obtain test.", testId);
		UserTestDAO studentTestDAO = DAOFactory.getUserTestDAO();
		UserTest studentTest = studentTestDAO.getByStudentIdAndTestId(user.getId(), id);
		if (studentTest == null) {
			studentTest = new UserTest();
			studentTest.setUserId(user.getId());
			studentTest.setTestId(id);
			studentTest = studentTestDAO.insert(studentTest);
		} else {
			studentTestDAO.update(studentTest);
		}
		return studentTest;
	}

	/**
	 * Obtains test result by user test id.
	 * 
	 * @param test
	 *            a test to be analyzed
	 * @return test result
	 * @throws AppException
	 */
	public static double getTestResultByUserTestId(Object test) throws AppException {
		TestQuestionsBean testing = null;
		try {
			testing = (TestQuestionsBean) test;
		} catch (ClassCastException e) {
			throw new AppException("Error while inserting test answers.");
		}
		Double result = DAOFactory.getTestDAO().getTestResultByUserTestId(testing.getUserTestId());
		if (result == null) {
			result = 0.;
		}
		return result;
	}
}
