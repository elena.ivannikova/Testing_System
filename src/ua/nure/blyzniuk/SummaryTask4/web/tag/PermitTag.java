package ua.nure.blyzniuk.SummaryTask4.web.tag;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import ua.nure.blyzniuk.SummaryTask4.db.entity.User;

public class PermitTag extends TagSupport {

	private static final long serialVersionUID = -2484915463553166793L;

	@Override
	public int doStartTag() throws JspException {
		HttpSession session = pageContext.getSession();
		User user = (User) session.getAttribute("user");

		if (user == null) {
			HttpServletResponse response = (HttpServletResponse) pageContext.getResponse();

			session.setAttribute("errorMessage", "Access denied");
			try {
				response.sendRedirect("index.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return super.doStartTag();
	}

}
