package ua.nure.blyzniuk.SummaryTask4.web.validation;

import java.util.List;

import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;

public class Validation {

	public static boolean isNullOrEmpty(final String... inString) {
		for (String string : inString) {
			if (string == null || string.length() == 0 || string.equals("")) {
				return true;
			}
		}
		return false;
	}

	public static boolean isNull(final Object... objects) {
		for (Object obj : objects) {
			if (obj == null) {
				return true;
			}
		}
		return false;
	}

	public static void throwAppExceptionIfNull(String errorMessage, Object... object) throws AppException {
		if (isNull(object)) {
			throw new AppException(errorMessage);
		}
	}

	public static void throwAppExceptionIfNullOrEmptyList(String errorMessage, List<?> list) throws AppException {
		if (isNull(list) || list.size() == 0) {
			throw new AppException(errorMessage);
		}
	}

	public static void throwAppExceptionIfNullOrEmpty(String errorMessage, String... objects) throws AppException {
		if (isNullOrEmpty(objects)) {
			throw new AppException(errorMessage);
		}
	}

	public static void throwAppExceptionIfNullOrSize0(String errorMessage, String[] strings) throws AppException {
		if (strings == null || strings.length == 0) {
			throw new AppException(errorMessage);
		}
	}

	public static long parseLong(String errorMessage, String object) throws AppException {
		long number = 0;
		try {
			number = Long.parseLong(object);
		} catch (NumberFormatException e) {
			throw new AppException(errorMessage);
		}
		return number;
	}
}
