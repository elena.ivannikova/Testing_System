package ua.nure.blyzniuk.SummaryTask4.web.validation;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.nure.blyzniuk.SummaryTask4.web.exception.AppException;

public class ValidationTest {

	@Test
	public void testIsNullOrEmpty() {
		assertTrue(!Validation.isNullOrEmpty("string"));
	}

	@Test
	public void testIsNull() {
		assertTrue(!Validation.isNull(new Object()));
	}

	@Test
	public void testThrowAppExceptionIfNull() throws AppException {
		Validation.throwAppExceptionIfNull("", new Object());
	}

	@Test
	public void testThrowAppExceptionIfNullOrEmpty() throws AppException {
		assertTrue(Validation.parseLong("", "3") == 3);
	}
}
